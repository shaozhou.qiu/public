#! /bin/bash

# 自动下载ftp服务器上的文件

function exec(){
	server=$1;
	port=$2;
	user=$3;
	pass=$4;
	cmd=$5;
	rpath=$6;
	lpath=$7;
	source default.cache;
	if [[ $server == '' ]]; then
		server=$lst_server;
	fi
	if [[ $port == '' ]]; then
		port=$lst_port;
	fi
	if [[ $user == '' ]]; then
		user=$lst_user;
	fi
	if [[ $pass == '' ]]; then
		pass=$lst_pass;
	fi
	if [[ $cmd == '' ]]; then
		cmd=$lst_cmd;
	fi
	if [[ $rpath == '' ]]; then
		rpath=$lst_rpath;
	fi
	if [[ $lpath == '' ]]; then
		lpath=$lst_lpath;
	fi
	echo "lst_server=$server" > default.cache;
	echo "lst_port=$port" >> default.cache;
	echo "lst_user=$user" >> default.cache;
	echo "lst_pass=$pass" >> default.cache;
	echo "lst_rpath=$rpath" >> default.cache;
	echo "lst_lpath=$lpath" >> default.cache;

	ret=`ftp -nvg <<EOF
open $server $port
user $user  $pass
prompt
bin
cd $rpath
lcd $lpath
$cmd
close 
EOF`
	
	echo "$ret";
}



read -p "ftp server ip: "  svr
read -p "ftp server port: "  prt
read -p "user: "  usr
read -p "password: "  pwd
read -p "remote path: "  rp
read -p "local path: "  lp

# if [[ $lp == '' ]]; then
# 	lp=`pwd`;
# fi


ret=$(exec "$svr" "$prt" "$usr" "$pwd" "ls" "$rp" "$lp");
ret=`echo $ret | sed -r "s/.*for LIST(.*)/\1/g"`;
ret=`echo $ret | sed -r "s/(.*\.jpg).*/\1/g"`;
ret=`echo $ret | sed -r "s/[-rwx]+[-rwx]+[-rwx]+ [0-9] owner group [0-9]+ \w+ [0-9]+ [0-9]+:[0-9]+ //g"`;

m=`echo $ret|grep "Not connected"`
if [[ $m == '' ]]; then
	echo $ret;
	for f in ${ret[@]}; do
		echo "download $f...";
		ret=$(exec "$svr" "$prt" "$usr" "$pwd" "get $f");
		echo "delete $f...";
		ret=$(exec "$svr" "$prt" "$usr" "$pwd" "delete $f");
	done
fi

