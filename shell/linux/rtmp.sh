#! /bin/bash

function main(){
	if [ ! -d "rtmp" ]; then
		mkdir rtmp;
	fi
	cd rtmp;
	echo "download nginx src ..."
	# 下载nginx源码
	ret=`nginx -v 2>&1 | sed -r "s/.*\/([0-9\.]+)/\1/g"`;
	pktNginx="nginx-$ret.tar.gz";
	if [ ! -f "./$pktNginx" ]; then
		wget -P ./ "http://nginx.org/download/$pktNginx"
		if [ ! -f "./$pktNginx" ]; then
			echo -e "\033[41m can't download $pktNginx \033[0m";
			return 0;
		fi
	fi

	echo "download rtmp extends with nginx ..."
	# 下载nginx rtmp扩展模块
	if [ ! -d "./nginx-rtmp-module" ]; then
		git clone https://github.com/arut/nginx-rtmp-module.git;
		if [ ! -d "./nginx-rtmp-module" ]; then
			echo -e "\033[41m can't download nginx-rtmp-module \033[0m";
			return 0;
		fi
	else
		echo "update rtmp extends  ..."
		cd nginx-rtmp-module;
		git pull origin master;
		cd ../
	fi

	echo "download openssl ..."
	# 下载 openssl
	if [ ! -d "./openssl" ]; then
		git clone https://github.com/openssl/openssl.git;
		if [ ! -d "./openssl" ]; then
			echo -e "\033[41m can't download openssl \033[0m";
			return 0;
		fi
	else
		echo "update openssl  ..."
		cd openssl;
		git pull origin master;
		cd ../
	fi

	echo "backup nginx  ..."
	# 备份nginx
	mkdir backup;
	if [ -d "/etc/nginx" ]; then
		mkdir "backup/etc";
		cp -r "/etc/nginx" "backup/etc";
	fi
	if [ -f "/usr/sbin/nginx" ]; then
		mkdir "backup/usr";
		mkdir "backup/usr/sbin";
		cp "/usr/sbin/nginx" "backup/usr/sbin/nginx";
	fi
	
	# 解压nginx
	np=`echo "nginx-1.18.0.tar.gz" | sed -r "s/(.*)\.tar\.gz/\1/g"`;
	tar zxvf "$pktNginx";
	cd $np;
	ls;
	./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module --with-openssl=../openssl --with-pcre=/usr/local/src/pcre-8.45 --add-module=../nginx-rtmp-module
	make
	make install
	rm $np -rf;


	# 配置nginx
	conf="/etc/nginx/nginx.conf";
	if [ -f "$conf" ]; then
		ncp="${conf%/*}";
		ret=`cat $conf|grep rtmp`;
		if [[ "$ret" == "" ]]; then
			echo "rtmp{" >> $conf
			echo "	include $ncp/rtmp/*.conf;" >> $conf
			echo "}" >> $conf
		fi

		if [[ ! -d "$ncp/rtmp" ]]; then
			mkdir "$ncp/rtmp";
		fi
		defconf="$ncp/rtmp/default.conf";
		if [[ ! -f "$defconf" ]]; then
			echo "server {" > $defconf;
			echo "	listen 1935;" >> $defconf;
			echo "	timeout 30s" >> $defconf;
			echo "	max_streams 10;" >> $defconf;
			echo "	chunk_size 4096;" >> $defconf;
			echo "	max_message 1M;" >> $defconf;
			echo "	application streaming {" >> $defconf;
			echo "		live on;" >> $defconf;
			echo "		#录播" >> $defconf;
			echo "		record all;" >> $defconf;
			echo "		record_path /usr/local/live;" >> $defconf;
			echo "		record_suffix .flv;" >> $defconf;
			echo "		# waite_key on;" >> $defconf;
			echo "		hls_path /usr/local/live;" >> $defconf;
			echo "		hls_fragment 5s;" >> $defconf;
			echo "		hls_playlist_length 10s;" >> $defconf;
			echo "	}" >> $defconf;
			echo "}" >> $defconf;
		fi
	fi

}

main;