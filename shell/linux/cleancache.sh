#! /bin/bash
fext=(log backup cache tmp temp)

function del(){
	p="$1";
	for f in $p/*; do
		sf="${f:0:1}...${f:19:13}...${f:0-13:13}   ";
		echo -ne "\r $sf ";
		if [ "$f" == "." ]; then continue; fi
		if [ "$f" == ".." ]; then continue; fi
		if [ -d "$f" ]; then
			del "$f";
			continue;
		fi
		if [ ! -f "$f" ]; then continue; fi
		for fx in ${fext[@]}; do
			fn="${f##*/}";
			fn=`echo $fn`;
			if [ "${fn##*.}" != "$fx" ];then
				cnt=1;
				ext=`echo $fn | sed -r "s/.*(\.$fx\.[0-9]+\.((gz)|(tar)|(tar.gz)))/\1/g"`;
				if [ "$ext" == "" ]; then cnt=1; fi
				if [ "$ext" != "$fn" ]; then
					cnt=0;
				else
					cnt=1;
				fi
				if [ "$cnt" == "1" ]; then continue; fi
			fi
			rm "$f" -rf;
			echo -ne "\r rm $sf";
			echo -ne "\n...";
			break;
		done
	done
}
if [ -d "/storage/emulated/0" ]; then
	del "/storage/emulated/0"
else
	del "";
fi

# skb=0;
# kb=`du -a $f|sed -r "s/^([0-9]+).*/\1/g"`;
# skb=$(($skb+$kb));
# ip=`ip addr |grep "inet [0-9].* brd" | sed -r "s/.*([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\/.*/\1/g"`
# echo "ssh root@$ip -p8022"