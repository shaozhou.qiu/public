#! /bin/bash

# register
function register(){
	desktop=`ls *.desktop`;
	for f in ${desktop[@]}; do
		cp $f ~/.local/share/applications/;
    done
	# update database
	update-desktop-database ~/.local/share/applications/;
}
# unregister
function unregister(){
	desktop=`ls *.desktop`;
	for f in ${desktop[@]}; do
		rm  ~/.local/share/applications/$f -rf;
    done
	# update database
	update-desktop-database ~/.local/share/applications/;
}
function help(){
	echo "You can use the following functions.";
	echo "	install enable		register desktop app";
	echo "	install disable		unregister desktop app";
}


case $1 in
    enable) register ;;
    disable) unregister ;;
    *) help ;;
esac
