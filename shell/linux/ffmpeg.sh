#! /bin/bash
fext=(ts avi wmv mpeg mp4 m4v mov asf flv f4v rmvb rm 3gp vob)

function getMS(){
	f="$1"
	ret=`ffmpeg -i "$f" 2>&1 | grep  -E "[0-9]{3,}x[0-9]{3,}"|sed -r "s/.*\s([0-9]+x[0-9]+).*/\1/g"`;
	echo $ret;
}
function getMW(){
	f="$1"
	ret=$(getMS "$f");
	echo $ret|sed -r "s/([0-9]+)\x.*/\1/g";
}
function getMH(){
	f="$1"
	ret=$(getMS "$f");
	echo $ret|sed -r "s/.*x([0-9]+)/\1/g";
}

# 转为h264编码格式
function toH264(){
	for e in ${fext[@]}; do
		for f in *.$e; do
			if [ ! -f "$f" ]; then
				continue;
			fi
			# echo $f;
			w=$(getMW "$f");
			if [ "$w" == "" ]; then
				continue;
			fi
			nw=`expr 1024 - $w`;
			if [ $nw -lt 0 ];then 
				nw=1024; 
			else
				nw=$w; 
			fi

			f364="h264-$f"
			nf="$f.$nw.mp4"
			if [ -f "$nf" ]; then
				continue;
			fi
			ffmpeg -i "$f" -vcodec h264 -threads 10 -preset ultrafast "$f364"
			if [ -f "$f364" ]; then
				ffmpeg -i "$f364" -vf scale=$nw:-1  -threads 10 -preset ultrafast "$nf"
			fi
			rm "$f364" -rf; 
			
		done
	done
}
# 变速
function x2(){ tspeed 1.25; }
function tspeed(){
	#	setpts	0.25	-	4
	#			0.25		4x
	#			0.50		2x
	#			1.00		1x
	#			
	#	atempo	0.5		-	2.0
	#			0.5		-	0.5x
	#			1.0		-	1x
	#			2.0		-	2x
	speed="$1";
	if [[ "$speed" == "" ]]; then speed="1.25"; fi
	for e in ${fext[@]}; do
		for f in *.$e; do
			speed=1.25;
			#statements
			ffmpeg -i "$f" -filter_complex "setpts=PTS/$speed[v];atempo=$speed[a]" -map "[v]" -map "[a]" -threads 5 -preset ultrafast  "_$f";
		 rm "$f" -rf;
			ffmpeg -i "_$f" -vf scale=1024:-1 -threads 5 -preset ultrafast "$f";
		 rm "_$f" -rf
		done
	done
}

# 录屏
function recoardScreen(){
	ffmpeg -f gdigrab -i desktop out.mp4
}

# 同时录制屏幕+系统声音+麦克风
function recoardScreen(){
	ffmpeg -f dshow -i audio="麦克风 (Realtek High Definition Audio)" -f dshow -i audio="virtual-audio-capturer" -filter_complex amix=inputs=2:duration=first:dropout_transition=0 -f dshow -i video="screen-capture-recorder" -pix_fmt yuv420p out.mp4

	ffmpeg -f dshow -i video="screen-capture-recorder" -f dshow -i audio="virtual-audio-capturer" -pix_fmt yuv420p -vcodec libx264 -acodec libvo_aacenc -s 1280x720 -r 25 -q 10 -ar 44100 -ac 2 -tune zerolatency -preset ultrafast -f mpegts - | ffmpeg -f mpegts -i - -c copy -bsf:a aac_adtstoasc -f flv temp.flv
}
# 同时录制屏幕+系统声音
function recoardScreen(){
	ffmpeg -f dshow -i audio="virtual-audio-capturer":video="screen-capture-recorder" -pix_fmt yuv420p out.mp4
	ffmpeg -f gdigrab -t 30 -framerate 15 -i desktop -f dshow -i audio="virtual-audio-capturer" -b:v 3M -pixel_format yuv420p -vcodec libx264 -s 1366x768 -y d:/test.flv
}
# 本地视频的推流
function pushMovies(){
	for e in ${fext[@]}; do
		for f in *.$e; do 
			ffmpeg -re -i "$f" -acodec aac -ar 32000 -vcodec copy -f flv "rtmp://127.0.0.1:8035/live/video"; 
			break;
		done
	done
}
# 本地摄相头的推流
function pushCamera(){
	for e in ${fext[@]}; do
		for f in *.$e; do 
			ffmpeg -f dshow -i video="USB2.0 PC CAMERA" -vcodec libx264 -preset:v ultrafast -tune:v zerolatency -f flv rtmp://127.0.0.1/live/camera; 
			break;
		done
	done
}
# 本地Mic的推流
function pushMic(){
	for e in ${fext[@]}; do
		for f in *.$e; do 
			ffmpeg -f dshow -i audio="麦克风 (2- USB2.0 MIC)" -vcodec libx264 -preset:v ultrafast -tune:v zerolatency -f flv rtmp://127.0.0.1/live/mic; 
			break;
		done
	done
}
# 本地Cam和Mic的推流
function pushMic(){
	for e in ${fext[@]}; do
		for f in *.$e; do 
			ffmpeg -f dshow -i video="USB2.0 PC CAMERA" -i audio="麦克风 (2- USB2.0 MIC)" -vcodec libx264 -preset:v ultrafast -tune:v zerolatency -f flv rtmp://127.0.0.1/live/mic; 
			break;
		done
	done
}