@echo off
set curPath=%cd%

call:createReg
call:createRenReg

goto:eof

:createReg
	set c_path=%curPath:\=\\%
	set fn=%curPath%\tmp.reg
	echo Windows Registry Editor Version 5.00 > "%fn%"
	echo [HKEY_CLASSES_ROOT\webshell] >> "%fn%"
	echo "URL Protocol"="" >> "%fn%"
	echo @="URL : webshell Protocol" >> "%fn%"
	echo [HKEY_CLASSES_ROOT\webshell\DefaultIcon] >> "%fn%"
	echo @="%c_path%\\webshell.bat" >> "%fn%"
	echo [HKEY_CLASSES_ROOT\webshell\shell] >> "%fn%"
	echo [HKEY_CLASSES_ROOT\webshell\shell\open] >> "%fn%"
	echo [HKEY_CLASSES_ROOT\webshell\shell\open\command] >> "%fn%"
	echo @="\"%c_path%\\webshell.bat\" \"%%1\"" >> "%fn%"
	"%fn%"
	del "%fn%" /Q
GOTO:EOF

:createRenReg
	set c_path=%curPath:\=\\%
	set fn=%curPath%\tmp.reg
	echo Windows Registry Editor Version 5.00 > "%fn%"
	echo [HKEY_CLASSES_ROOT\Directory\Background\shell\et_rename] >> "%fn%"
	echo @="Rename By Directory" >> "%fn%"
	echo "Icon"="" >> "%fn%"
	echo [HKEY_CLASSES_ROOT\Directory\Background\shell\et_rename\command] >> "%fn%"
	echo @="%c_path%\\rename.bat" >> "%fn%"
	"%fn%"
	del "%fn%" /Q
GOTO:EOF