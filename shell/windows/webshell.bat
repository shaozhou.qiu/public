@echo off
set url=%1
set cmd=%url:~12, -1%
set cmd=%cmd:/= %
setlocal enabledelayedexpansion
rem set cmd=!cmd:%%26=&!
set cmd=!cmd:%%2F=/!
set cmd=!cmd:%%3A=:!
set cmd=!cmd:%%3B=;!
set cmd=!cmd:%%3C=<!
set cmd=!cmd:%%3D==!
set cmd=!cmd:%%3E=>!
set cmd=!cmd:%%3F=?!
set cmd=!cmd:%%5B=[!
set cmd=!cmd:%%5C=\!
set cmd=!cmd:%%5E=%%!
set cmd=!cmd:%%60=`!
set cmd=!cmd:%%40=@!
set tfn=ws.tmp.bat
echo %cmd% > %tfn%
del /f %tfn%

echo %cmd%|findstr "^xfreerdp" >null
if %errorlevel% equ 0 ( 
	call:xfreerdp %cmd%
	GOTO:EOF
)

echo %cmd%|findstr "^mstsc" >null
if %errorlevel% equ 0 ( 
	call:xfreerdp %cmd%
	GOTO:EOF
)

%cmd%


GOTO:EOF


:xfreerdp
	:loop
		if %1a==a goto :end
		set arg=%1;
		shift
		echo %arg%|findstr "^/v:" > null
		if %errorlevel% equ 0 ( 
			set v=%arg:~3,-1%
			goto :loop
		)
		echo %arg%|findstr "^/u:" > null
		if %errorlevel% equ 0 ( 
			set u=%arg:~3,-1%
			goto :loop
		)
		echo %arg%|findstr "^/p:" > null
		if %errorlevel% equ 0 ( 
			set p=%arg:~3,-1%
			goto :loop
		)
	goto :loop
	:end
	call:runmstsc %v% %u% %p%
GOTO:EOF

:runmstsc
	set svr=%1
	set usr=%2
	set pass=%3
	set port=%svr:*:=%
	if "%svr%" == "%port%" ( set port=3389 )
 	for /f "tokens=1 delims=:" %%a in ("%svr%") do set svr=%%a
	Cmdkey /add:TERMSRV/%svr% /user:%usr% /pass:%pass%
	echo login.....
	Start /wait mstsc /v:%svr%:%port% /f /noConsentPrompt
	Cmdkey /delete:TERMSRV/%svr%
GOTO:EOF
