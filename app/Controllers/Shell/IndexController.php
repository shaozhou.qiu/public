<?php
namespace App\Controllers\Shell;
use App\Controllers\Controller;

class IndexController extends Controller
{
	public function index(){
        $cmd = $_POST["cmd"]??$_GET["cmd"]??null;
        if(!$cmd){
        	return ['errcode'=>1, 'data'=>"$cmd: command not found"];
        }
        $ret = ['errcode'=>0, 'data'=>"ok"];
        if(preg_match("/^\s*?([^\s]+)(.*)$/im", $cmd, $m)){
        	$fun=strtolower($m[1]);
        	$ret = $this->$fun($m[2]);
        }
		return $ret;
	}
	function __call($name, $arguments){
		return ['errcode'=>2, 'data'=>"$name: command not found"];
	}
	private function argumentsToArray($args, $withoutParameters=[]){
		$ret = [];
		$fdata=[];
		if(preg_match_all("/\"([^\"]*)\"/im", $args, $mc, PREG_SET_ORDER)){
			foreach ($mc as $v) {
				array_push($fdata, $v[1]);
				$args = str_replace($v[0], "{{fd_".(count($fdata)-1)."}}", $args);
			}
		}
		foreach ($withoutParameters as $v) {
			$pattern = "/(^|\s)(".preg_quote($v).")(\s|$)/im";
			if(preg_match($pattern, $args, $m)){
				$args = preg_replace($pattern, " ", $args);
				$ret[$m[2]] = true;
			}
		}
		if(preg_match_all("/(\-[\w\-]+)\s+([^\s]+)/im", $args, $mc, PREG_SET_ORDER)){
			foreach ($mc as $v) {
				$ret[$v[1]] = $v[2];
				$args = preg_replace("/".preg_quote($v[0])."/im", " ", $args);
			}
		}
		
		if(preg_match_all("/([^\s]+)/im", $args, $mc, PREG_SET_ORDER)){
			foreach ($mc as $v) {
				$val = $v[1];
				array_push($ret, $val);
			}
		}
		foreach ($ret as $k => $v) {
			if(preg_match("/\{\{fd_(\d+)\}\}/im", $v, $m)){
				$ret[$k] = preg_replace("/".preg_quote($m[0])."/im", $fdata[$m[1]]??'', $v);
			}
		}
		return $ret;
	}

	private function curl($args){
		$args = $this->argumentsToArray($args, ['-k', '-L', '-s']);
        $ret = ['errcode'=>0, 'data'=>"ok"];
		// 1:POST  0:GET
    	$method = $args['-X']??null;
    	if(null == $method) $method = isset($args['-d'])?"POST":"GET";
		// Request URL
    	$url = $args[0];
    	// referer
    	$refererUrl = $args['-e']??$args['--referer']??$url;
    	$post_data = $args['-d']??"";
    	$useragent = $args['-A']??$args['--user-agent']??'';
    	$contenttype = $args['-H']??'';
    	$maxredirs = isset($args['-L'])?20:0;
    	$cookieFile = $args['-b']??'';
    	$cookieJar = $args['-c']??'';
    	$clientIP = mt_rand(0, 255).".".mt_rand(0, 255).".".mt_rand(0, 255).".".mt_rand(0, 255)."";
    	$header = [
    		"Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
			, "Accept-Encoding: gzip, deflate, br"
			, "Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7"
			, "Content-Type: text/html; charset=UTF-8"
			, "Accept-Language: zh-CN,zh;q=0.9,en;q=0.8"
			, "Cache-Control: max-age=0"
			, "Connection: keep-alive"
			, "sec-ch-ua: \"Chromium\";v=\"92\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"92\""
			, "CLIENT-IP:$clientIP"
			, "X-FORWARDED-FOR:$clientIP"
    	];






    	if($refererUrl == $url){
    		$refererUrl = preg_replace("/(http.*\/\/[^\/]+).*/im", "$1", $refererUrl);
    	}
    	$path = $_SERVER['DOCUMENT_ROOT'];
        if(file_exists("$path/downloadfiles")){
            $path .= "/downloadfiles";
        }
        $path = "$path/cookie";
        if(!file_exists($path))mkdir($path);

        if($cookieFile){
        	$cookieFile = "$path/".preg_replace("/.*?\.([^\/]+).*/im", "$1", $url).".tmp";
        }
        if($cookieJar){
        	$cookieJar = "$path/".preg_replace("/.*?\.([^\/]+).*/im", "$1", $url).".tmp";
        }

    	$method = preg_replace("/\s*/im", "", $method);
    	$method = strtoupper($method);
    	if(is_string($post_data)){
    		$poststr=$post_data;
    		$post_data=[];
    		if (preg_match_all("/([^\=\?\&]+)=([^\=\?\&]+)/im", $poststr, $mc, PREG_SET_ORDER)) {
    			foreach ($mc as $m) {
    				$post_data[$m[1]] = $m[2];
    			}
    		}
    	}

    	// 创建一个cURL资源
		$ch = curl_init();
		//设置头文件的信息作为数据流输出
		curl_setopt($ch, CURLOPT_HEADER, 0);


		if(preg_match("/^\s*https:.*/im", $url)){
			//https请求 不验证证书和hosts
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		}
		//设定来源页面 
		curl_setopt($ch, CURLOPT_REFERER, $refererUrl);

		if($maxredirs>0){
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // 跟随重定向
	        curl_setopt($ch, CURLOPT_MAXREDIRS, $maxredirs); // 重定向次数
		}
        
		//设置获取的信息以文件流的形式返回，而不是直接输出。
 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 		if(!$useragent){
 			$useragent = $this->getUserAgent();
 		}
 		curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
 		if($method == "POST"){ // POST
			//设置post方式提交
			curl_setopt($ch, CURLOPT_POST, 1);
 		}else if($method == "PUT"){ // PUT
 			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
 		}else if($method == "DELETE"){ // DELETE
 			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
 		}else{ // GET
 			curl_setopt($ch, CURLOPT_HTTPGET, 1);
 			if(strpos($url, "?")===false)$url .= "?";
 			foreach ($post_data as $k => $v) {
 				$url .= "&$k=".urlencode($v);
 			}
 		}
		if(count($post_data)){
			if($contenttype) array_push($header, "Content-Type:".$contenttype);
			//设置post数据
			if(preg_match("/(.*\/json\s*$)/im", $contenttype)){
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
			}else if(preg_match("/(.*\/x-www-form-urlencoded\s*$)/im", $contenttype)){
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
			}else{
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			}
		}
		if(is_string($header))$header= array($header);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		foreach ($header as $v) {
			if(preg_match("/Accept-Encoding: (.+)/im", $v, $m)){
				curl_setopt($ch, CURLOPT_ENCODING, $m[1]);
			}
		}

		if($cookieFile)
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile); // 设置cookie
		if($cookieJar)
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieJar); // 设置cookie


 		// 设置URL和相应的选项
		curl_setopt($ch, CURLOPT_URL, $url);
		// 抓取URL并把它传递给浏览器
		$data = curl_exec($ch);

		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if(!$data){
			$data=curl_getinfo($ch);
		}
		// 关闭cURL资源，并且释放系统资源
		curl_close($ch);
		return ["errcode"=>$httpCode, "data"=>$data];
	}
	private function getUserAgent(){
		$uas = array(
			"chrome"=>"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36"

			,"百度爬虫"=>"* Baiduspider+(+http://www.baidu.com/search/spider.htm)"
			,"google爬虫001"=>"* Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
			,"google爬虫002"=>"* Googlebot/2.1 (+http://www.googlebot.com/bot.html)"
			,"google爬虫003"=>"* Googlebot/2.1 (+http://www.google.com/bot.html)"
			,"雅虎爬虫001"=>"*Mozilla/5.0 (compatible; Yahoo! Slurp China; http://misc.yahoo.com.cn/help.html)"
			,"雅虎爬虫002"=>"*Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)"
			,"新浪爱问爬虫001"=>"*iaskspider/2.0(+http://iask.com/help/help_index.html)"
			,"新浪爱问爬虫002"=>"*Mozilla/5.0 (compatible; iaskspider/1.0; MSIE 6.0)"
			,"搜狗爬虫001"=>"*Sogou web spider/3.0(+http://www.sogou.com/docs/help/webmasters.htm#07)"
			,"搜狗爬虫002"=>"*Sogou Push Spider/3.0(+http://www.sogou.com/docs/help/webmasters.htm#07)"
			,"网易爬虫"=>"*Mozilla/5.0 (compatible; YodaoBot/1.0; http://www.yodao.com/help/webmaster/spider/)"
			,"MSN爬虫"=>"*msnbot/1.0 (+http://search.msn.com/msnbot.htm"
		);
		$ret = "";
		$i = -1;
		$n = mt_rand(0, count($uas));
		foreach ($uas as $v) {
			$i++;
			if($i != $n)continue;
			$ret = $v;
			break;
		}
		return $ret;
	}
}