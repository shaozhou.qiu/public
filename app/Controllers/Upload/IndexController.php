<?php
namespace App\Controllers\Upload;
use App\Controllers\Controller;

class IndexController extends Controller
{
    private $basePath = "";
	public function index(){
        $this->verification();
        $path = $_POST['path']??null;
        $this->initUploadPath($path);
        $url = $_GET['url']??null;
        if($url)$this->uploadRemoteFile(urldecode($url));
        if(count($_FILES)>0){
            $this->uploadBlock();die;
        }
	}
    // 验证是否允许上传
    private function verification(){
        if(!isset($_SERVER['HTTP_REFERER'])) $this->error404();
        $referer = $_SERVER['HTTP_REFERER'];
        if(!strstr($referer, "//".$_SERVER['HTTP_HOST'])) $this->error404();

        $blobIdx = $_POST['index']??0;
        if($blobIdx ==0 ) 
            if($this->limiteTime())$this->error404();
        
    }
    private function limiteTime($millisecond=500){
        // 限制 500 毫秒内只能提交一次
        $uid = $_COOKIE['uid']??null;
        if(!$uid){
            $uid = json_encode(["uid"=>hash("sha1", time()), "quantity"=>0, "time"=>microtime(true)-$millisecond], true);
            setcookie("uid", $uid);
        }
        $uid = json_decode($uid, true);
        if((microtime(true)-$uid['time'])*1000<$millisecond){
            $uid['time'] = microtime(true);
            if($uid['quantity']>5) $uid['time'] += 60; 
            if($uid['quantity']>8) $uid['time'] += 60*5; 
            if($uid['quantity']>10) $uid['time'] += 60*60*24; 
            if($uid['quantity']>11) $uid['time'] += 60*60*24*30; 
            if($uid['quantity']>15) $uid['time'] += 60*60*24*30*2; 
            $uid['quantity']+=1;
            setcookie("uid", json_encode($uid, true));
            dump($uid); die;
            return true;
        }
        $uid['quantity']=0;
        $uid['time']=microtime(true);
        setcookie("uid", json_encode($uid, true));
        return false;
    }
    private function error404(){
        ob_end_clean();
        header("Connection: close");
        header("HTTP/1.1 404 Error"); 
        die;
    }
    private function uploadRemoteFile($url){
        $ext = preg_replace("/.*\.(\w+)$/", "$1", $url);
        $filename = $ext = $this->basePath.DIRECTORY_SEPARATOR.hash('sha1', $url).".".$this->getExt("$ext");
        // if(file_exists($filename)){ callback(["errcode"=>1, "data"=>"err : file exists."]);  }
        $start = $_GET['s']??0;
        $this->uploadRemoteFileBlob($url, $start, $filename);

    }
    private function uploadRemoteFileBlob($url, $s=0, $filename){
        $fn = "$filename";
        $header = @get_headers($url, true);
        if(!$header)$header=['Content-Length'=>filesize($url)];
        $size =  $header['Content-Length'];
        $nurl = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']. preg_replace("/[\\\\]+/im", "/", "".str_replace($_SERVER['DOCUMENT_ROOT'], "", $fn));

        ignore_user_abort(true); // 允许客户端关闭，后台运行，不受前端断开连接影响
        set_time_limit(3600); // 脚本最多运行1个小时
        ob_end_clean();

        ob_start();
        header("Connection: close");
        header("HTTP/1.1 200 OK"); 
        echo json_encode(['errcode'=>0, 'data'=>[
            "uri" => $fn
            ,"size" => $size
            ,"url" => $nurl
        ]]);
        ob_end_flush();
        flush();
        if (function_exists("fastcgi_finish_request")) {
             fastcgi_finish_request(); /* 响应完成, 关闭连接 */
        }
        if(file_exists($fn)){
            if($size==filesize($fn)) die;
        }
        $ch = curl_init();
        if($ch == false){ die; }
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
        curl_setopt($ch, CURLOPT_BUFFERSIZE, 20971520);
        $tfn = "$fn.tmp";
        $h = @fopen($tfn, 'wb');
        if($h == false){ die; }
        curl_setopt($ch, CURLOPT_WRITEFUNCTION, function($ch ,$str) use($h) {
            $len = strlen($str);
            fwrite($h, $str);
            return $len;
        });
        $output = curl_exec($ch);
        fclose($h);
        curl_close($ch);
        rename($tfn, $fn);

    }
    private function initUploadPath($path=null){
        if(null == $path){
            $path = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."uploadfiles";
            $path .= date(DIRECTORY_SEPARATOR."Y".DIRECTORY_SEPARATOR."m".DIRECTORY_SEPARATOR."d");
        }
        $this->basePath = $path;
        $this->mkdir( $path );
    }
    private function fillterExt($ext){
        if(preg_match("/^\s*(php)|(jsp)|(java)|(html?)\s*$/", $ext, $m)){
            return true;
        }
        return false;
    }
    private function reExt($ext){ return "$ext.txt"; }
    private function getExt($fn){
        if(preg_match("/(.+\.)?(\w+)$/", $fn, $m)){
            return ($this->fillterExt($m[2]))?$this->fillterExt($m[2]):$m[2];
        }
        return "txt";
    }
    /**
     * 上传本地文件， 以blob 分段上传
     * @return [type] [description]
     */
    private function uploadBlock(){
        $data = array(
            'name' => $_POST['name']??''
            ,'index' => $_POST['index']??0
            ,'total' => $_POST['total']??0
        );
        $tmpPath = $this->basePath.DIRECTORY_SEPARATOR.hash('sha1', $data['name']).".tmp";
        $this->mkdir( $tmpPath );
        $cachefile = $tmpPath.DIRECTORY_SEPARATOR.$data['index'];
        if(!file_exists($cachefile)){
            $ret = @move_uploaded_file($_FILES['block']['tmp_name'], $cachefile);
            if(!$ret){
                echo json_encode(['errcode'=>3, 'error'=>$_FILES['block']['error'], 'errmsg'=>'Err:upload failed!', 'data'=>$data], true); die;   
            }
        }
        if($data['index'] == $data['total']){
            $idx=1; $err=false;
            $ext = $this->getExt($data['name']);
            $fn = $this->createFileName($ext, $this->basePath);
            shell_exec("touch $fn");
            while ($idx <= $data['total']) {
                $tf = $tmpPath.DIRECTORY_SEPARATOR.$idx;
                if(!file_exists( $tf )){ $err= true; unlink($fn); break; }
                shell_exec("cat $tf >> $fn");
                unlink($tf);

                $idx++;
            }

            if($err){
                echo json_encode(['errcode'=>2, 'data'=>"[$idx] No such file block found!"], true);
            }else{
                rmdir($tmpPath);
                echo json_encode(['errcode'=>0, 'data'=>[
                    'size' => filesize($fn)
                    , 'filepath' => $fn
                    , 'uri' => $this->path2uri($fn)
                    , 'url' => $this->path2url($fn)
                    , 'name' => preg_replace("/.*?(\w+)(\.\w+)?$/im", "$1$2", $fn)
                ]], true);
            }
            die;   
        }else{
            echo json_encode(['errcode'=>1, 'data'=>$data], true); die;   
        }
    }
    private function denyFile(){

    }
    private function path2url($path){
        $uri = $this->path2uri($path);
        return $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$uri;
    }
    private function path2uri($path){
        $pathname = str_replace($_SERVER["DOCUMENT_ROOT"], "", $path);
        $pathname = preg_replace("|".preg_quote(DIRECTORY_SEPARATOR)."|im", "/", $pathname);
        return $pathname;
    }
    private function createFileName($ext, $path){
        $fn = hash("sha1", time().microtime());
        if($ext)$fn .= ".".preg_replace("/^[^\.]*\.(.+)/", "$1", $ext);
        return (null==$path)?$fn:$path.DIRECTORY_SEPARATOR.$fn;
    }
    private function mkdir($path){
        $ret = true;
        preg_match_all("/\\".DIRECTORY_SEPARATOR."[^\\".DIRECTORY_SEPARATOR."]+/im", $path, $mc, PREG_SET_ORDER);
        if($mc){
            $p = '';
            foreach ($mc as $k => $v) {
                $p .= $v[0];
                $ret = $this->createPath($p);
                if(!$ret) return $ret;
            }
        }
        return $ret;
    }
    private function createPath($path){
        if( file_exists( $path ) ){return true;}
        try{
            return @mkdir($path, 0744);
        }catch(\Exception $e){
            return false;
        }
    }
}
  