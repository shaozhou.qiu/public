<?php
namespace App\Controllers;

class Controller
{
	protected function getRootPath($filename=null){
        $uri = "";
        if(preg_match("/(.*)\/app\/Controllers.*/im", __FILE__, $m)){
            $uri = $m[1];
        }
        if($filename){
            $uri.=$filename;
            if(!file_exists($uri))return null;
        }
        return $uri;
    }
    protected function http_get($url, $data=null){return $this->http($url, $data, false); }
	protected function http_post($url, $data=null){return $this->http($url, $data, true); }
	
	protected function http($url, $params = false, $ispost = 0, $https = 0)
    {
    	$https =$https!=0?$https:( strstr($url, "https")==$url )?true:false;
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        // curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); // 强制进行ip4解析
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($https) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 对认证证书来源的检查
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // 从证书中检查SSL加密算法是否存在
        }
        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params){
                if (is_array($params)) {
                    $params = http_build_query($params);
                }
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);	// 此处就是参数的列表,给你加了个
			}else{
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }

        $response = curl_exec($ch);

        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));

		$info  = curl_getinfo( $ch );
		$info['errno'] = curl_errno( $ch );
		$info['data'] = $response;

        curl_close($ch);
        return $info;
    }	
}