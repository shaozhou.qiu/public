<?php
namespace App\Controllers\Cross;
use App\Controllers\Controller;

class IndexController extends Controller
{
	public function index(){
		$url=isset($_GET['url'])?$_GET['url']:null;
        $type = $_GET['type']??null;
		if($url==null)return "";
        $sptName = ($_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']);
        if(preg_match("/(^\.\/)(\w.*)/im", $url, $m)){ // ./lib/xxx.js
            $curPath = preg_replace("/^(.*)\/.*$/im", "$1", $sptName);
            $url = $curPath.'/'.$m[2];
        }else if(preg_match("/^((\.\.\/){1,})(\w.*)/im", $url, $m)){// ,,/../lib/xxx.js
            $nurl = preg_replace("/(.*\/).*/im", "$1", $sptName);
            $nurl = $nurl.$url;
            while( strstr($nurl, "../") ){
                $nurl = preg_replace("/[^\/]+\/\.\.\//im", "", $nurl);
            }
            $url = $nurl;
        }else if(preg_match("/^\/(\w.*)/im", $url, $m)){
            $url = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].'/'.$m[1];
        }
        // dump($url); die();
		$ret = $this->http_get($url);
        if(!$type){
            if(preg_match("/.*\.(\w+).*$/im", $url, $m))
            {
                $type = strtolower($m[1]);
            }
        }
        if(strpos($url, $_SERVER['HTTP_HOST'])){
            if($type == 'css'){
                $ret['data'] = $this->removeComments($ret['data']);
                $ret['data'] = $this->excludeString("/\{[\w\W]*?\}/im", $ret['data'], function($e){
                    $d =  preg_replace("/^[\r\n\s]+/im", '', $e);
                    $d =  preg_replace("/(^[\r\n\s]+)|($[\n\r\s]+)/im", '', $d);
                    return $d;
                });
            }else if($type == 'js'){
                // echo($ret['data']); die;
                $ret['data'] = $this->excludeString("/[^\\]\/.*?[^\\]\//im", $ret['data'], function($e){
                    $e = $this->removeComments( $e );
                    return $e;
                    return $this->excludeString("/'.*?'/im", $e, function($e){
                        return $this->excludeString("/\".*?\"/im", $e, function($e){
                            return preg_replace("/\/\/.*/im", '', $e);
                        });
                    });
                });
            }
        }

        if($type == 'css'){
            $ret = $this->reponseCss( $ret );
        }else if($type == 'js'){
            $ret = $this->reponseJs($ret);
        }
        return $ret;
	}
    private function reponseJs($data){
        header('Content-type: text/javascript');
        return $data;
    }
    /**
     * 输出css
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    private function reponseCss($data){
        $fontSize = $_GET['fontSize']??null;
        $css = $data['data'];
        preg_match_all("/([^\d]\s*)([\d\.]+\s*)rem/im", $css, $mc, PREG_SET_ORDER);
        foreach ($mc as $key => $m) {
            $css = preg_replace("/".$m[0]."/im", $m[1].($m[2]*100)."px", $css);
        }
        header('Content-type: text/css');
        
        echo($css); 
        die();
    }
    /**
     * 移除注释 
     * @return [type] [description]
     */
    private function removeComments($data){
        return $this->excludeString("/'.*?'/im", $data, function($e){ // 单引号
            return $this->excludeString("/\".*?\"/im", $e, function($e){ // 双引号
                $d = preg_replace("/^\s*\/\*[\w\W]*?\*\/[\s\r\n]*$/im", '', $e);
                return preg_replace("/\s*\/\*[\w\W]*?\*\/[\s\r\n]*/im", '', $d);

            });
        });
        // return $data; 
    }

    private function excludeString($re, $data, $fun){
        $flag = "{ET_EXS_FLAG_".dechex(mt_rand())."_%d}";
        $ret = preg_match_all($re, $data, $mc, PREG_SET_ORDER);
        if($ret){
            foreach ($mc as $k => $v) {
                $f = sprintf($flag, $k);
                $data = preg_replace("/".preg_quote($v[0], "/")."/im", $f, $data);
            }
        }
        if($fun){ $data = $fun($data); }
        if($ret){
            foreach ($mc as $k => $v) {
                $f = sprintf($flag, $k);
                $v = preg_replace("/\\$/im", "{et_charcodeat_".ord('$')."}", $v[0]);
                $v = preg_replace("/\\\\/im", "{et_charcodeat_".ord('\\')."}", $v);

                $data = preg_replace("/".preg_quote($f, "/")."/im", $v, $data);

                if(preg_match_all("/\{et_charcodeat_(\d+)\}/im", $data, $mc, PREG_SET_ORDER)){
                    foreach ($mc as  $m) {
                        $data = preg_replace("/".preg_quote($m[0])."/im", chr($m[1]), $data);
                    }
                }

                // $data = preg_replace("/\{et_charcodeat_36\}/im", "$", $data);
            }
        }
        return $data;
    }
}