<?php
namespace App\Controllers\Images;
use App\Controllers\Controller;

class IndexController extends Controller
{
	public function index(){
        $url = $_GET["url"]??null;
        $type = $_GET["tp"]??'image/jpeg';
        $sx = $_GET["sx"]??0; $sy = $_GET["sy"]??0;
        $sw = $_GET["sw"]??0; $sh = $_GET["sh"]??0;
        $w = $_GET["w"]??0; $h = $_GET["h"]??0;
        if($w == 0 && $h == 0){
        	$w = -32; $h = -32;
        }
        $act = $_GET["act"]??null;
        switch ($act) {
          case 'thumbnail':
      			$img = $this->thumbnail($url, $w, $h, $sx, $sy, $sw, $sh);
        		$this->reponseImg($img, $type);
        		break;
        	
        	case 'getsize':
        		$data = $this->getSize($url);
        		return $data;
        		break;
        	
        	default:
				if(!file_exists($url)){
					$nopic = $this->getRootPath("/img/nopic.png");
					if($nopic)$url = $nopic;
				}
        		$ret = $this->imagecreatefrom($url);
        		$this->reponseImg($ret['img'], $type);
        		break;
        }
	}
	
	private function getSize($url){
		list($w, $h) = getimagesize($url);
		return [
			'url'=>$url
			,'width'=>$w
			,'height'=>$h
		];
	}
	private function reponseImg($img, $type="image/png"){
    header("Content-type: $type");
		imagejpeg($img, null, 100);
		imagedestroy ($img);
		die;
	}


	/**
	 * 获取缩略图保存位置
	 * @param  [type] $flg 标签
	 * @return [type]      [description]
	 */
	private function getThumbnailFileName($flg){
		$id = hash("sha1", "$flg");
		preg_match_all("/(\w{5})/mi", $id, $mc, PREG_SET_ORDER);
		$thumbPath = $_SERVER["DOCUMENT_ROOT"]."/downloadfiles/thumbnail";
		foreach ($mc as $k => $m) {
			$thumbPath .= "/".$m[0];
		}
		return $thumbPath.DIRECTORY_SEPARATOR."$id.jpg";
	}
	/**
	 * 缩略图
	 * @param  [type]  $url 源图片地址
	 * @param  [type]  $w   缩放后宽度
	 * @param  [type]  $h   缩放后高度
	 * @param  integer $sx  从原图X坐标开始复制，默认为0
	 * @param  integer $sy  从原图Y坐标开始复制，默认为0
	 * @param  integer $sw  从原图截止宽度，默认为原图宽度
	 * @param  integer $sh  从原图截止高度，默认为原图高度
	 * @return [type]       [description]
	 */
	private function thumbnail($url, $w, $h, $sx=0, $sy=0, $sw=0, $sh=0){
		if(!$url) return ;
		if(preg_match("/^[\w]+:\/\/\.*/im", $url)){
      $url = $this->download($url);
      if($url == null){
          $url = preg_replace("/(.+)\/app\/.*$/im", "$1", $_SERVER["SCRIPT_FILENAME"]);
          $url = "$url/img/404.png";
      }
    }
		$thumbFile = $this->getThumbnailFileName("$url,$w,$h,$sx,$sy,$sw,$sh");		
		if(file_exists($thumbFile)){
			$image = $this->imagecreatefrom($thumbFile);
			return $image==null?null:$image['img'];
		}
		if(!function_exists("imagecreate")){
			echo("no install gd."); die;
    }
    if(!getimagesize($url)){
        return null;
    }
		list($sWidth, $sHeight) = getimagesize($url);
		if($sWidth==0 && $sHeight==0){
			preg_match("/(.*)\/app\/.*/im", $_SERVER['PHP_SELF'], $m);
			$nopic = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$m[1]."/img/nopic.png";
			$url = $nopic; 
			list($sWidth, $sWidth) = getimagesize($url);		
			$redirctUrl = $_SERVER['REQUEST_URI']; 
			$redirctUrl = preg_replace("/url=([^&$]+)/im", "url=".urlencode($url), $redirctUrl);
			header("Location: " . $redirctUrl); die;
			die;
		}

		$ratio = $sWidth/$sHeight;
		if($sw==0 && $sh==0){
			$sw = $sWidth;
			$sh = $sHeight;
		}else{
			if($sw==0) $sw = $sh*$ratio;
			if($sh==0) $sh = $sw/$ratio;
		}
		$ratio = $sw/$sh;		
		if($w<0 && $h<0){
			$w *=-1; $h *=-1;
			if($sWidth >$sHeight)
				$h=-1;
			else
				$w=-1;
		}
		if($w<1){ $w = $h * $ratio; }
		if($h<1){ $h = $w / $ratio; }
		$img = @imagecreatetruecolor($w, $h);
		$bgclr = @imagecolorallocate($img, 255,255,255);
		@imagefill($img, 0, 0, $bgclr);
		$image = $this->imagecreatefrom($url);
		if(null != $image){
			imagecopyresampled($img, $image['img'], 0, 0, $sx, $sy, $w, $h, $sw, $sh);
			imagedestroy($image['img']);
			
			$thumbPath = $this->createPath($thumbFile);
			Imagejpeg($img, $thumbFile);
		}
		return $img;
	}
	/**
	 * 下载远程图片
	 * @param  [type] $url [description]
	 * @return [type]      [description]
	 */
	private function download($url){
		$filename = hash("sha1", "$url");		
		if(preg_match("/^.*(\.\w+)$/im", $url, $m)){
			$filename .= $m[1];
		}
		// $path = $_SERVER["DOCUMENT_ROOT"]."/downloadfiles/" . date("Y/m/d",time());
		$path = $_SERVER["DOCUMENT_ROOT"]."/downloadfiles";
		if(preg_match_all("/\w{5}/im", $filename, $mc, PREG_SET_ORDER)){
			foreach($mc as $m){
				$path .=  "/".$m[0];
			}
		}
		$this->createPath($path);
		$filename = $path . "/" . $filename;
		if(is_file($filename))return $filename;

		// 设置脚本高执行最长时间为 60 秒
		$seconds = 60;
		// ini_set("max_execution_time", $seconds);
		set_time_limit($seconds);

		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 执行后信息以文件流形式返回
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); // 强制进行ip4解析

    curl_setopt($ch, CURLOPT_SSLVERSION, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    $content = curl_exec($ch);
		curl_close($ch);
		// 保存文件
		$fp = @fopen($filename, 'a');
		fwrite($fp, $content);
    fclose($fp);

    //file_put_contents($filename, $content);
    if (filesize($filename) ==0 ){
        unlink($filename);
        return null;
    }
		return $filename;
	}
	/**
	 * 创建目录
	 * @param  [type] $p [description]
	 * @return [type]    [description]
	 */
	private function createPath($p){
		if(file_exists($p))return $p;
		$path = $p;
		if(preg_match("/(.*)\/(\w+)\.\w{1,4}/", $path, $m)){
			$path = $m[1];
		}
		preg_match_all("/[^".preg_quote(DIRECTORY_SEPARATOR, "/")."]+/", $path, $mc);
		$root = "";
		foreach ($mc[0] as $k => $m) {
			$root .= DIRECTORY_SEPARATOR.$m;
			if(!file_exists($root)){
				mkdir($root);
			}
		};
		return $root;
	}
	// 创建image对象
	private function imagecreatefrom($url){
		$type = "jpeg";
		$img = @imagecreatefromjpeg($url);
		if(false == $img) {$type = "bmp"; $img = @imagecreatefrombmp($url);}
		if(false == $img) {$type = "gd2"; $img = @imagecreatefromgd2($url);}
		if(false == $img) {$type = "gd2part"; $img = @imagecreatefromgd2part($url);}
		if(false == $img) {$type = "gd"; $img = @imagecreatefromgd($url);}
		if(false == $img) {$type = "png"; $img = @imagecreatefrompng($url);}
		if(false == $img) {$type = "gif"; $img = @imagecreatefromgif($url);}
		// if(false == $img) {$type = "string"; $img = @imagecreatefromstring($url);}
		if(false == $img) {$type = "wbmp"; $img = @imagecreatefromwbmp($url);}
		if(false == $img) {$type = "webp"; $img = @imagecreatefromwebp($url);}
		if(false == $img) {$type = "xbm"; $img = @imagecreatefromxbm($url);}
		if(false == $img) {$type = "xpm"; $img = @imagecreatefromxpm($url);}
		if(false == $img) {$type = "xpm"; $img = @imagecreatefromxpm($url);}
		return !$img?null:['type'=>$type, 'img'=>$img];

	}

}
