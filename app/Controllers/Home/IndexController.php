<?php
namespace App\Controllers\Home;
use App\Controllers\Controller;

class IndexController extends Controller
{
	public function index(){
        //$callback = isset($_GET["callback"])?trim($_GET["callback"]):"callback";//获取方法名称
        $result = json_encode(['server'=>$_SERVER]);
        $url = $_SERVER['SCRIPT_FILENAME'];
        $jsUrl = null;
        $content = null;
        # $partten = "/(.*)\/.+\/\w+\.php.*/im";
        $partten = "/(.*)\\".DIRECTORY_SEPARATOR.".+\\".DIRECTORY_SEPARATOR."\w+\.php.*/im";
        preg_match($partten, $url, $m);
        if( $m ){
            $jsUrl = $m[1]."/js/EtSoftWare.8.0.js";
            $f = fopen($jsUrl, 'rb');
            if($f){
                $ilen = filesize($jsUrl);
                $content = fread($f, $ilen);
                fclose($f);
            }

        }
        $basePath = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        preg_match($partten, $basePath, $m);
        if( $m ){
            $basePath=$m[1];
        }
        $content = str_replace("var basePath=''", "var basePath='$basePath'", $content);
        $content = preg_replace("/var\s+rootPath\s*=\s*['\"](.*?)['\"](\s*)/im", "var rootPath = '$basePath'$2", $content);
        
        echo $content;
	}
    public function ext(){
        $app = $_SERVER['SCRIPT_FILENAME'];
        $extPath = preg_replace("/(.+)\/\w+\/.*$/im", "$1/js/ext", $app);
        $ret = [];
        if(file_exists($extPath)){
            $fs = scandir("$extPath/");
            foreach ($fs as $f){
                if($f == '.' || $f == '..'){continue; }
                if(!preg_match("/(\w+)\.ext\.js/im", $f, $m)){ continue; }
                array_push($ret, $m[1]);
            }
        }
        echo json_encode($ret, true);
    }
}
  