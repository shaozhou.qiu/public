<?php
namespace App\Controllers\Ip;
use App\Controllers\Controller;
require_once (dirname(dirname(__FILE__)).'/Ip/Qqwry.php'); 

class IndexController extends Controller
{
	public function index(){
        $ipv4 = $_GET['ipv4']??null;
        $root = dirname(dirname(dirname(dirname(__FILE__))));
        $db = $root."/db/qqwry.dat";
        $Qqwry = new \Qqwry($db);
        $location = $Qqwry->getlocation($ipv4);
        return $location;
	}
	public function showmyip(){
		echo "You are ip address is".$this->getip();
	}
	public function getip(){
	    if ($_SERVER["HTTP_CLIENT_IP"] && strcasecmp($_SERVER["HTTP_CLIENT_IP"], "unknown")) {
	        $ip = $_SERVER["HTTP_CLIENT_IP"];
	    } else {
	        if ($_SERVER["HTTP_X_FORWARDED_FOR"] && strcasecmp($_SERVER["HTTP_X_FORWARDED_FOR"], "unknown")) {
	            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	        } else {
	            if ($_SERVER["REMOTE_ADDR"] && strcasecmp($_SERVER["REMOTE_ADDR"], "unknown")) {
	                $ip = $_SERVER["REMOTE_ADDR"];
	            } else {
	                if (isset ($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'],
	                        "unknown")
	                ) {
	                    $ip = $_SERVER['REMOTE_ADDR'];
	                } else {
	                    $ip = "unknown";
	                }
	            }
	        }
	    }
	    return ($ip);
	}

}