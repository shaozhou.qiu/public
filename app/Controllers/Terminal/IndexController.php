<?php
namespace App\Controllers\Terminal;
use App\Controllers\Controller;

class IndexController extends Controller
{
	public function index(){
        $args =$_GET;
        $cmd =$_GET['cmd']??null;
        if(!$cmd){ return ;}
        $url = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER['HTTP_HOST']."/".$_SERVER['REQUEST_URI'];
        $url .= "&a=$cmd";
        $url = preg_replace("/&?((callback)|(rmd))=[^&]*/im", '', $url);
        $ret = $this->http_get( $url );
        
        if(!$ret['data']){return ; }
        return $ret['data'] ;

	}
    public function nmap(){
        // $args = $_SERVER['QUERY_STRING'];
        // $args = preg_replace("/[?&]*[^?&=]+=[^?&=]*/im", '', $args);
        $args=[
            $_GET['ip']??"127.0.0.1"
            ,"-p"
            ,$_GET['ports']??"80"
        ];
        // $args = explode("&", $args);
        $this->exec("nmap", $args);
    }
    private function exec($cmd, $args){
        ini_set("max_execution_time", "120");
        set_time_limit(120);
        foreach ($args as $k => $v) {
            $cmd .= " ".$v;
        }
        $cmd = $cmd." ";
        // echo "$cmd\n";
        // echo $cmd; die();
        $reval = system($cmd, $ret);
        // dump(['cmd'=>$cmd,'return'=>$reval, 'result'=>$ret]);
        
    }
}
  
