<?php
// require __DIR__.'/';
require_once( __DIR__."/Controllers/Controller.php" );
$mdl = isset($_GET['m'])?$_GET['m']:'Home';
$cls = isset($_GET['c'])?$_GET['c']:'Index';
$act = isset($_GET['a'])?$_GET['a']:'Index';
$cls .= "Controller";

$ctl = __DIR__."/Controllers/$mdl/$cls.php";
file_exists($ctl) && require_once($ctl);
$cls = "App\\Controllers\\$mdl\\$cls";
if( class_exists($cls) ){
	$class = new $cls();
	$parameter=null;
	$fun=['$1_before', '$1', '$1_after'];
	foreach ($fun as $k => $v) {
		$f = preg_replace("/^(.*)$/im", $v, $act);
		if( method_exists($class, $f) ){
			$ret = call_user_func(array($class, $f), $parameter);
			if(is_array($ret)){
				$ret = json_encode($ret);
			}
			if(isset($_GET["callback"])){
				$ret = jsonp($ret);
			}
			echo( $ret );
		}
	}
}

function jsonp($data){
	$callback = isset($_GET["etCallback"])?trim($_GET["etCallback"]):null;//获取方法名称
	if($callback == null){
		$callback = isset($_GET["callback"])?trim($_GET["callback"]):"callback";//获取方法名称
	}
	$result = json_encode($data);
	return $callback ."($result)";
}


function dump(...$args){
	$e =new Exception();
	$trace = $e->getTrace()[0];
	echo "<fieldset style=\"border: 1px solid #bdbdbd;background: #171717;color: #cecece;\">";
	echo "<legend style=\"background: #171717;padding: 5px 15px;border-radius: 5px;font-weight: bolder;\">dump Tips</legend>";
	if($trace){
		$file =  $trace['file']."(". $trace['line'] ."):". $trace['function'] ."\n";
		echo "<i style=\"color: #00ff0c; \">$file</i>";
		// print_r( $e->getTrace());
	}
	echo "<pre style='background: #000; '>";
	foreach ($args as $k => $v) {
		print_r($v);
	}
	echo "</pre>";
	echo "</fieldset>";
}
function obj2str($obj){
	$t = gettype($obj);
	$reval = $t;
	if( $t == 'string'){
		$reval .= "(". strlen($obj) .")";
		$reval .= " \"$obj\"";
		return $reval;
	}
	if( $t == 'array'){
		$reval .= "(". count($obj) ."){";
		foreach ($obj as $k => $v) {
			$reval .= "\n\t[$k]=>".obj2str($v);
		}
		$reval .= "\n}";
		return $reval;
	}

}