

(function(global, factory){
    factory(global?global:this);
})(window, function(win){
    'use strict'
    // type, url, callback, params
    function getXMLHttpRequest(){
        var xhr;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        return xhr;
    }
    function getData(config){
        var str='';
        if(config.type == 'GET'){
            for(var k in config.data){
                str += (/\?/gi.test(url))?'&':'?';
                str += k+'='+ encodeURI(config.data[k]);
            }
        }else{return null;}
        return str.substr(1);
    }
    function getUrl(config){
        var url = config.url;
        if(config.type == 'GET'){
            for(var k in config.data){
                url += (/\?/gi.test(url))?'&':'?';
                url += k+'='+ encodeURI(config.data[k]);
            }
        }
        return url;
    }
    function Ajax(config){
        if(!config.url)return null;
        if(!config.type)config.type = 'GET';
        if(!config.success)config.success = function(){};
        if(!config.error)config.error = function(){};
        if(!config.complete)config.complete = function(){};
        if(config.async == undefined)config.async =  true;
        var xhr = getXMLHttpRequest();
        xhr.config = config;
        xhr.onreadystatechange=function(){
            if (this.readyState == 4){
                if (this.status == 200){
                    var txt = this.responseText;
                    config.success.call(this, txt);
                }else{
                    config.error.call(this);
                }
                config.complete.call(this);
            }
        }
        xhr.open(config.type, getUrl(config), config.async);
        try{ 
            xhr.send(getData(config)); 
        }catch(e){
            config.complete.call(xhr);
        }
    }
    (!window.Ajax) && (window.ajax=window.Ajax=Ajax);
});


