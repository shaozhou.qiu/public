


(function(global, factory){
    factory(global?global:this);
})(window, function(win){
    'use strict'

    
    var getFrpServerList=function(conf){
        var urls={http:[], https:[]}, ports=[88, 443, 8443, 444];
        for(var i=1; i<10; i++){
            var url = 'centos7.web.home.frp'+i+'.39doo.com';
            for(var j=0; j<ports.length; j++){
                urls.http.push('http://' + url +':'+ports[j] +'/public/app/EtSoftWare.php' );
                urls.https.push('https://' + url +':'+ports[j] +'/public/app/EtSoftWare.php');
            }
        }
        return urls;
    };
    function toLocation(url){
        var m = /(\w+:)\/\/([\w\.]+)(\:(\d+))?([\w\/\.]+)$/gi.exec(url);
        if(m){
            return {
                'hash': ""
                ,'host': m[2]+(m[3]?m[3]:'')
                ,'hostname': m[2]
                ,'href': url
                ,'origin': m[1]+'//'+m[2]+(m[3]?m[3]:'')
                ,'pathname': m[5]
                ,'port': m[4]?m[4]:80
                ,'protocol': m[1]
            };
        }
        return {};
    };

    var FrpServer=function(conf){
        if(conf){
            for(var k in conf)this[k]=conf[k];
            return ;
        }else{
            var urls = {http:[], https:[]}, _me = this;
            var onComplete=function(){
                if( _me.http || _me.https){
                    _me.status = 0;
                    if(  _me.prefun && _me.prefun.length>0){
                        while(_me.prefun.length){
                            var fun = _me.prefun.splice(0,1);
                            fun[0]();
                        }
                    }
                    if(_me.http && _me.https){
                        var v = ''
                        if(_me.http &&  _me.http.href) v+=',"http":"'+_me.http.href+'"';
                        if(_me.https &&  _me.https.href) v+=',"https":"'+_me.https.href+'"';
                        if(v){
                            var c = new LCache('frps');
                            c.set('{'+ v.substr(1) +'}');
                        }
                    }
                }
            }
            var ret = null;
            if(typeof(Cache) != 'undefined'){
                var c = new LCache('frps');
                var ret = c.get();
            }
            if(ret){
                var o = JSON.parse(ret);
                if(o){
                    for(var k in o)urls[k].push(o[k]);
                }
            }else{
                urls = getFrpServerList();
            }
            var _me = this;
            _me.status = -1;
            var comfun = function(){ onComplete(); }
            testfrpsvr(urls.http, function(e){_me.http=toLocation(e.config.url); }, comfun);
            testfrpsvr(urls.https, function(e){_me.https = toLocation(e.config.url); }, comfun);
        }
    };
    FrpServer.prototype.complete=function(fun){
        if(typeof(fun) != "function"){return ;}
        if(this.status==0){fun();return ;}
        this.prefun=this.prefun?this.prefun:[];
        this.prefun.push(fun);
    };
    FrpServer.prototype.setProtocol=function(protocol){
        this.protocol = protocol;
    };
    FrpServer.prototype.getLocation=function(key){
        if(!key){
            if(!this.protocol){
                var m = /(\w+)/gi.exec(location.protocol);
                if(!m)return null;
                key = m[0];
            }else{
                key = this.protocol;
            }
        }
        key = key.toLocaleLowerCase();
        var u = this[key];
        if(!u)return null;
        return u;
    };
    FrpServer.prototype.getApp=function(param){
        var u = this.getLocation();
        if(!u) return null;
        return u.href;
    };
    FrpServer.prototype.getUrl=function(url){
        var u = this.getLocation();
        if(!u) return null;
        var ret = u.protocol+'//'+u.host;
        if(url){
            if(/^\/.*/gi.test(url)) ret += '/';
            ret += url;
        }
        return ret;
    };

    function testfrpsvr(urls, callback, comfun){
        var xhs=[], protocol = 'http:';
        for(var i=0; i<urls.length; i++){
            Ajax({
                url : urls[i]
                ,success : function(e){callback(this, e); }
                ,complete : function(e){comfun(this, e); }
                ,error : function(e){}
            });
        }
    }
    win.FrpServer = FrpServer;

});


