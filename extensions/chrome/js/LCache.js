

(function(global, factory){
    factory(global?global:this);
})(window, function(win){    
    'use strict'


    var LCache=function(key){
        if(!key) key = 'key_'+parseInt(Math.random()*99999).toString(16);
        this.key = key
    };
    LCache.prototype.get=function(){
        return localStorage.getItem(this.key);
    };
    LCache.prototype.set=function(v){
        localStorage.setItem(this.key, v);
        return this;
    };

    // console.log(LCache);
    (!win.LCache) && (win.lcache=win.LCache=LCache);
});


