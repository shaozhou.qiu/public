var comm = new Communication();

var getCurrentTab=function(callback){
    var tab = null;
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        var tab = tabs[0];
        callback.call(tab);
    });
    return tab;
}

function GuestUrl(){
    getCurrentTab(function(tab){
        var url= this.url?this.url:''
        , re = '^(.*)(.+)=([^&]+)$'
        , m = new RegExp(re, 'gi').exec(url);
        if(m){
            url = m[1] + m[2]+'=';
            url += (/^\d+$/gi.test(m[3]))?'\\d':'\\w';
            url += '{'+ m[3].length +'}';
        }
        function addLst(obj, txt){
            if(!txt)return null;
            var ul = $(obj).find('>ul');
            if(ul.length<1){
                ul = document.createElement('ul');
                $(obj).append(ul);
            }else{ul=ul[0]; }
            var li = document.createElement('li');
            $(li).html(txt);
            $(ul).append(li);
            $(obj).scrollTop($(obj)[0].scrollHeight)
            return li;
        }
        function getWord(num){
            var val='';
            for(var i=0; i<num;i++){
                var rnd = Math.random()*3;
                if(rnd>2){
                    val += getInter(1);
                }else if(rnd>1){
                    val += String.fromCharCode(65 + parseInt(Math.random()*25));
                }else{
                    val += String.fromCharCode(97 + parseInt(Math.random()*25));
                }
            }
            return val;
        }
        function getInter(num, min, max){
            var val='';
            min = min?min:0; max = max?max:9;
            for(var i=0; i<num;i++){
                val += min + parseInt(Math.random()*(max-min));
            }
            return val;
        }
        function createUrl(url){
            if(!url)return null;
            var num=1, val='', restr='\\\\(w|d)((\\{)(\\d+)(\\}))?'
            ,m = (new RegExp(restr, 'gi')).exec(url);
            if(!m){return null; }
            if(m[4]){ num = m[4]*1; }
            if(m[1]=='d'){
                val=getInter(num);
            }else{
                val=getWord(num);
            }
            url = url.replace(new RegExp(restr, 'gi'), val);

            return url;
        }
        var obj = $('.guesturl')
        ,list = obj.find('.list');
        obj.find('[name=url]').val(url);
        obj.find('.btnStart').click(function(){
            var _me=this, url = obj.find('[name=url]').val();
            if(_me.start){
                _me.start = false;
                this.value = 'Guess';
                addLst(list, 'stop.');
                return ;
            }
            addLst(list, 'guest...');
            this.value = 'Stop';
            function guest(){
                if(!_me.start)return ;
                var nurl = createUrl(url),
                li = addLst(list, nurl);
                $.ajax({
                    url:nurl
                    ,success:function(e){
                        guest();
                        if(!e){ $(li).remove(); return ;}
                        $(li).css('color', 'green');
                    }
                    ,error:function() {
                        $(li).remove();
                        guest();
                    }
                });
            }
            _me.start = true;
            for(var i=0; i<10; i++){ guest(); }
            
        });
    });

}

function AutoRefresh(){
    // autorefresh_list
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        var div = document.createElement('div');
        for(var i=0; i<tabs.length; i++){
            var tab = tabs[i]
            ,itm = document.createElement('div')
            ,img = document.createElement('img')
            ,h5 = document.createElement('h5')
            ;
            if(tab.favIconUrl){
                img.src = tab.favIconUrl;
            }else{
                img.src = '../img/ico.16x16.png';
                img.style.opacity="0.1";
            }
            img.style.height = "32px";

            h5.innerHTML = tab.id;

            itm.id=tab.id;
            itm.title=tab.title;
            itm.className="col-xxxs-2 text-center";
            if(tab.active){
                itm.className = itm.className+' selected';
            }
            itm.onclick=function(){
                if(this.className.indexOf('selected')>-1){
                    this.className = this.className.replace(' selected','');
                }else{
                    this.className = this.className+' selected';
                }
            }
            itm.append(img);
            itm.append(h5);
            div.append(itm);
        }
        hdiv_autorefresh_list.append(div);
        hipt_autorefresh_start.onclick=function(){
            if(this.tids){
                for(var i=0; i<this.tids.length; i++){
                    clearInterval(this.tids[i]);
                }
            }
            this.tids=[];

            var time = hipt_autorefresh_time.value, ts=0;
            var sids=[];
            var divs = hdiv_autorefresh_list.getElementsByClassName('selected');
            if(divs.length<1){ alert('Please select the tab ！') ;return ;}
            this.tids.push(setInterval(function(){
                ts++; if(ts>time){ts = 0;}
                hdiv_autorefresh_info.innerHTML = 'Start counting down time ' + (time-ts) 
            }, 1000))
            for(var i=0; i<divs.length; i++){
                var tid = divs[i].id * 1;
                this.tids.push(setInterval(function(){
                    ts=0;
                    comm.sendTabMsg(tid, {event:'refresh', errcode:0, data:'start reload.'});
                }, time*1000));
            }
        }
    });    
}

new Vue({
    el : '#tools'
    , meths : {
        unlockContextmenu:function(){
            document.body.onselectstart = document.body.oncontextmenu = function(){ return true; }
        }
    }
});


window.onload=function(){
    GuestUrl();   
    AutoRefresh();
}