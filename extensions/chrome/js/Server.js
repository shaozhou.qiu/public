

(function(global, factory){
    factory(global?global:this);
})(window, function(win){
    'use strict'
    var frps = new FrpServer();
    frps.complete(function(){
        frps.setProtocol('http');
    });
    
    function Server(){
        var comm = new Communication();
        comm.onMessage('frps', function(message, sender){return frps; });
        comm.onMessage('getet', function(message, sender){return getEtCode(sender.url); });
        comm.onMessage('getetf', function(message, sender){
            if(!message.data)return '';
            var code = '';
            Ajax({
                url : message.data
                ,async : false
                ,success : function(e){code = e; }
            });
            return code;
        });
    }


    function getEtCode(protocol){
        var code =  getEtCodeByFrp(); 
        if(protocol && protocol.indexOf('https')>-1){
            var l = frps.https;
            if(l){
                code = code.replace(/(var\s*rootPath\s*=\s*')((http|https):\/\/[^\/]+)(.*)/gi, "$1"+l.origin+"$4");
            }
        }
        return code;
    }
    function getEtCodeByFrp(){
        var code = '';

        Ajax({
            url : frps.http.href
            ,async : false
            ,success : function(e){
                code = e;
            }
        });
        return code;
    }


    (!window.Server) && (window.Server=Server());
});