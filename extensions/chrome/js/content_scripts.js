
(function(global, factory){
    factory(global?global:this);
})(window, function(win){
    'use strict'
    var excludeDoname = ['39doo.com'];
    var frps = null, checkTimes=0;
    var comm = new Communication(true);
    var ent = null;
    comm.sendMessage('frps', function(e){frps = new FrpServer(e);});

    comm.onMessage('onEtMenu', function(message, sender){
    	if(!ent)return ;
        var el = ent.srcElement;
        var cels = " thead tfoot tbody td tr dl dd dt li";
        while(cels.indexOf(el.tagName.toLowerCase())>-1){
            el = el.parentElement;
        }
        addAttrEt(el);
    });
    

    function addAttrEt(el){
        var attr = document.createAttribute('et');
        el.attributes.setNamedItem(attr);
    	ET(el);
        ET(el);
        setTimeout(function(){el.removeAttribute('et');}, 100);
    }
    function isExclude(){
    	for(var i=0; i<excludeDoname.length; i++){
    		if(location.hostname.indexOf(excludeDoname[i])>-1)return true;
    	}
    	return false;
    }

    function main(){
    	if(!frps){
    		if(checkTimes++>10)return ;
    		setTimeout(function(){ main(); }, 100);
    		return ;
    	}
    	if(!isExclude() && typeof(ET)=='undefined'){ 
    		loadETByServer(comm, frps); 
    	}

    	win.onmousedown=function(){ent = event; }
    	
    }

    main();
});


