var cache={};

function loadETByServer(comm, frps){
	var preload = [
		// 'http://centos7.web.home.frp6.39doo.com:88/public/js/lib/tablelib.lib.js'
		'/public/js/lib/tablelib.lib.js'
		, '/public/js/lib/messagebox.lib.js'
		, '/public/js/lib/object.lib.js'
		, '/public/js/lib/array.lib.js'
	];
	function getFile(url, callback, conf){
		if(frps.https && url.indexOf(frps.https.host)>-1){
			url = url.replace(frps.https.host, frps.http.host);
			url = url.replace(frps.https.protocol, frps.http.protocol);
		}
		!callback && (callback=function(){});
		if(!cache.f)cache.f={};
		if(cache.f[url]){callback(cache.f[url]);return cache.f[url];}
		comm.sendMessage({
			action:'getetf'
			,data: url
		}, function(e){
			cache.f[url] = e;
			callback(e);
		});
		return null;
	}
	comm.sendMessage('getet', function(e){
		code = e+'';
		code = code.replace(/([^\w]+getFile\s*:\s*function.*\{\s*)/gi, "$1\n return getFileFormServer.apply(this, arguments);\n");
		var protocol = location.protocol;
		var l = frps[protocol.replace(/[^\w]/gi, '')];
		if(l) code = code.replace(/(var\s*rootPath\s*=\s*')((http|https):\/\/[^\/]+)(.*)/gi, "$1"+l.origin+"$4");
		new Function("getFileFormServer", code)(getFile);

		preload.forEach(function(v){
			getFile(frps.http.protocol+'//'+frps.http.host+v, function(e){
				var m = /.*\/(.+)\.lib.js/gi.exec(v);
				if(m){
					ET[m[1]] = ET.execJS(e);
				}
			}, null);
		});
	});		
}

//页面中添加script
function appendScript(url){
	if(!url)return;
	var spt = document.createElement('script');
	spt.type="text/javascript";
	spt.src=url;
	document.body.appendChild(spt);
}


