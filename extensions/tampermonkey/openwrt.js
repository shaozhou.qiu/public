// ==UserScript==
// @name         openwrt 1.0
// @namespace    http://www.39doo.com/
// @version      0.1
// @description  try to take over the world!
// @author       Rimke
// @match        http://*/cgi-bin/luci/admin/services/shadowsocksr/client
// @match        http://*/cgi-bin/luci/admin/services/shadowsocksr

// @require      http://centos7.web.home.frp6.39doo.com:88/public/js/jquery-3.4.1.js
// @require      http://centos7.web.home.frp6.39doo.com:88/public/extensions/tampermonkey/openwrt.js
// @grant           unsafeWindow
// @grant           GM_setValue
// @grant           GM_getValue
// @grant           GM_log
// @grant           GM_deleteValue
// @grant           GM_listValues
// @grant           GM_info
// @grant           GM_xmlhttpRequest
// @connect      *

// ==/UserScript==

//(function() {
//    (new OpenWrt).run();
//})(); 

(function() {
    'use strict';
    var idx=0, OpenWrt = function(){};
    var waiteTime = 1500;
    //start app
    OpenWrt.prototype.run=function(){
        if (window.top != window.self) return;
        var ifm = reRenderView(function(){
            var _me = this;
            var act = this.contentDocument.getElementsByClassName('cbi-page-actions')[0];
            var ipt = document.createElement('input');
            ipt.type = "button";
            ipt.value = "自动选择";
            ipt.onclick=function(){startAutoSelectNode(_me);}
            act.append(ipt);            
        });
    }
    function startAutoSelectNode(ifm){
        var fun=ifm.onload
        ifm.onload=function(){
            if( confirm('是否从第一个开始检测') ){
                idx = 1;
            }else{
                var slt = this.contentDocument.getElementsByTagName('select')[0];
                idx = slt.selectedIndex+1;
            }
            ifm.onload=fun;
            chgNodeAndApply(ifm);
        };
        ifm.src='/cgi-bin/luci/admin/services/shadowsocksr';
    }
    function chgNodeAndApply(ifm){
        if(!chgNode(ifm, idx++)){
            alert('finish!');return;
        }
        if(!applyNode(ifm)){
            idx--;
            chgTabs(ifm, 0);
            setTimeout(function(){ chgNodeAndApply(ifm); }, waiteTime);
            return ;
        }
        setTimeout(function(){
            var tid = setInterval(function(){
                var txt = getApplyStatus(ifm);
                if(txt.match(/配置已应用/gi)){
                    clearInterval(tid);
                    chgTabs(ifm, 5);
                    setTimeout(function(){
                        testGoogle(ifm, function(e){
                            if(e.match(/正常/gi))return;
                            chgTabs(ifm, 0);
                            var tid = setInterval(function(){
                                if(ifm.contentDocument.getElementsByTagName('select').length<3)return;
                                clearInterval(tid);
                                chgNodeAndApply(ifm);
                            }, 300);
                        });
                    }, waiteTime+300);
                }
            }, 300);
        }, waiteTime);
    }
    function testGoogle(ifm, cb, n=0){
        // ifm.contentWindow.check_connect(this,'google');
        var btn = ifm.contentDocument.getElementsByClassName('cbi-button')[0];
        if(!btn) {
            if(n>50)return false;
            setTimeout(function(){
                testGoogle(ifm, cb, n++);
            }, 500);
            return false;
        }
        btn.click();
        var tid = setInterval(function(){
            if(btn.value.match(/正在/gi))return;
            clearInterval(tid);
            var stat = ifm.contentDocument.getElementById('google-status');
            cb(stat?stat.innerText:'');
        }, 300);
        return true;
    }
    function chgTabs(ifm, idx){
        var tabs = ifm.contentDocument.getElementsByClassName('tabs')[0];
        var tab = tabs.children[idx];
        tab.getElementsByTagName('a')[0].click();
    }
    function getApplyStatus(ifm){
        var stat = ifm.contentDocument.getElementById('cbi-apply-shadowsocksr-status');
        return stat?stat.innerText:"";
    }
    function getStatus(ifm){
        var stat = ifm.contentDocument.getElementById('shadowsocksr_status');
        return stat?stat.innerText:'';
    }
    function applyNode(ifm){
        var btn = ifm.contentDocument.getElementsByName('cbi.apply')[0];
        if(!btn)return false;
        btn.click();
        return true;
    }
    function chgNode(ifm, idx){
        var slt = ifm.contentDocument.getElementsByTagName('select')[0];
        if(!slt){return false;}
        var sum = slt.children.length;
        if(!(idx<sum)){return false;}
        slt.value = slt.options[idx].value;
        return true;
    }
    function reRenderView(fun){
        var ifm = document.createElement('iframe');
        ifm.style.width='100%';
        ifm.style.height='100%';
        ifm.style.border='0';
        if(fun)ifm.onload=fun;
        
        var html = document.getElementsByTagName('html')[0]
        html.style.height='100%';
        document.body.style.height='100%';
        document.body.style.padding='0';
        document.body.style.overflow='hidden';
        ifm.src = location.href;
        document.body.innerHTML='';
        document.body.append(ifm);
        var html = document.getElementsByTagName('html')[0];
        html.style.overflow='hidden';
        return ifm;
    }


    if(!window.OpenWrt){window.OpenWrt = OpenWrt; }
    // (new OpenWrt).run();
})();
