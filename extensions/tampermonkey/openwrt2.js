// ==UserScript==
// @name         openwrt 1.0
// @namespace    http://www.39doo.com/
// @version      0.1
// @description  try to take over the world!
// @author       Rimke
// @match        http://*/cgi-bin/luci/admin/services/shadowsocksr/client
// @match        http://*/cgi-bin/luci/admin/services/shadowsocksr

// @require      http://centos7.web.home.frp6.39doo.com:88/public/js/jquery-3.4.1.js
// @require      http://centos7.web.home.frp6.39doo.com:88/public/extensions/tampermonkey/openwrt.js
// @grant           unsafeWindow
// @grant           GM_setValue
// @grant           GM_getValue
// @grant           GM_log
// @grant           GM_deleteValue
// @grant           GM_listValues
// @grant           GM_info
// @grant           GM_xmlhttpRequest
// @connect      *

// ==/UserScript==

//(function() {
//    (new OpenWrt).run();
//})(); 

// (function() {
    'use strict';
    var OpenWrt=function(){}
    , tools={
        "/cgi-bin/luci/admin/services/shadowsocksr":[
            {
                el:{
                    'tagname':'input'
                    ,'type':'button'
                    ,'value':'自动'
                    ,onclick:function(){
                        function testNode(){
                            var ifm=$('iframe');
                            if(ifm.length<1)return;
                            ifm=ifm[0];
                            var url = ifm.contentWindow.location.href;
                            if(!/.*\/client/gi.test(ifm.contentWindow.location.href)){
                                ifm.loaded=function(){ setTimeout(function(){ ifm.loaded=null; testNode(); }, 500); }
                                tab.set(0, ifm);
                                return ;
                            }
                            var svr = $(ifm.contentDocument).find('select[id]'), el=null;
                            for(var i=0; i<svr.length;i++){
                                if(/.*\.global_server$/gi.test(svr[i].id)){
                                    el = svr[i];break
                                }
                            }
                            if(!el)return;
                            if(el.selectedIndex >= (el.options.length-1)){
                                console.log('已经到了最后一个！');return;
                            }
                            el.selectedIndex++;
                            var btnApply = $(ifm.contentDocument).find('[name=cbi\\.apply]');
                            btnApply.click();
                            var tid=setInterval(function(){
                                var stat = $(ifm.contentDocument).find('#cbi-apply-shadowsocksr-status');
                                if(/.*配置已应用.*/gi.test(stat.text())){
                                    clearInterval(tid);
                                    testgoogle(ifm, function(e){
                                        if(e)return;
                                        testNode();
                                    });
                                }
                            }, 800);
                        }
                        function testgoogle(ifm, cb){
                            if(!/.*\/status/gi.test(ifm.contentWindow.location.href)){
                                ifm.loaded=function(){ setTimeout(function(){ ifm.loaded=null; testgoogle(ifm, cb); }, 500); }
                                tab.set(5, ifm);
                                return ;
                            }
                            var tgoogle = $(ifm.contentDocument).find('#cbi-Version-1-google > div > input');
                            tgoogle.click();
                            setTimeout(function(){
                                var tid=setInterval(function(){
                                    var tgoogle = $(ifm.contentDocument).find('#cbi-Version-1-google > div > input');
                                    if(/.*(正在).*/gi.test(tgoogle.val())){return; }
                                    clearInterval(tid);

                                    var tgstatus = $(ifm.contentDocument).find('#google-status');
                                    console.log(tgstatus.text());
                                    if(/.*(正常)|(成功).*/gi.test(tgstatus.text())){
                                        cb(true);
                                    }else{
                                        cb(false);
                                    }
                                }, 500);
                            }, 2000);

                        }

                        testNode();

                    }
                }
                ,to:"#maincontent > form > div.cbi-page-actions"
            }
        ]
    }, tab={
        set:function(n, el){
            var tabs=null;
            if(el){
                if(el.tagName=='IFRAME')el=el.contentDocument
                tabs=$(el).find('.tabs');
            }else{
                tabs=$('.tabs');
            }
            if(!tabs || tabs.length<1)return;
            var li=tabs.find('li').eq(n);
            li.click();
            li.find('a').click();
            li.find('a')[0].click();
        }
    };
    function createtool(ifm, tls){
        for (var k in tls) {
            var tool = tls[k];
            var el = tool.el;
            var e=document.createElement(el.tagname);
            for(var attr in el){
                e[attr] = el[attr];
            }
            var doc=$(ifm.contentDocument);
            doc.find(tool.to).append(e);

        }
    }
    OpenWrt.prototype.run=function(){
        if(!isOpenwrtSystem() || top != self)return;
        var _me=this,ifm = createFrame();
        ifm.loadtools=function(){_me.tools(this);};
        document.write("<html><head></head><body style=\"margin: 0px; padding: 0px; height: 100%; width: 100%; overflow: hidden; \"> </body></html>");
        document.body.appendChild(ifm);
    }
    OpenWrt.prototype.tools=function(ifm){
        var pathname = ifm.contentWindow.location.pathname;
        var tls = tools[pathname];
        if(!tls){
            for (var k in tools) {
                var re = new RegExp('.*'+k+'/.*', "gi");
                if( re.test(pathname) ){
                    tls = tools[k];
                    break;
                }
            }
        }
        if(!tls)return;
        createtool(ifm, tls);
    }
    
    function listenPage(){

    }

    function createFrame(callback){
        var ifm = document.createElement('iframe');
        ifm.style.width="100%";
        ifm.style.height="100%";
        ifm.style.margin="0px";
        ifm.style.padding="0";
        ifm.style.border="0px";
        var chgaddr = function(){
            if(!window.history.pushState){return false;}
            top.window.document.title=this.contentDocument.title
            return top.window.history.pushState(
                {},
                this.contentDocument.title,
                this.src
            );
        }
        ifm.chgaddr=chgaddr;
        ifm.loadtools = function(){}
        ifm.loaded = function(){ callback && callback.call(this); }
        ifm.onload=function(){
            this.chgaddr();
            this.loadtools();
            if(typeof(this.loaded)== 'function'){
                this.loaded();
            }
        };
        ifm.src=location.href;
        return ifm;
    }
    function isOpenwrtSystem(){
        var footer = document.getElementsByTagName('footer');
        if(footer.length<1)return false;
        if(/.*OpenWrt.*/gi.test(footer[0].innerText))return true;
        return false;
    }
// })();
