// ==UserScript==
// @name         facebook 1.0
// @namespace    http://www.39doo.com/
// @version      0.1
// @description  try to take over the world!
// @author       Rimke
// @include      https://*.facebook.com/*

// @require      http://centos7.web.home.frp6.39doo.com:88/public/extensions/tampermonkey/ET.mini.js
// @require      http://centos7.web.home.frp6.39doo.com:88/public/extensions/tampermonkey/facebook.js
// @grant           unsafeWindow
// @grant           GM_setValue
// @grant           GM_getValue
// @grant           GM_log
// @grant           GM_deleteValue
// @grant           GM_listValues
// @grant           GM_info
// @grant           GM_xmlhttpRequest
// @connect      *

// ==/UserScript==

(function() {
    'use strict';
    var facebook = function(){
        var _me = this;
        ET.floatMenu({
            'Collection Group':function(){_me.collectionGroup();}
        });
    };
    facebook.prototype.collectionGroup=function(){
        var _me = this;
        ET.messagebox.show('<p>Group URL or ID</p><div><textarea style="width:80%"></textarea></div>', function(){
            var txt = this.getElementsByTagName('textarea')[0].value;
            var mc = txt.match(/(.+)/gi);
            if(!mc)return;
            var urls =[];
            for (var i = 0; i < mc.length; i++) urls.push(_me.getGroupUrl(mc[0]));
        });
    }
    // 获取群地址
    facebook.prototype.getGroupUrl=function(url){
        if(/\s*https:\/\/.*\/groups\/.+/gi.test(url))return url;
        if(/\w+/gi.test(url))return "https://www.facebook.com/groups/"+url+"/";
    }

    if(!window.Facebook){window.Facebook = new facebook(); }    
})();
