// ==UserScript==
// @name         Amazon 1.0
// @namespace    http://www.39doo.com/
// @version      0.1
// @description  try to take over the world!
// @author       Rimke
// @include      https://*.amazon.com/sp*

// @require      http://centos7.web.home.frp6.39doo.com:88/public/js/jquery-3.4.1.js
// @require      http://centos7.web.home.frp6.39doo.com:88/public/extensions/tampermonkey/amazon.js
// @grant           unsafeWindow
// @grant           GM_setValue
// @grant           GM_getValue
// @grant           GM_log
// @grant           GM_deleteValue
// @grant           GM_listValues
// @grant           GM_info
// @grant           GM_xmlhttpRequest
// @connect      *

// ==/UserScript==

//(function() {
//    (new Amazon).start();
//})();

(function() {
    'use strict';
    var Amazon = function(){}
    //启动
    Amazon.prototype.start=function(){
        var lp = location.pathname;
        if(lp=="/sp"){ this.showSellerExt(); }
        console.log("Amazon start");
    }
    Amazon.prototype.showSellerExt=function(){
        this.getSellerExt(function(dat, copinfo){
            var ul = $("#seller-profile-container > div.a-row.a-spacing-medium > div > ul");
            function cli(tit, txt){
                var li = document.createElement('li');
                var spn = document.createElement('span');
                var spnt = document.createElement('span');
                spn.className="a-list-item";
                spnt.className="a-text-bold";
                $(spnt).html(tit+":");
                spn.appendChild(spnt);
                $(spn).append(txt);
                li.appendChild(spn);
                return li;
            }
            var resultList = dat.CompanyInformation.result.resultList;
            if(!resultList)return;
            var ci = resultList[0];
            debugger;
            ul.append(cli('Company Name', ci.entName));
            ul.append(cli('legalPerson', ci.legalPerson));
            ul.append(cli('Scope', ci.scope));
            ul.append(cli('domicile', ci.domicile));
            ul.append(cli('Status', ci.openStatus));
            ul.append(cli('validityFrom', ci.validityFrom));
            ul.append(cli('regNo', ci.regNo));
            if(copinfo){
                var copi = copinfo.result;
                ul.append(cli('email', copi.email));
                if(copi.emailinfo){
                    for(var i=0; i<copi.emailinfo.length; i++){
                        var d = copi.emailinfo[i];
                        ul.append(cli(d.desc+'email', d.email));
                    }
                }
                if(copi.phoneinfo){
                    for(var i=0; i<copi.phoneinfo.length; i++){
                        var d = copi.phoneinfo[i];
                        ul.append(cli(d.desc+'phone', d.phone));
                    }
                }

            }
        });

    }
    Amazon.prototype.getSellerExt=function(cb){
        var bn = this.getBusinessName();
        var bbnn=bn.replace(/(.*:)|(Co.,\s*Ltd)|(\-.*)/gi,'');
        bbnn=bbnn.replace(/Network\s*Technology/gi,'wang luo ke ji');
        bbnn=bbnn.replace(/Technology/gi,'ke ji');
        bbnn=bbnn.replace(/Electronic/gi,'dian zi');
        bbnn=bbnn.replace(/Group/gi,'ji tuan');
        bbnn=bbnn.replace(/Brand/gi,'pin pai');
        bbnn=bbnn.replace(/Trading/gi,'mao yi');
        bbnn=bbnn.replace(/Clothing/gi,'fu shi');
        bbnn=bbnn.replace(/Co\s*.\s*,\s*Ltd/gi,'you xian gong si');
        var dat = {
            BusinessName:bn
            ,CompanyInformation:{}
        }
        company(bbnn, function(e){
            dat.CompanyInformation = e;
            companyInfo(e, function(e){
                debugger;
                cb && cb(dat, e);
            });
        });

    }
    Amazon.prototype.getBusinessName=function(){
        var addr = $('#seller-profile-container > div.a-row.a-spacing-medium > div > ul > li:nth-child(1) > span');
        if(!addr || addr.length<1)return ;
        var bn=addr[0].innerText;
        return bn;
    }

    if(!window.Amazon){window.Amazon = Amazon; }


    function companyInfo(data, cb){
        var resultList = data.result.resultList;
        if(!resultList)return;
        var ci = resultList[0];
        var flag = "cop_"+ci.pid;
        var ret = null;
        ret = GM_getValue(flag, null);
        if(ret){
            ret =  new Function('return '+ret+';')();
            cb && cb(ret);
            return ret;
        }        
        ajax({
              method: "post",
              url: "http://centos7.vmkali.ys.vip.frp.wlphp.com:88/public/app/EtSoftWare.php?m=Shell",
              data:"cmd="+encodeURIComponent('curl -s -k -L -b "cookie.tmp" -c "cookie.tmp" https://aiqicha.baidu.com/company_detail_'+ci.pid),
              onload: function(response){
                  var text=response.responseText;
                  var ret = pageData(text);
                  if(ret && ret.obj && ret.obj.result){
                    GM_setValue(flag, ret.text);
                  }
                  cb(ret.obj);
              }
        });
    }
    function company(businessName, cb){
        debugger;
        var ret = null;
        ret = GM_getValue(businessName, null);
        if(ret){
            ret =  new Function('return '+ret+';')();
            cb && cb(ret);
            return ret;
        }
        ajax({
          method: "post",
          url: "http://centos7.vmkali.ys.vip.frp.wlphp.com:88/public/app/EtSoftWare.php?m=Shell",
          data:"cmd="+encodeURIComponent('curl -s -k -L -b "cookie.tmp" -c "cookie.tmp" "https://aiqicha.baidu.com/s?t=0&q='+encodeURIComponent(businessName)+'"'),
          
          onload: function(response){
            debugger;
              var text =response.responseText;
              if(!text)return ;
              ret = pageData(text);
              if(!ret)return ;
              if(ret.obj && ret.obj.result.resultList){
                GM_setValue(businessName, ret.text);
              }
              cb && cb(ret.obj);
          }
        });
        return ret;
    }
    
    function pageData(text){
        // var m = /(window.pageData.*\}\}\}\};)/gi.exec(text);
        var m = /window.pageData\s*=\s*(.*);[^;]*\s*window.isSpider/gi.exec(text);
        if(!m){ return ;}
        var c = m[1];
        c = c.replace(/\\\\/gi, "\\");
        c = c.replace(/\\\"/gi, "\"");
        var ret =  new Function('return '+c+';')();
        return {obj:ret, text:c};
    }
    function ajax(conf){
        var defConf={
            method: "get",
            headers: {"Content-Type": "application/x-www-form-urlencoded;charset=utf-8"},
            onload: function(response){},
            onerror: function(response){console.log("请求失败"); }
        }
        GM_xmlhttpRequest($.extend(defConf, conf));
    }


})();