/*
*
*	EtSoftware Beta 8.0.1.0
*	Release date: 2020-05-23
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*/
(function(global, jQuery, factory){
    var rootPath = '/public';
    var callback=null,css = Array.prototype.filter.call(document.styleSheets, function(v){ return /.*Etsoftware.*\.css$/gi.test(v.href) });
    if(css.length==0){
        callback=function(){
            ET('html>head')[0].ce('link',{
                "rel":"stylesheet"
                ,"href":rootPath+"/css/Etsoftware6.1.css"
            });
        }
    }
    factory(global, jQuery, rootPath, callback);

}(window, window.jQuery?jQuery:null, function(global, $, rootPath, callback){
    var 
    // debug=true
    debug=window.debug?window.debug:false
    ,_cache = {}
    , doc = document
    , exts = null
    ,ET=function(selector, context){
        if(typeof(selector) == 'function'){
            loadDepends(selector.toString());
            return selector.call(context);
        }
        var selector = $(selector, context);
        selector.each(function(){
            isHTMLNode(this) && EElement.init(this);
        });
        return selector;
    }
    , isHTMLNode=function(e){
        return (e instanceof Node || e instanceof Element || e instanceof HTMLElement)?true:false;   
    }
    // 加载依赖
    , loadDepends = function(code){
        if(!code || typeof(code) !== 'string') return false;
        var restr='[^\\w]ET\\.(\\w+)'; mc = code.match(new RegExp(restr, 'gi'));
        if(mc){
            for(var i=0; i<mc.length; i++){
                var m = (new RegExp(restr, 'gi')).exec(mc[i]);
                ET.loadLib(m[1]);
            }
        }
    }
    , serialization = function(obj){
        var txt = '', toString=function(v){
            v = v.replace(/\\/gi, '\\\\');
            v = v.replace(/(["'])/gi, '\\$1');
            v = v.replace(/\r/gi, '\\r');
            v = v.replace(/\n/gi, '\\n');
            return v;
        };
        if(obj instanceof Array ){
            txt += '[';
            for(var i=0; i<obj.length; i++){
                var v = toString(serialization(obj[i]));
                txt += '"'+v+'",';
            }
            txt=txt.length>2?txt.substr(0,txt.length-1):txt;
            txt += ']';
        }else if(typeof(obj)=='function'){
            return obj.toString();
        }else if(typeof(obj)=='object'){
            txt += '{';
            for (var k in obj) {
                var v = obj[k];
                if(typeof(v)=='string'){
                    txt += '"'+k+'":"' + toString(v) +'",';
                }else{
                    txt += '"'+k+'":' + serialization(v) +','; 
                }
            }
            txt=txt.length>2?txt.substr(0,txt.length-1):txt;
            txt += '}';
        }else{
            txt = obj;
        }
        return txt;

    }
    , unserialization = function(txt){return ET.execJS(txt);}
    , WLS = function(k){
        if(typeof(window.localStorage)!='object'){ return null; }
        var LS = function(k){this.key = 'ET_'+k;}
        LS.prototype.get=function(){
            var data = ET.execJS(localStorage.getItem(this.key));
            if(!data)return null;
            if(data.dataType == 'object'){
                return unserialization(data.value);
            }
            return data.value;
        }
        LS.prototype.set=function(v){
            var t = typeof(v);
            return localStorage.setItem(this.key, serialization({
                    dataType : t
                    ,value : (t=='object')?serialization(v):v
                }));
        }
        LS.prototype.delete=function(v){
            if(v){delete localStorage[v]; }else{
                var ks=[], _me = this;
                for(var i=0; i<localStorage.length; i++){
                    var k = localStorage.key(i);
                    if((new RegExp('^'+this.key, 'gi')).test(k)){ks.push(k); }
                }
                ks.map(function(k) { _me.delete(k); })
            }
        }
        return new LS(k);
    }
    , cache = function(k, defval){
        if(!_cache[k]){_cache[k] = {_listen:{}}; }
        var obj = _cache[k];
        return $.extend(obj, {
            get:function(k){ 
                var ret = this[k];
                if(ret === undefined && defval){
                    ret = (typeof(defval)=='function')?defval():defval;
                    this.set(k, ret);
                }
                return ret;
            }
            ,set:function(k, v){
                if(!k)return;
                this[k] =  v; 
                this._listen[k] = 1;
            }
            ,listen:function(k, callback){
                var _me=this, conf={set:function(){ callback(_me.get(k)); }};
                Object.defineProperty(this._listen, k, conf);
            }
        });
    }
    // 移除注释
    , removeComments = function(code){
        return (typeof(code)!='string')?code:excludeString(code, /\\[\\"'\/]/gi, function(code){
            return excludeString(code, /"[^"]*"/gi, function(code){
                return excludeString(code, /'[^']*'/gi, function(code){
                    return excludeString(code, /[^\*\/]\/[^\*\/\r\n][^\/\n\r]+?\//gi, function(code){
                        code = code.replace(/(\/\*[\w\W]*?\*\/)|(\/\/.*)/gi, '').replace(/[\r\n]{2,}/gi, '\n');
                        return code;
                    });
                });
            });
        });
    }
    // 排除内容
    ,excludeString = function(code, re, callback){
        if(!code || typeof(code) != "string" || !re){return callback?callback(code):code;}
        var mc = code.match(re)
        , reSympol = '^\\.-*()[]{}|'
        , f = ['{{__mc' + parseInt(Math.random()*9999) + '_' ,'__}}']
        , relab = "("+reSympol.replace(/(.)/gi, '(\\$1)|') + "(\\$))";
        if(mc == null){
            callback && (code = callback(code));
            return code;
        }
        mc.forEach( function(v, i) {
            var respl = new RegExp(relab, 'gi');
            var re = v.replace(respl, '\\$1');
            code = code.replace(v, f[0]+i+f[1]);
        });
        callback && (code = callback(code));
        if(!code) return code;
        mc.forEach( function(v, i) {
            var reMcLab = new RegExp((f[0]+i+f[1]).replace(
                    new RegExp(relab, 'gi'), '\\$1'
                ), 'gi');
            var val = v.replace(/\$/gi, '$$$$');
            code = code.replace(reMcLab, val);
        }); 
        return code;        
    }
    , isVue = function(fun){
        var isvue = (typeof(Vuec) == 'undefined')?false:true, spts = $('script');
        if(!isvue){
            for(var i=0; i<spts.length; i++){
                if(/[^\w]vue[\.\/].*.js/gi.test(spts[i].src)){
                    isvue = true; break;
                }
            }
        }
        return isvue;
    }
    , init = function(){
        $(function(){
            if( isVue() ){                
                var spt = ET.ce('script', {type : 'text/javascript'});
                $(spt).html("(function(){ typeof(ET)=='function' && ET('[et]'); })();");
                setTimeout(function(){ $('>html').append(spt); }, 1);
            }else{ 
                ET('[et]'); 
            }
        });
    }
    , extendLib = function(obj, tag, lib){
        if(obj[tag] || !lib)return false;
        obj[tag]={};
        $.extend(obj[tag], lib);
        obj[tag].initialize && obj[tag].initialize();
    }
    , EElement = function(ele){}
    ;
    EElement.init=function(el){ 
        el = $.extend(el, new EElement);
        if( $(el).attr('et') != undefined ){
            ET.loadExt(el.tagName, function(e){
                $.extend(el, e);
                $(el).addClass('ET_STYLE_'+el.tagName.toUpperCase());
                if(e.initialize){
                    el.initialize();
                    delete el.initialize;
                }
                $(el).removeAttr('et');
            });
        }
        return el; 
    }
    // 所在层级
    EElement.prototype.hierarchy=function(el){
        var context = isHTMLNode(this)?this:null;
        var deep = -1;
        if(!context || !el || !el.nodeType || el.nodeType != 1) return deep;
        if(context == el) deep = 0;
        var pel = el;
        var n = 0;
        while(pel){
            if(pel == context){ deep = n; break; }
            pel = pel.parentNode;
            n++;
        }
        return deep;
    }
    EElement.prototype.extend=function(){
        var _me = this;
        for(var i=0; i<arguments.length;i++){
            ET.loadExt(arguments[i], function(e){
                $.extend(_me, e);
                if(_me.initialize){_me.initialize(); delete _me.initialize; }
            });
        }
    }
    EElement.prototype.call=function(){
        if(arguments.length<1)return ;
        var e = arguments[0], arg = [];
        for(var i=1; i<arguments.length; i++)arg.push(arguments[i]);
        return this.apply(e, arg);
    }
    EElement.prototype.apply=function(e, args){
        var f=this[e];
        if(!f && this['on'+e])f = this['on'+e];
        if(!f && $(this).attr(e)) f = new Function($(this).attr(e));
        return f?f.apply(this, args):null;
    }
    EElement.prototype.cid=function(){
        if(this.id)return this;
        this.id = 'ET_' + (this.tagName?this.tagName:'ODR')+'_'+(new Date).getTime();
        return this;
    }
    EElement.prototype.ceNS=function(tagName, attrs, events){ return this.ce(tagName, attrs, events, ""); }
    EElement.prototype.ce=function(tagName, attrs, events, ns){
        tagName=tagName?tagName:'div';
        var context = isHTMLNode(this)?this:null;
        var el = null; 
        if(ns != null){
            ns = ns||"http://www.w3.org/2000/svg";
            el = doc.createElementNS(ns, tagName);
            $(el).html('not support!');
        }else{
            el = doc.createElement(tagName);
        }
        for (var key in attrs){ this.ca.call(el, key, attrs[key]); }
        if(context){ $(context).append(el); }
        el =  EElement.init(el);
        if(events){ el.cee(events); }
        return el;
    };
    EElement.prototype.cee=function(events){
        var el = this instanceof HTMLElement?this:null;
        return el?$.extend(el, events):this;
    }
    EElement.prototype.ca=function(k, v){
        var _attr=['colSpan'];
        var ele = isHTMLNode(this)?this:null;
        for(var i=0, l=_attr.length; i<l; i++){
            if(_attr[i].toLowerCase()==k.toLowerCase()){k=_attr[i];break;}
        }
        var att = doc.createAttribute(k);
        att.value = v;
        if(ele){
            if(typeof(ele[k])!='undefined'){ ele[k] = v;
            }else{ ele.attributes.setNamedItem(att); }
        }
        return att;
    }; 
    EElement.prototype.onDomNodeRemoved=function(fun){return this.on('DOMNodeRemoved', fun);}
    EElement.prototype.onDomNodeRemovedFromDocument=function(fun){return this.on('DOMNodeRemovedFromDocument', fun);}
    EElement.prototype.onDomNodeInserted=function(fun){return this.on('DOMNodeInserted', fun);}
    EElement.prototype.onDomSubtreeModified=function(fun){return this.on('DOMSubtreeModified', fun);}
    EElement.prototype.onDOMAttrModified=function(fun){
        var _me = this; Array.from(el.attributes).map(function(v){ _me.watchAtrr(v.name, fun);});
    }
    EElement.prototype.watchAtrr=function(key, func){
        var _me = this;
        if(!this.wal)this.wal={data:[], tid:null};
        this.wal.data.push({name:key, value:$(this).attr(key), func:func});
        if(!this.wal.tid){
            this.wal.tid=setInterval(function(){
                if(_me.wal.data.length<1){
                    clearInterval(_me.wal.tid);
                    _me.wal.tid = null;
                    return ;
                }
                for(var i=0; i<_me.wal.data.length; i++){
                    var attr = _me.wal.data[i], v=$(_me).attr(attr.name);
                    if(v != attr.value){
                        attr.value = v;
                        try{attr.func.call(_me, {name:attr.name, value:v}); }catch(e){}
                    }
                }
            }, 500);
        }
    }

    EElement.prototype.on=function(ent, fun){
        var ret = null, ele = isHTMLNode(this)?this:null
        if(!ele)return ;
        if(ele.addEventListener){
            ret = ele.addEventListener(ent, fun);
        }else if(ele.attachEvent){
            ret = ele.attachEvent('on'+ent, fun);
        }else {
            return null;
        }
        return ret;
    }

    EElement.prototype.onappend=function(fun){
        var ele = isHTMLNode(this)?this:null
        _me=this, ifm=this.ce('iframe', {'src' : rootPath+'/img/data-lazyload.gif', 
            'style':'position:absolute;top:0rem;margin-top:-1rem;opacity:0;filter:alpha(opacity=0);visibility:hidden;width:0rem;height:0rem;'});
        if(!fun && ele){
            if(_me.load){fun=_me.load; }
            else if(_me.onload){fun=_me.onload; }
        }
        if(!fun)fun=function(){};
        ifm.forobj=_me;
        ifm.forfun=fun;
        ifm.onload=function(){ 
            var ifm = this;
            setTimeout(function(){
                forobj = ifm.forobj;
                var data =Array.from(arguments);
                ifm.remove();
                ET($(forobj).find('[et]'));
                ifm.forfun.apply(forobj, data);
            }, 1);
        };

    };

    ['js', 'css','img', 'app', 'manual'].map(function(v){
        ET[v+'Path'] = rootPath+'/'+v;
    });
    $.extend(ET, {
        ce:function(){var el = new EElement(); return el.ce.apply(el, arguments); }
        ,ca:function(){var el = new EElement(); return el.ca.apply(el, arguments); }
    });
    $.extend(ET, {
        //临时函数只执行一次
        tFun : function(callback, context){
            var fn = 'ET_TF';
            ET.tFun.caller && (fn += '_' + ET.tFun.caller.name);
            fn += '_'+(new Date).getTime();
            context = context?context:this;
            window[fn]=function(){
                callback.apply(context, arguments);
                delete window[fn];
            }
            return fn;
        }
        ,copyToclipboard : function(txt){
            var ipt = ET.ce('textarea', {'style':'position: fixed;opacity:0;width:1;height:1px','value':txt});
            ipt.onappend(function(){
                ipt.select(); document.execCommand('copy');
                $(ipt).remove(); 
                var msg = 'copied complate!';
                if(ET.messagebox)ET.messagebox.show(msg);
                else alert(msg);
            });
            $('body').append(ipt);
        }
        ,timeConsumingTest : function(fun, n){
            n = n?n:1;
            var _this = arguments.callee.caller;
            var s = new Date;
            if(typeof(fun)=="function") while(n--) fun.call(_this);
            var e = new Date;
            var o = {s:s, e:e, t:e.getTime()-s.getTime()};
            o.__proto__ = {
                toString:function(){
                    return 'start time:' + this.s.getTime()+ '\nend time:' + this.e.getTime()+ '\ntime consuming:' + this.t; 
                }
            };
            return o;
        }
        ,extend : function(){
            var args = [];
            if(arguments.length<1){ return null; }
            args = (arguments.length==1)?[this, arguments[0]]:arguments;
            return $.extend.apply(this, args);
        }
        // 加载组件
        ,loadLib : function(lib, fun){
            var obj = this[lib];
            if(!obj){
                var _me=this, c = new cache('lib', function(){
                    var url = _me.jsPath+'/lib/'+lib+'.lib.js';
                    obj = _me.loadJS(url);
                    return !obj?null:obj.errcode?null:obj;
                });
                obj = c.get(lib);
                extendLib(_me, lib, obj);

            }
            fun && fun.call(obj);
            return this;
        }
        // 加载扩展
        , loadExt : function(ext, fun){
            ext = ext.toLowerCase();
            if(exts && exts.indexOf(ext)==-1 ){
                return ;
            }
            var _me=this, c = new cache('ext', function(){
                var url = _me.jsPath+'/ext/'+ext+'.ext.js';
                obj = _me.loadJS(url);
                return !obj?null:obj.errcode?null:obj;
            });
            obj = c.get(ext); 
            if(!obj && fun){
                c.listen(ext, fun);
            }else{
                fun && fun(obj);
            }
            return obj;
        }
        // 加载JS
        ,loadJS : function(url, callback){
            if(!url){return;}
            var ret = this.getFile(url, null, null);
            if(!ret){ return {errcode:1, data:'error:Can\'t get content'}; }
            if(ret.errcode!=0){ return ret; }
            var obj = this.execJS(ret.data);
            callback && callback(obj);
            return obj;
        }
        // 执行JS
        ,execJS : function(code){
            if(!code || typeof(code)!='string')return code;
            code = removeComments(code);
            code = code.replace(/(^[\s\r\n]+)|(\s\r\n+$)/gi, '');
            if(/^\s*(\[|\{)[\w\W]*(\]|\})\s*;?\s*$/gi.test(code)){
                code = 'return ' + code;
            }
            code = 'try {'+ code +'} catch(e) { throw e; }';
            try {
                setTimeout(function(){loadDepends(code);}, 100);
                return new Function('$, ET', code)($, ET);
            } catch(e) {
                console.error(e);
                console.log(code);
            }
            return null;
        }
        // 读取文件内容
        ,getFile : function(url, callback, conf){
            if(!url) return null;
            var key = url.match(/\w+/gi).map(function(v){
                var c = 0;
                v.split(/\s*/gi).map(function(v){ c+=(v+'').charCodeAt(); })
                return v.substr(0,1)+c+v.substr(v.length-1,1);
            }).toString().replace(/[\s\,]/gi,'');
            var ls = debug?null:WLS(key);
            var reval={errcode:0, data:'ok'}, async = callback?true:false
            , cb = function(e){ reval = {errcode:0, data:e};  ls && ls.set(e); };
            if(callback){
                var ucb = (typeof(callback) == 'string')?new Function('e', callback):callback;
                callback = function(d){ try{ ucb.call(this, d); }catch(e){throw e;}; cb.call(this, d);}
            }else{
                callback = cb;
            }            
            if(ls && ls.get()){
                callback(ls.get());
                return reval;
            }
            if(debug){
                url +=(url.indexOf('?')!=-1)? '&':'?';
                url += 'rnd='+Math.random();
            }
            var c = {
                url : url
                ,type : 'GET'
                ,dataType : 'text'
                ,async : async
                ,success : function(e, tts, xhr){ callback.call(xhr, e); }
                ,error : function(XMLHttpRequest, textStatus, errorThrown){
                    reval.errcode=XMLHttpRequest.status;reval.data=errorThrown; 
                }
            }
            if( /^\w+/gi.test(url) && url.indexOf(location.host)==-1){
                c.url= this.appPath+'/EtSoftWare.php?m=Cross&url=' + encodeURIComponent(url);
                c.success = function(e, tts, xhr){ var o=ET.execJS(e); o && callback.call(xhr, o.data); }
                c.dataType='jsonp';
                reval={errcode:2, data:'Cross-domain loading'}
                var cl = this.getFile.caller;
                var _me=this, fn='';
                while(cl){ fn+='_'+cl.name; if(fn.indexOf('_loadJS__get_')==0){break;} cl=cl.caller; }
                var args = cl?cl.arguments:[null];
                if(fn.indexOf('_loadJS__get_loadLib')==0){
                    c.jsonpCallback=ET.tFun(function(e){
                        var http = _me.execJS(e);
                        var obj = _me.execJS(http.data);
                        (new cache('lib')).set(args[0], obj);
                        _me.loadLib(args[0]);
                    });
                }else if(fn.indexOf('_loadJS__get_loadExt')==0){
                    c.jsonpCallback=ET.tFun(function(e){
                        var http = _me.execJS(e);
                        var obj = _me.execJS(http.data);
                        var ext = args[0].toLowerCase();
                        if(!ext){return ;}
                        ext = ext.toLowerCase();
                        (new cache('ext')).set(ext, obj);
                        _me.loadExt(ext);
                    });
                }
            }
            $.extend(c, conf);
            $.ajax(c);
            return reval;
        }
    });
    if(debug){ WLS('').delete(); }
    if(!exts){
        ET.getFile(ET.appPath+'/EtSoftWare.php?a=ext', function(e){
            exts = ET.execJS(e);
        });
    }
    global.Et = global.ET = ET;
    init();
    callback && callback();
}));

