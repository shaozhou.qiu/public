/*
*
*	EtSoftware Beta 6.0.0.0
*	Release date: 2018-04-11
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*/
(function _ET(global, jQuery, factory, bqueue){	
	if(global.ET && global.ET.$){return ;}
	var basePath=''
	, doc = global.document
	, eExt = {
        cid:function(ele){
            if(!ele) return null;
            var id=ele.id;
            if(id){ return id; }
            id = ele.getAttribute?ele.getAttribute('name'):'';
            if(id){
                id = id.replace(/[^\w]+/gi,'_').replace(/^_*(\w*?)_*$/gi,'$1');
            }else{
            	id = "ET_" + ele.tagName.toUpperCase();
            }
            if( doc.all(id) ){
                var idx=0;
                while( doc.all(id+'_'+idx) ){ idx++; }
                id += '_' + idx;
            }
            ele.id = id;
            return id;
        }
        ,ca:function(key,value,ele){
            var _attr=['colSpan'];
            var k=key.toString();
            for(var i=0, l=_attr.length; i<l; i++){
                if(_attr[i].toLowerCase()==key.toLowerCase()){k=_attr[i];break;}
            }
            var att = doc.createAttribute(k);
            att.value = value;
            if(ele) ele.attributes.setNamedItem(att);
            return att;
        }
        ,ce:function(tagName, param, ns){
            var _me = this, o = null;
            if(ns != null){
                ns = ns||"http://www.w3.org/2000/svg";
                o = doc.createElementNS(ns, tagName);
            }else{
                o = doc.createElement(tagName);
            }
            if(param){ for(var k in param){ this.ca(k, param[k], o); } }

            o.ca=function(key,value){ _me.ca(key,value, this); return this; }
            o.ce=function(tagName, param, ns){ this.appendChild(_me.ce(tagName, param, ns)); return this; }
            o.onappend=function(fun){ _me.onappend(this, fun); return this; }
            o.exec=function(fun, data){ return _me.exec(fun, data, this);  }
            o.cid=function(){ return _me.cid(this);  }

            return o;
        }
        /**
         * 执行函数
         * @return {[type]} [description]
         */
        ,exec:function(fun, data, o){
            if(!fun)return;
            if( typeof(fun) != 'function' )return;
            var f='tmpFun_'+ parseInt(Math.random()*10000);
            o = o?o:window; o[f]=fun;
            if( o[f] ){
                var ret=o[f](data);
                o[f]=undefined; delete(o[f]);
            }
            return ret;
        }
        /**
         * 追加到页面时调用
         * @param  {[type]} o   [description]
         * @param  {[type]} fun [description]
         * @return {[type]}     [description]
         */
        ,onappend:function(o,fun){
            if(!o){return ;}
            var _me=this, ifm=this.ce('iframe', {'src' : basePath+'/img/data-lazyload.gif', 
                'style':'position:absolute;top:0rem;margin-top:-1rem;opacity:0;filter:alpha(opacity=0);visibility:hidden;width:0rem;height:0rem;'});
            o.appendChild(ifm);
            ifm.onload=function(){ 
                if(!fun){if(o.load){fun=o.load; }else if(o.onload){fun=o.onload; }else{fun=function(){}; } }
                var data =Array.from(arguments);
                this.remove(); _me.exec(fun, data, o); 
            };
        }
    },
    cache = {
    	get : function(key){
    		if(typeof(localStorage)!='object'){return null;}
    		return localStorage.getItem('ET_'+key);
    	}
    	, set : function(key, val){
    		if(typeof(localStorage)!='object'){return null;}
    		localStorage.setItem('ET_'+key, val);
    	}
    },
    queue = {//消息队列
		push:function(key, para){//将消息推入队列
			!this.data && (this.data={});
			var d=this.data[key]?this.data[key]:[];
			d.push(para); this.data[key]=d;
		}
		,pull:function(key){//抽取队列中的消息
			!this.data && (this.data={});
			var d=this.data[key]?this.data[key]:[];
			if(d.length==0 ){ 
				this.data[key] && delete this.data[key]; return null;
			}
			var ret = d[0]; d.splice(0,1); this.data[key]=d;
			return ret;
		}
	};
    if(basePath == ''){
		var hspts=doc.getElementsByTagName('script');
		for(var i=0;i<hspts.length; i++){
			var e = hspts[i], src=e.src?e.src:'';
			if( !/EtSoftWare/gi.test(src.toLowerCase()) ){continue;}
			var m=/(.*)\/.+\/[^\/]+$/gi.exec(src);
			if(m){basePath = m[1]; break; }
		}
    }
	if(jQuery == null){
		var id='jQuery341', _me = this;
		if(!doc.all(id)){
			var spt = eExt.ce('script', {id : id, 'type' : 'text/javascript', 'src':basePath+'/js/jquery-3.4.1.js'});
			window.ET = function(){ 
				queue.push('b', Array.from(arguments)); 
			 	_ET(global, window.jQuery, factory, queue);
			};
		    Object.defineProperty(window.ET, '$', {
		    	get:function(){return this._$;},
		    	set:function(e){
		    		_ET(global, e, factory, queue);	
		    		this._$ = e;
		    	}
			});
			doc.head.appendChild(spt);
		}
	}else{
		if(!!window.ET && ET.path){ return ;}
		/* 入口函数 */
		factory(global, jQuery, basePath, eExt, cache, queue);
		if(bqueue){
			var b = null;		
			while( b = bqueue.pull('b')){
				var js = 'var $=window.jQuery; ET(';
				for(var i=0, l=b.length; i<l; i++){
					js += 'b['+i+'],';
				}
				js += '0);';
				new Function('$, b', js)(window.jQuery, b);
			}
		}
	}
}(typeof window !== "undefined" ? window : this, typeof window.jQuery !== "undefined" ? window.jQuery : null,function(win, $, basePath, eExt, cache, queue){
	"use strict";
	var
	debug=false
	// debug=true
	,doc=win.document
	,ET=function(selector, context){
		if(!$ ){ 
			if(selector) ET.queue.push('ini', {'s':selector, 'c':context});
			setTimeout(function(){ET();}, 100); return ; 
		}
		var q=null;		
		while( q=ET.queue.pull('ini')){ET.fn.init(q.s, q.c);}
		if(selector===undefined)return ;
		return ET.fn.init(selector, context);
	}	
	,EleFun={// 元素功能
		ca:function(key,value,ele){
			if(!key)return ele;
			ele=ele?ele:this;
			if(!ET.object){ET.loadLib('object'); }
			if(ET.object &&  ET.object.getType(ele).indexOf('element')<0 ){return null;}
			if(ele[key]===undefined){
				try{
					eExt.ca(key, value, ele);
				}catch(e){
					window.console && console.error('ERROR\tCreate '+ ele.tagName +' of '+key.toString()+' attribute failure');
				}
			}else{// this.trace('Element is not specified!');
				if( key.toLowerCase()=='innerhtml'){
					$(ele).html(value);
				}else if( key.toLowerCase()=='innertext'){
					$(ele).html( ET.code.removeFlag(value, 'html') );
				}else{
					if( ET.object && ET.object.getType(value)=="function" ){
						ele[key]=value;
					}else{
						$(ele).attr(key, value);
					}
				}
			}
			return ele;
		}
		/**
		 * 创建新元素
		 * @param  {[type]} tagName 创建Element元素
		 * @param  {[type]} param   附加属性参数
		 * @param  {[type]} ele     目录Element元素，如果为null刚为当前对象
		 * @param  {[type]} ns      namespace 命名空间
		 * @return {[type]}         Element元素
		 */
		,ceNS:function(tagName, param, ele){ return this.ce(tagName, param, ele, ''); }
		,ce:function(tagName, param, ele, ns){
			tagName=tagName?tagName:null; ele = ele?ele:this;
			if(tagName==null){return this.ce('div', param, ele);}
			var o = eExt.ce(tagName, null, ns);
			if(!o){return null;}
			for (var key in param){this.ca(key, param[key], o); }
			ET.extend(o, EleFun);
			if(!ET.object){ET.loadLib('object'); }
			if( ET.object && ET.object.getType(ele).indexOf('html')==0 ){ ele.appendChild(o);}
			return o;
		}
		/**
		 * 向Elements添加控制事件
		 * @param {[type]} event 函数名
		 * @param {[type]} fun   触发/回调函数
		 * @param {[type]} ele   绑定到Element元素，默认为自身
		 */
		,addEvents:function(event, fun, ele){
			ele = ele?ele:this;
			var triggerEvent=event.replace(/^on(.*)/gi, '$1');
			if( !ET.object.isElement(ele) ) return ;
			fun=fun?fun:function(e){};
			if( ele[event]==undefined || ele[triggerEvent]==undefined ){
				if(ele.attachEvent){
					ele.attachEvent('on'+triggerEvent, fun);
				} else if(ele.addEventListener){
					ele.addEventListener(triggerEvent, fun, false);
				}else{
					ele[event]=fun;
				}
			}
		}
	};	
	ET.fn = ET.prototype ={};
	ET.extension = {};//扩展库
	/* 扩展函数 */
	ET.extend = ET.fn.extend =function(){
		var i=0,agm=arguments,d=this;
		if(agm.length>1){d=agm[0];i=1;}
		for (;i<agm.length ;i++ ){
			for (var property in agm[i]){
				try{
					d[property] = agm[i][property];
					if(agm.length==1 && d[property].initialize){
						d[property].initialize();
					}
				}catch(e){
					// console.log(e);
				}
			}
		}
		return d;
	}
	ET.extend(eExt);
	ET.extend(EleFun);
	ET.extend({
		clone:function(obj){
			var d, typef = typeof obj;	                	
			if( typef == "object"){  //object分为两种情况 对象（Object）和数组（Array）
	            if(obj === null) {d = null; } else {
	                if( ET.object.getType(obj) === "array") {
	                    d = []; for( var i = 0 ; i < obj.length ; i++ ) {d.push( this.clone(obj[i])); }
	                }else if( ET.object.getType(obj).indexOf('html')>-1) {
	                	d=$(obj).clone();
	                }else { // [object Object]
	                    d = {}; for( var j in obj) {d[j] = this.clone(obj[j]); }
	                }
	            }
		    // }else if( typef==="function" ){
		    	// d=function(){};
		    }else{
		        d = obj;
		    }
			return d;
		}
	});
	ET.extend({
		execJS:function(code){
			if(!code || typeof(code)!='string') return null;
			var reVal=null, pureCode=code;
			if( !!ET.code && ET.code.removeComments ){
				pureCode = ET.code.removeComments(code);
			}
			if(!pureCode) return null;
			if(! /^[\s\t]*return\s+/gi.test(pureCode)){
				var re = new RegExp("^[\\s\\t]*("
					+"(\"[\\w\\W]*\")"
					+"|(\\'[\\w\\W]*\\')"
					+"|(\\{[\\w\\W]*\\})"
					+"|(\\[[\\w\\W]*\\])"
					+"|(\\([\\w\\W]*\\))"
					+"|(\\d+)"
					+")[\\s\\t]*$", "gi");
				if(re.test(pureCode)){
					pureCode = 'return (' +pureCode + ');';
				}
				pureCode = 'try{ ' +pureCode + ' }catch(e){ throw e;}';
			}
			try{ 
				if(pureCode){reVal = new Function('$', pureCode)($); }
			}catch(e){
				if(window.console){console.error(e);} 
				if(debug){debugger; }
			}
			return  reVal;
		}
		/**
		 * 加载Element扩展
		 * @param  {[type]} extName 扩展名          
		 * @param  {[type]} fun 回调函数
		 * @return {[type]} 
		 */
		,loadExt:function(extName, fun){
			if(ET.extension[extName]) return {errcode:0, data:ET.extension[extName]};
			var ret = this.loadJS(ET.extPath+extName+'.ext.js', fun);
			if(ret.errcode == 1002 ){//异步加载
				if(ET._extdef && ET._extdef[extName]){ return ret; }
				if(!ET._extdef)ET._extdef={};
				ET._extdef[extName] = true;
				Object.defineProperty(ET.extension, extName, {
					get:function(){return this['_'+extName];}
					,set:function(v){ 
						this['_'+extName] = v;
						ET(extName+'[et]');
					}
				});
				return ret;
			}
			return {errcode:0, data:ET.extension[extName]};
		}
		/**
		 * 加载动态库
		 * @param  {[type]} lib 名称
		 * @param  {[type]} fun 回调函数
		 * @return {[type]}     [description]
		 */
		,loadLib:function(lib, fun){
			var o={};
			if(lib instanceof Array){
				for(var i=0; i<lib.length; i++){ o[lib[i]]=this.loadLib(lib[i]);}				
			}else{
				if( ET[lib] ){o=ET[lib]; }else{
					Object.defineProperty(ET, lib, {
						get:function(){return this['_lib_'+lib];}
						,set:function(v){ 
							this['_lib_'+lib] = v;
							libReadyCall(lib).forEach(function(v, i, arr){
								(typeof(v)=='function') && v(v);
							});
						}
					});					
					var ret =this.loadJS(this.libPath + lib + '.lib.js');
					o = (ret.errcode == 0)?ret.data:null;
					if(o){
						var ext={};ext[lib]=o; 
						ET.extend(ext); 
						(typeof(ET[lib].initialize)=='function') && ET[lib].initialize(); 
					}else if(ET[lib] && ET[lib].initialize){
						ET[lib].initialize();
					}
				}
			}
			fun && fun( o );
			return o;
		}
		,includeJS:function(url, fun){
			if(!this.cache){ this.cache={}; }
			if(!this.cache.ijs_cache){ this.cache.ijs_cache=[]; }
			if(this.cache.ijs_cache.indexOf(url)>-1){ return ; }
			this.cache.ijs_cache.push(url);
			var js = ET._ce('script', {'type':"text/javascript", 'src':url});
			doc.head.appendChild(js);
			if(fun){
				setTimeout(function(){ fun && fun();}, 1000);
			}
		}
		/**
		 * 载入JS文件 
		 * @param  {[type]} url JS文件地址
		 * @param  {[type]} fun 回调函数
		 * @return {[type]}     [description]
		 */
		,loadJS:function(url, fun){
			if(!url){ return null; }
			var ret=this.getFile(url, fun?function(e){exec(e.data, fun);}:null, 'script');
			if(ret.errcode!=0){ return ret; }
			var code=ret.data;
			function exec(code, fun){
				var o=null;				
				if(code){o = ET.execJS(code);fun&&fun(o, code);}
				return {errcode:0, data:o};
			}
			if( typeof(code)=="object" ){fun && fun(code, code); return code; }
			return exec(code, fun);
		}
		/**
		 * 计算执行函数所需要的时间（单位为 毫秒数）
		 * @param  {[type]} fun 函数
		 * @return {[type]}     [description]
		 */
		,timeConsuming:function(fun){
			var t1=(new Date).getTime();
			var t2 = t1;
			if(fun){
				fun && fun();
				t2 = (new Date).getTime()-t1;
			}else if(this.cache.tc_s){
				t2 = (new Date).getTime()- this.cache.tc_s;
				this.cache.tc_s = null;
			}else{
				this.cache.tc_s = t1;
			}
			return t2;
		}
		/**
		 * 读取本地文件
		 * @param  {[type]} url [description]
		 * @param  {[type]} fun [description]
		 * @return {[type]}     [description]
		 */
		,getLocalFile:function(fun){
			var ret=null;
			if(!fun){console.log('%cMust specify a callback function!!','background-color:red;color:#fff;');return ret;}
			var ipt=doc.createElement('input');
			$(ipt).attr({'type':'file'}).hide();
			$(doc.body).append(ipt);
			ipt.onchange=function(){
				if(this.files.length){
					ret=[];
					var l = this.files.length;
					for(var i=0; i<l;i++){
						var f = this.files[i];
						readfile(f, function(e){
							ret.push(e);
							(ret.length==l)&& fun(ret);
						});
					}
				}
			};
			ipt.click();
			$(ipt).remove();

			function readfile(f, fun){
				var ret={
					'lastModified':f.lastModified
					,'lastModifiedDate':f.lastModifiedDate
					,'name':f.name ,'size':f.size ,'type':f.type
				};
				if( typeof(ActiveXObject)!= "undefined" ){
					var fso = new ActiveXObject("Scripting.FileSystemObject");
					if(fso.fileExists( path ) ){
						var f = fso.OpenTextFile(path, 1, true);	
						if( !f.atendofstream ){
							ret.text = f.ReadAll(); 
						}
						f.Close();
					}
					fun(ret);
				}else if( typeof(FileReader)!= "undefined" ){
					var r = new FileReader();
					r.callback=fun;
					r.onload=function(e){ 
						ret.text=this.result; 
						r.callback && r.callback(ret, e);
					}
					r.readAsText(f);
				}
			}
	
		}
		/**
		 * 获取文件内容
		 * @param  {[type]} url 文件所在地址
		 * @param  {[type]} fun 回调函数
		 * @param  {[type]} dataType  预期服务器返回的数据类型。如果不指定，jQuery 将自动根据 HTTP 包 MIME 信息来智能判断;
		 * @return {[type]}     [description]
		 */
		,getFile:function(url, fun, dataType){

			!this.cache && (this.cache={});
			!this.cache._c_g_f && (this.cache._c_g_f={});
			if( /^\/\w.*/gi.exec(url) ){ // /public/.......
				url = location.origin + url;
			}else if( /^\.\/\w.*/gi.exec(url) ){ // ./public/.......
				url = location.origin + location.pathname + url.substr(2);
			}
			var _me = this, key = url
			, ret={ errcode:1000, data:"Err:unspecified url!" };
			if(!url){ return ret; }
			key = encryption(key);
			var o = this.cache._c_g_f[key];
			if( o ){ fun && fun(o); return o; }
			var sch = function(e){
				var o = {
					url:url
					, errcode:e.errcode?e.errcode:0
					, data:e.data?e.data:e
				};
				_me.cache._c_g_f[key] = o ;
				if(!debug && e.errcode == 0){
					cache.set(key, e.data);
				}
				return o;
			}, callback = null;
			if( typeof(fun) == 'function' ){
				ret={ errcode:0, data:"info:execute through callback!" };
				callback = function(e){ var ret = sch(e); fun && fun(ret); }
			}
			// if( url.indexOf(ET.path) ){
			if( isCrossDomain(url) ){
				url = this.appPath+"EtSoftWare.php?m=Cross&url="+encodeURIComponent(url);
			}
			var rme = url.indexOf(location.origin);
			ret = debug?null:cache.get(key);
			
			if(!ret){
				if(rme == -1){
					var jspCb = function(e){
						var ret = {errcode:0, data:e}
						, o = callback?callback(ret):sch(ret);
						if(o.errcode != 0){ return false;}
						if(dataType == 'script'){
							ET.execJS(o.data);
						}
						return o;
					};
					ret = this.getFileByJsonp(url, jspCb);
				}else{
					ret = this.getFileByAjax(url, callback, dataType);
				}
			}else{
				ret = {errcode:0, 'data':ret};
			}
			return sch(ret);
		}
		,getFileByJsonp:function(url, fun){
			var conf = {
				url:url,
				type:"get",
				dataType:"jsonp",     // 伪造ajax  基于script
				//jsonpCallback是实现跨域请求的时候定义回调函数用的 ，可以随意起名字. 
				//默认jQuery会给定一个函数如 jQuery112408245748099542181_1563779195936
				// jsonpCallback: 'callback',
				success:function (e) {
					if(conf.jsonpCallback){ return ;}
					try{
						var o = ET.execJS(e);
						if(o.http_code != 200){ return ;}
						if( !o.data ){ return ;}
						ET.execJS(o.data);
					}catch(err){
					}
				}
			}
			if(fun){ 
				var tmpFun = '_tmp_fun_'+parseInt(Math.random()*10000);
				window[tmpFun]=function(e){
					var result = null, v = e;
					var o = ET.execJS(e);
					if(o && o.http_code == 200){ v = o.data; }
					if(fun){ eExt.exec(fun, v); }
					delete(window[tmpFun]);
					return result;
				};
				conf.url+='&etCallback='+tmpFun; 
				conf.jsonpCallback=tmpFun; 
			}
			$.ajax( conf ); 
			return {errcode:1002, data:'The program uses the asynchronous loading process'};
		}
		,getFileByAjax:function(url, fun, dataType){
			var ret={ errcode:1000, data:"Err:unspecified url!" }
			if(!url){ fun&&fun(ret);return ret; }
			$.ajax({url:url ,type:'get',async:(fun?true:false)
				,'dataType':dataType
				,'cache':!debug
				,'success':function(e){ret={errcode:0, data:e}; }
				,'error':function(XMLHttpRequest, textStatus, errorThrown){
					ret={errcode:1001, data:null};
					var msg = "Err("+XMLHttpRequest.status+"),"+textStatus+":"+url;
					window.console && console.log([msg, XMLHttpRequest, textStatus, errorThrown]);					
				}
				,'complete':function(){
					fun&&fun(ret);
				}
			});
			return ret;
		}
	});
	//加密
	function encryption(str){
		if(!str || str=='')return null;
		var h=0, c=0, x=1, ret='', b='';
		for(var i=0, l=str.length;i<l;i++ ){
			var asc=str.substr(i,1).charCodeAt();
			b+=asc>x?'1':'0';
			h+= asc; c+= asc+asc%x; x= asc; 
			c=c<255?c:c-255;
		}
		ret=str.substr(0,1)+h;
		var m = str.match(/\W/gi);
		if(m) ret+='m'+m.length;
		ret+='x'+x;
		ret+=str.substr(str.length-1,1);
		for(var i=0, l=b.length;i<l;i++ ){
			if(i>=ret.length)break;
			var s=ret.substr(i,1).charCodeAt(), ns=s;
			if(b[i]=='1'){
				ns++;
				if(s>=48 && s<=57 && ns>57){ //数字
					ns-=10;
				}else if((s>=79 && s<=122 && ns>122)||(s>=65 && s<=90 && ns>90)){ //az
					ns-=26;
				}
			}else{
				ns--;
				if(s>=48 && s<=57 && ns<48){ //数字
					ns+=10;
				}else if((s>=79 && s<=122 && ns<79)||(s>=65 && s<=90 && ns<65)){ //az
					ns+=26;
				}
			}
			ret=ret.substr(0,i)+String.fromCharCode(ns)+ret.substr(i+1);
		}
		ret+=c;
		return ret;
	}
	ET.extend({queue:queue});
	var init=ET.fn.init=function(selector, context){
		var e = $(selector);
		if( e.length>1 ){
			var es=[];
			e.each(function(){ es.push(ET(this)); });
			return es;
		}else if( e.length == 0 ){return null; }
		e=e[0];
		if(!e)return null;
		var tname = (e==doc)?"DOCUMENT":e.tagName.toUpperCase();
		if(e.EtReadyState & 2)return null;
		e.EtReadyState |= 1;
		$(e).addClass('ET_STYLE_'+tname);
		ET.extend(e, eExt, EleFun);
		ET.extend(e, {extend:function(obj){
			var ret=null;
			if(obj==null)return ;
			this.exts=this.exts?this.exts:[];
			if(typeof(obj)=='string'){
				if(obj == 'undefined'){return ;}
				if(this.exts[obj]==true){return ;}
				ret = ET.loadExt(obj);
				if(ret.errcode == 0 && typeof(ret.data)=='object')
				{
					this.exts[obj] = true;
					e.EtReadyState |= 2
					return this.extend( ret.data );
				}else{
					e.EtReadyState |= 4
				}
				return this;
			}
			obj=ET.clone(obj);			
			$.extend(this, obj);
			obj.initialize && (ret=this.initialize());
			if(ret && ret.errcode != 0){
				var _me = this;
				ret.lib.forEach(function(v){
					libReadyCall(v, function(){ _me.initialize();});
				});
				console.log(ret);
			}
			return this;
		}});
		// console.log(e.EtReadyState);

		e.extend(tname.toLowerCase());
		var ext = $(e).attr('et');
		if( ext==undefined )return e;
		if( !!ext && ext != 'et'){ 
			var eobj = ET.loadLib(ext);
			if(eobj){
				ET.extend(e, eobj); 
				try{
					eobj.initialize && e.initialize( e );
				}catch (e){
					console.error("%c loadLib:"+ext+' '+e.toString(),'color:red');
				}
			}
		}
		e.onload && e.onload();
		e.load && e.load();
		return e;
	}	
	function libReadyCall(lib, fun){
		if(!ET._lib ){ ET._lib=[]; }
		if(!fun || typeof(fun) !='function'){return ET._lib[lib];}
		if(!ET._lib[lib] ){ ET._lib[lib] = []; }
		ET._lib[lib].push(fun);
		return ET._lib[lib];
	}	
	/**
	 * 判断是否跨域调用
	 * @return {Boolean} [description]
	 */
	function isCrossDomain(){ 
		var args = arguments
		, url = args.length>0?args[0]+'':ET.path
		, hn = args.length>1?args[1]+'':location.host
		;
		return (url.indexOf(hn)<0)?true:false; 
	}


	/**
	 * 检测系统参数
	 * @return {[type]} [description]
	 */
	function chksysconf(){
		var es=null;
		ET.extend({
			'path':basePath
			,'jsPath':basePath+'/js/'
			,'libPath':basePath+'/js/lib/'
			,'extPath':basePath+'/js/ext/'
			,'cssPath':basePath+'/css/'
			,'imgPath':basePath+'/img/'
			,'appPath':basePath+'/app/'
			,'manualPath':basePath+'/manual/'
		});
		var exist=false; es=doc.getElementsByTagName('link');
		for(var i=0;i<es.length; i++){
			var href = es[i].href;
			if( href && /etsoftware.*\.css/gi.test(href.toLowerCase()) ){
				exist=true;break ;
			}
		}
		if(!exist){
			var o=doc.createElement('link')
			, cssurl = ET.cssPath+'Etsoftware6.1.css'+(debug?'?rnd='+Math.random():'')
			;
			if( isCrossDomain() ){
				var fs = $('html').css('font-size');
				cssurl = ET.appPath+"EtSoftWare.php?m=Cross&type=css&fontSize="+ encodeURIComponent(fs) +"&url="+encodeURIComponent(cssurl);
			}
			$.extend(o, {
				'rel':'stylesheet'
				,'type':'text/css'
				,'href':cssurl
			});
			var heads = doc.getElementsByTagName('head');
			if(heads.length){heads[0].appendChild(o); }
		}
	}
	window.etsoftware=window.EtSoftware= window.et = window.ET = window.Et = ET;
	chksysconf();
	
	$(function(){ initialize(); });
	
	function initialize(){
		for(var k in ET){ ET[k].initialize && ET[k].initialize(); }
		var ret = ET.loadLib(['random', 'code', 'object', 'console', 'cache', 'cookie', 'localStorage', 'messagebox','unit','css']);
		if( typeof(Vue) !='undefined' ){
			var spt = ET.ce('script', {'type' : 'text/javascript'});
			$(spt).html(" ET($('[et]')); ");
			setTimeout(function(){ $('body').append(spt); }, 1);
			return ;
		}
		ET($('[et]'));
	}

}));
// IE8下兼容HTML5标签
(function(){if(!/*@cc_on!@*/0)return;var e ="abbr,article,aside,audio,canvas,datalist,details,dialog,eventsource,figure,footer,header,hgroup,mark,menu,meter,nav,output,progress,section,time,video".split(','),i=e.length;while(i--){document.createElement(e[i])}})();

