/*
*
*	table 扩展 1.0.0.1 
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		ET.loadLib('tablelib');
		ET.loadLib('event');
		ET.loadLib('contextmenu');
		ET.loadLib('responsive');
		ET.loadLib('element');
		!this.id && (this.id = this.cid());
		this.init.__construct(this);
		this.init.thead(this);
		this.init.tbody(this);
		ET.element.new(this).addEvent('edit');
		this.addExtensions();
	}
	,addExtensions:function(){
		var _me = this;
		if(!ET.tablelib){ setTimeout(function(){ _me.addExtensions() ;}, 500); return ;}
		ET.extend(this, ET.tablelib.fn);
		ET.responsive.xs.in(function(){_me.showSimple(); });
		ET.responsive.xs.out(function(){_me.showOrigin(); });
	}
	/**
	 * 生成id
	 * @return {[type]} [description]
	 */
	,cid:function(){
		var id = 'ET_TABLE_';
		var tbls = $('table');
		for(var i=0, l=tbls.length; i<l; i++ ){
			if(this == tbls[i]){
				id += i;
			}
		}
		return id;
	}
	/**
	 * 当发生编辑后调用
	 * @return {[type]} [description]
	 */
	,edit:function(){
		var args = [];
		for(var i=0; i<arguments.length; i++){
			args.push(arguments[i]);
		}
		var fun = this.onedit?this.onedit:this.onEdit?this.onEdit:new Function();
		fun.apply(this, args);
	}
	/**
	 * 获取已选中的数据
	 * @return {[type]} [description]
	 */
	,getDataBySelected:function(fillter){
		var data =[], _me = this;
		$(this).find('>tbody>tr.selected').each(function(){
			data.push( _me.getRowDataByKey(this, fillter) );
		});
		return data;
	}
	/**
	 * 切换所有选择项
	 * @return {[type]} [description]
	 */
	,toggleChooseAll:function(){
		var _me = this;
		$(this).find('>tbody>tr').each(function(){
			_me.toggleChoose(this);
		});
	}
	/**
	 * 取消所有选择项
	 * @return {[type]} [description]
	 */
	,unChooseAll:function(){
		var _me = this;
		$(this).find('>tbody>tr').each(function(){
			_me.unChoose(this);
		});
	}
	/**
	 * 选择所有项
	 * @return {[type]} [description]
	 */
	,chooseAll:function(){
		var _me = this;
		$(this).find('>tbody>tr').each(function(){
			_me.choose(this);
		});
	}
	/**
	 * 选择行
	 * @param  {[type]} tr [description]
	 * @return {[type]}    [description]
	 */
	,choose:function(tr){
		$(tr).addClass('selected');
	}
	/**
	 * 取消选择行
	 * @param  {[type]} tr [description]
	 * @return {[type]}    [description]
	 */
	,unChoose:function(tr){
		$(tr).removeClass('selected');
	}
	/**
	 * 切换行选择状态
	 * @param  {[type]} tr [description]
	 * @return {[type]}    [description]
	 */
	,toggleChoose:function(tr){
		if( $(tr).hasClass('selected') ){
			this.unChoose(tr);
		}else{
			this.choose(tr);
		}
	}
	,orderby:function(cell, orderby){
		orderby = orderby?(orderby+'').toLowerCase():'asc';
		var keys = this.getKey();
		if( typeof(cell) != 'number' ){
			cell = keys.indexOf(cell);
			return cell>-1?this.orderby(cell, orderby):this; 
		}
		if( keys.length <= cell ){ return this;}
		var tbody = $(this).find('>tbody')
		, trs = tbody.find('>tr')
		, data = this.getData()
		, indexField = 'index_'+ET.random.Hex(5);
		for(var i = 0, l = data.length; i<l; i++){
			data[i][indexField] = i;
		}
		data = ET.array.sort.fast(data, orderby, keys[cell]);
		var otr = null;
		for(var i = 0, l = data.length; i<l; i++){
			var tr = trs[data[i][indexField]];
			if(otr){
				$(tbody).prepend( tr );
			}else{
				$(otr).after( tr );
			}
			otr = tr;
		}
		return this;
	}
	/**
	 * 渲染表格数据
	 * @param  {[type]} data 	初始数据
	 * @return {[type]} appendData  追加数据
	 */
	,rendering : function(data, appendData){
		if(!data && !appendData)return this;
		var tbody = $(this).find('>tbody')
		, keys = this.getKey();
		if(data){ tbody.empty(); }else{ data=[]; };
		if(appendData) data = data.concat(appendData);
		data = this.dataFilter(this.dataMap(data));
		for (var i = 0, l = data.length; i <l; i++) {
			var trs = tbody.find('>tr');
			var tr = ET.ce('tr', {'class':(trs.length%2)?'odd':'even'});
			for(var idx in keys){
				var td = ET.ce('td'), k = keys[idx];
				$(td).html(data[i][k]);
				this.init.td(td, this);
				tr.append(td);
			}
			this.init.tr(tr, this);
			tbody.append(tr);
		}
		return this;
	}
	,push : function(){
		var data = [];
		for(var i=0, l=arguments.length; i<l; i++){
			data.push(arguments[i]);
		}
		this.rendering(null, data);
	}
	,simpleStyle:{
		create : function(data){
			var div = ET.ce('div', {'class':'ET_STYLE_TABLE_SS'});
			$.extend(div, this.fn);
			for(var i=0, l=data.length; i<l; i++){
				div.addCard(data[i]);
			}
			return div;
		}
		, fn : {
			addCard : function(data){
				if(!data)return ;
				var itms = $(this).find('>.itm');
				var div = ET.ce('div', {'class':'itm '+ (itms.length%2?'odd':'even') });
				for(var k in data){
					var dl = div.ce('dl'), dt = dl.ce('dt'), dd = dl.ce('dd');
					if(!/^column_\d+$/gi.test(k)){
						$(dt).append( k+":" );
					}
					$(dd).append( data[k] );
				}
				this.length =this.length?this.length+1:1;
				var lazyload = $(this).find('>div[data-lazyload]'); 
				if(lazyload.length){
					lazyload.before(div);
				}else{
					$(this).append(div);
				}
			}
			, push : function(){
				var data = [];
				for(var i=0, l=arguments.length; i<l; i++){
					data = data.concat( arguments[i] );
				}
				for(var i=0, l=data.length; i<l; i++){
					this.addCard(data[i]);
				}
			}
			, dataLazyload:function(url, fun){
				var _me = this
				, div = this.ce('div', {'et':'', 'data-lazyload':url, 'class':'loadnext'}
				,{"callback":function(e){
					// var data = ET.execJS(e);
					var data = e.data;
					var nurl = null;
					if(data){
						while( data.data ){ 
							if(data.next_page_url) nurl = data.next_page_url;
							data = data.data; 
						}
						if(fun){data=fun(data);}
						_me.push(data);
					}
					$(this).remove();
					if(nurl){ _me.dataLazyload(nurl, fun); }
				}});
				div.onappend(function(){ $(div).show();});
				return div;
			}
		}
	}
	/**
	 * 数据映射
	 * @param  {[array]} data   源数据 
	 * @param  {[json]} mapRuler 映射规则 {'序号':'id'}
	 * @return {[type]} [description]
	 */
	,dataMap:function(data, mapRuler){
		if(!mapRuler){
			mapRuler = this.getKeyMap();
		}
		if(!mapRuler)return data;
		return data.map(function(e){
			return ET.object.map(e, mapRuler);
		})
	}
	/**
	 * 数据过虑
	 * @param  {[array]} data   源数据 
	 * @return {[type]} [description]
	 */
	,dataFilter:function(data){
		var keys = this.getKey();
		return data.map(function(e){
			return ET.object.filter(e, keys);
		})
	}
	/**
	 * 数据格式化
	 * @param  {[array]} data   源数据 
	 * @return {[type]} [description]
	 */
	,dataFormat:function(data){
		if(!data)return data;
		var reVal=[], tpl = this.getKeyTemplate()
		, dm = this.getKeyMap(), ntpl={};
		for(var k in dm){
			ntpl[dm[k]] = tpl[k];
		}
		for(var i=0, l=data.length; i<l; i++){
			var d={}, dat = data[i];
			for(var k in ntpl){
				if(!k){continue;}
				var v = dat[k]?dat[k]:'';
				if(!ntpl[k]){ d[k] = v; continue; }
				d[k] = ET.string.format(ntpl[k], dat, '\\{', '\\}');
			}
			reVal.push(d);
		}
		return reVal;
	}
	/**
	 * 显示简单视图
	 * @return {[type]} [description]
	 */
	,reShowSimple:function(){ this.displayStyle = ''; this.showSimple();}
	,showSimple:function(){
		var _me = this;
		if( this.displayStyle == 'showSimple' )return ;
		if(($(this).find("[et]")).length){ // 判断是否有未完成加载的对象
			setTimeout(function(){_me.showSimple()}, 100);
			return ;
		}
		this.displayStyle = 'showSimple';

		if(this.vt){$(this.vt).remove(); this.vt = null;}else{
			setTimeout(function(){
				$(_me).find('[data-lazyload]').each(function(){
					this.lazyload(true);					
				});
			}, 100);
			this.onDomNodeInserted(function(){
				if( _me.displayStyle != 'showSimple' )return ;
				clearTimeout(this.mtid);
				this.mtid = setTimeout(function(){
					_me.reShowSimple();
				}, 1000);
			});
		}

		var footContent = ''
		, div = this.simpleStyle.create(this.getData())
		, title = $(this).find('>caption').html();
		// 添加右键菜单
		$(div).on('contextmenu', function(){
			if(this.menu){$(this.menu).remove();}
			this.menu = ET.contextmenu.create(event, _me);
			this.menu.add('Show Origin', 'O', function(){
				_me.showOrigin();
			}).add('Delete', 'D', function(){
				_me.showOrigin();
			}).add('Insert', 'I', function(){
				_me.showOrigin();
			});
			return false;
		});
		if(this.id) $(div).attr('for', this.id);
		// auto insert card 
		$(this).find('>tbody').bind('DOMNodeInserted', function(e){
			if(!div)return ;
			var afun=function(){
				var data = _me.getRowDataByKey(e.target); 
				div.addCard(data);
			};
			var rfun=function(){
				if(($(this).find("[et]")).length){
					setTimeout(function(){rfun();}, 1);
					return ;
				}
				setTimeout(function(){afun(); }, 1);
			};
			(this.hierarchy(e.target)==1) && rfun();
		});

		if(title){ 
			var hdiv = ET.ce('div', {'class':'header'});
			var hh1 = ET.ce('h1', {innerHTML:title});
			var hhr = ET.ce('hr');
			$(hdiv).append([hh1, hhr]);
			$(div).prepend(hdiv); 
		}

		var trs = $(this).find('>tfoot>tr');
		var pagination = trs.find('ul.pagination');
		var nextDiv = null;
		if(pagination.length){
			var pia = pagination.find('>li.active')
			, pi = pia.next(), ha = pi.find('a')
			, url = ha.attr('href');
			if(url){
				div.dataLazyload(url, function(e){
					var d = _me.dataFormat(e);
					return _me.dataFilter(_me.dataMap(d));
				});
			}
		}

		$(this).after(div);
		this.vt = div;
		$(this).hide();

	}
	/**
	 * 显示原table视图
	 * @return {[type]} [description]
	 */
	,showOrigin:function(){
		if( this.displayStyle == 'showOrigin' )return ;
		this.displayStyle = 'showOrigin';
		if(this.vt){$(this.vt).remove(); this.vt = null;}
		$(this).show();
	}
	/**
	 * 统计
	 * @return {[type]} [description]
	 */
	,statistics:{
		column : function(th, data, tbl){
			if(!th)return;
			var key = tbl.getKey(th);
			var sum = 0, avg = 0;
			for(var i=0, l=data.length; i<l; i++){
				var d = data[i];
				if(!d[key]){ continue; }
				var v = d[key]+'';
				if(/^\s*[\+\-]?((\d+)|(\d+\.\d+))\s*$/gi.test(v)){
					sum += parseFloat(v);
				}
			}
			if(data.length){ avg = parseFloat((sum/data.length).toFixed(4)); }
			return {sum:sum ,avg:avg
				,count:data.length
				,key:key
			}
		}
	}
	,menu:{
		add:function(tit, hotkey, fun, targe){
			targe = targe?targe:'tbody';
			if(!this[targe]){ this[targe] = [];}
			this[targe].push({title:tit, hotkey:hotkey, fun:fun});
			return this;
		}
	}
	,event:{
		thead:{
			contextmenu:function(e){
				e = e?e:window.event;
				var _me = this, tbl = this.table
				, tds = $(this).find('>tr>td, >tr>th')
				, el = e.toElement?e.toElement:e.target?e.target:null, i=0;
				while(el && i<9999){
					if(tds.index(el) != -1){break;}
					el = $(el).parent();
				}
				if(tbl._contextmenu){ tbl._contextmenu.destroy(); }
				tbl._contextmenu = ET.contextmenu.create(e, tbl);
				var cmenu = tbl.menu['thead'];
				if( cmenu ){
					for(var i = 0, l = cmenu.length; i<l; i++){
						var m = cmenu[i];
						tbl._contextmenu.add(m.title, m.hotkey, m.fun);
					}
				}
				if(_me.RBF){
					tbl._contextmenu.add('系统标题', 'R', function(){
						_me.RBF();
					});
				}
				tbl._contextmenu.add('统计', 'S', function(){
					var data = tbl.getDataBySelected();
					if(data.length==0){ data = tbl.getData(); }
					
					var ret = tbl.statistics.column(el, data, tbl);
					if(!ret)return;
					var msg = '';
					msg += "统计列：[ " + ret.key + ' ]';
					msg += "<br>合计：" + ret.sum;
					msg += "<br>平均：" + ret.avg;
					msg += "<br>行数：" + ret.count;
					ET.messagebox.show(msg, null, null);
				}).add('降序↓', 'A', function(){
					var idx = ET.tablelib.getColIndex(el);
					tbl.orderby(idx, 'asc');
				}).add('升序↑', 'D', function(){
					var idx = ET.tablelib.getColIndex(el);
					tbl.orderby(idx, 'desc');
				}).addLine().add('精简视图', 'S', function(){
					tbl.showSimple();
				});
				return false;
			}
		}
		,tbody:{
			contextmenu:function(e){
				e = e?e:window.event;
				var tbl = this.table
				, tds = $(this).find('>tr>td, >tr>th')
				, tdIdx = tds.index(e.toElement);
				if(tdIdx==-1){return true;}
				var el = tds[tdIdx];
				function show(data){
					var json = ET.array.serialize(data);
					json = ET.string.escapeHtmlLabel(json);
					ET.messagebox.show( json );
				}
				if(tbl._contextmenu){ tbl._contextmenu.destroy(); }
				tbl._contextmenu = ET.contextmenu.create(e, tbl);
				var cmenu = tbl.menu['tbody'];
				if( cmenu ){
					for(var i = 0, l = cmenu.length; i<l; i++){
						var m = cmenu[i];
						tbl._contextmenu.add(m.title, m.hotkey, m.fun);
					}
					tbl._contextmenu.addLine();
				}
				tbl._contextmenu.add('选择', 'S', function(){
					tbl.choose($(el).parent());
				}).add('全选', 'A', function(){
					tbl.chooseAll();
				}).add('反选', 'T', function(){
					tbl.toggleChooseAll();
				}).add('清选', 'C', function(){
					tbl.unChooseAll();
				}).addLine().add('编辑', 'E', function(){
					$(el).trigger('onCanedit');
				}).add('插入', 'I', function(){
					var tr = $(el).parent()
					, ntr = tr.clone();
					ntr.find('>th,>td').each(function(){
						$(this).html('&nbsp;');
					});
					ntr.attr('class', tr.hasClass('even')?'odd':'even' );
					tbl.init.tr(ntr, tbl);
					tr.before(ntr);

				}).add('删除', 'D', function(){
					var els = $(el).parent();
					var sltData = tbl.getDataBySelected();
					if(sltData.length){
						els = $(tbl).find('>tbody>tr.selected');
					}
					els.remove();
				});
				var expMenu = tbl._contextmenu.addSubMenu('导出', 'E');
				expMenu.add('全部数据', 'A', function(){
						show( tbl.getData() );
				}).add('全部数据T', 'A', function(){ 
					show( tbl.getData(function(e){ return ET.string.fillter.html(e); }) );
				}).add('CSV', 'C', function(){ 
					var data = tbl.getData(function(e){ return ET.string.fillter.html(e); });
					var title = [];
					var csv = "";
					for(var i=0; i<data.length; i++){
						var d=data[i];
						for(var k in d){
							if(title.indexOf(k)==-1){title.push(k);}
							csv += "\""+ d[k].replace(/\n/gi, "\\n").replace(/\"/gi, "\"\"") +"\"\t";
						}
						csv += "\n";
					}
					var st='';
					for(var i=0; i<title.length; i++){
						st+="\""+title[i]+"\"\t";
					}
					csv = st+"\n" +csv;
					csv = csv.replace(/\n/gi, "<br/>");
					ET.messagebox.show( csv );
				});
				var sltData = tbl.getDataBySelected();
				if(sltData.length){
					expMenu.add('导出已选数据', 'S', function(){ 
						show( tbl.getDataBySelected() );
					}).add('导出已选数据T', 'S', function(){ 
						show( tbl.getDataBySelected(function(e){ return ET.string.fillter.html(e); }) );
					});
				}else{
					expMenu.add('导出当前数据', 'J', function(){
						var tr = $(el).parent()
						, data = tbl.getRowValue(tr);
						show( data );
					}).add('导出当前数据T', 'J', function(){
						var tr = $(el).parent()
						, data = tbl.getRowValue(tr, function(e){return ET.string.fillter.html(e); });
						show( data );
					});
				}
				tbl._contextmenu.addLine().add('精简视图', 'S', function(){
					tbl.showSimple();
				});
				return false;
			}
		}
		,tr:{
			'click':function(e){
				e = e?e:event;
				if(e.ctrlKey){this.table.toggleChoose(this);}
			}
			,'mousedown':function(e){
				if(e.shiftKey){
					this.table.onselectstart=function(){ event.returnValue=false; }
					if(!this.table.start_at){
						this.table.start_at = this;
						this.table.unChooseAll();
					}
				}else{
					this.table.start_at = null;
				}
			}
			,'mouseup':function(e){
				this.table.onselectstart=function(){ event.returnValue=true; }
				if(e.shiftKey){
					if(this.table.start_at && this.table.start_at != this){
						var s = $(this.table.start_at).index()
						,e = $(this).index()
						,trs = $(this).parent().find('>tr');
						if(s>e){s += e; e = s-e; s -= e; }
						this.table.unChooseAll();
						for(;s<=e;s++){this.table.choose(trs[s]); }
						return ;
					}
				}
			}
		}
		,td:{
			'click':function(){}
			,'dblclick':function(){
				var tbl = this.table, ent = event
				, tds = $(tbl).find('>tbody>tr').find('>td, >th')
				, tdIdx = tds.index(ent.toElement);
				if(tdIdx==-1){return true;}
				$(this).trigger('onCanedit');
				event.stopPropagation();
				return false;
			}
			,'onCanedit':function(){
				if($(this).attr('contenteditable') == 'true'){return ;}
				if (document.selection) { 
					document.selection.empty(); 
				} else if (window.getSelection) { 
					window.getSelection().removeAllRanges(); 
				} 
				$(this).attr('contenteditable', true);
				$(this).focus();
				this.data = this.table.getRowDataByKey( $(this).parent() );
			}
			,'blur':function(){
				$(this).attr('contenteditable', false);
				var data = this.table.getRowDataByKey( $(this).parent() )
				, keys = this.table.getKey()
				, idx = $(this).index()
				, key = idx<keys.length?keys[idx]:null
				, statu = ET.object.compare(this.data, data);
				this.table.edit(this, idx, key, data, statu);
			}
		}
	}
	,init:{
		__construct:function(tbl){
			var _me=this, trs = $(tbl).find('>tr')
			, caption = $(tbl).find('>caption')
			, thead = $(tbl).find('>thead')
			, tbody = $(tbl).find('>tbody')
			, tfoot = $(tbl).find('>tfoot')
			;
			if(_me.isInit(tbl))return ;
			
			if(thead.length == 0){
				thead = $(ET.ce('thead'));
				$(tbl).prepend(thead);
			}
			if(tbody.length == 0){
				tbody = $(ET.ce('tbody'));
				thead.after(tbody);
			}
			if(tfoot.length == 0){
				tfoot = ET.ce('tfoot');
				tbody.after(tfoot);
			}
			if(trs.length){ tbody.append(trs); }

			trs = thead.find('>tr');
			if( trs.length<1 ){
				trs = tbody.find('>tr:first');
				if( trs.length ){
					var tr = ET.ce('tr');
					trs.find('>th, >td').each(function(idx){
						tr.ce('th', {'innerHTML':'t'+idx});
					});
					thead[0].RBF = function(){
						var trs = $(this).find('>tr:last'), tr = ET.ce('tr', {'class' : 'odd'});
						trs.find('>th').each(function(){
							var td = ET.ce('td');
							$(td).html($(this).html());
							$(tr).append(td);
							$(this).html('col'+$(this).index());
						});
						$(tbody).prepend(tr);
						this.RBF = undefined;
					};
					thead.append(tr);
				}
				$(thead).hide();
			}
			tbl.onDomSubtreeModified(function(e){
				var el = e.srcElement;
				if( tbl.hierarchy(el) ==1 ){
					if(_me.isInit(el))return ;
					var tgn = el.tagName.toLowerCase();
					if(_me[tgn])_me[tgn](tbl);
				}
			});
		}
		,thead:function(tbl){
			var _me = this, thead = $(tbl).find('>thead');			
			thead.each(function(){
				if(_me.isInit(this))return ;
				this.table = tbl;
				_me.on(this, ET.loadExt('table').event.thead);
			});
			thead.find('>tr').each(function(){ _me.tr(this, tbl); });
		}
		,tbody:function(tbl){
			var _me = this, tbody = ET($(tbl).find('>tbody'))
			domNodeInserted = function(e){
				ET(this);
				var srcEle = e.srcElement;
				var tbody = this;
				if(!srcEle)return ;
				if(tbody.hierarchy(srcEle)!=1)return ;
				if(srcEle.tagName == 'TR') _me.tr(srcEle, tbody.table);
			};
			tbody.each(function(){
				if(_me.isInit(this))return ;
				this.table = tbl;
				_me.on(this, ET.loadExt('table').event.tbody);
				this.onDomNodeInserted(domNodeInserted);
			});
			tbody.find('>tr:odd').addClass('odd').removeClass('even');
			tbody.find('>tr:even').addClass('even').removeClass('odd');
			tbody.find('>tr').each(function(){ _me.tr(this, tbl); });
		}
		,tr:function(tr, tbl){
			tr.table = tbl;
			var _me = this;
			if(_me.isInit(tr))return ;
			this.on(tr, ET.loadExt('table').event.tr);
			$(tr).find('>td').each(function(){_me.td(this, tbl);});
			$(tr).find('>th').each(function(){_me.th(this, tbl);});
		}
		,th:function(th, tbl){
			if(this.isInit(th))return ;
			th.table = tbl;
			this.on(th, ET.loadExt('table').event.th);
		}
		,td:function(td, tbl){
			if(this.isInit(td))return ;
			td.table = tbl;
			this.on(td, ET.loadExt('table').event.td);
		}
		,on:function(e, ent){
			for(var key in ent) $(e).on(key, ent[key]);
		}
		,isInit:function(el){
			if(el.init_ext_tbl)return true;
			el.init_ext_tbl = true;
			return false;
		}
	}
}