/*
*
*	input file 扩展 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*  <input type="file" et id="hiptfile" name="hiptfile" onUploaded="" defval=""  accept="image/jpeg" action="/upload.php">
*/
{
	initialize:function(){
		ET.loadLib('upload');
		ET.loadLib('image');
		this.cid(this);
		var v = this.cui.initialize(this, this.fn);
		$(this).before(v);
		$(this).attr('disabled', 'disabled'); 
		$(this).hide();
	}
	,fn:{
		errors : [
	        '正常，上传成功'
	        , '上传文件大小超过服务器允许上传的最大值，php.ini中设置upload_max_filesize选项限制的值' 
	        , '上传文件大小超过HTML表单中隐藏域MAX_FILE_SIZE选项指定的值' 
	        , '文件只有部分被上传'
	        , ''
	        , ''
	        , '没有找不到临时文件夹'
	        , '文件写入失败 '
	        , '文件上传扩展没有打开'
		]
	}
	,cui:{
		initialize : function(obj, fn){
			var _me = this, spn = ET.ce('span', {'class' : 'ET_STYLE_UPLOAD'})
			, ipt = null
			, img = ET.ce('img', {'et':'', 'for': obj.id, 'src' : ET.imgPath+'/upload.png'})
			, onUploaded = $(obj).attr('onUploaded')
			, hiptFile = ET.ce('input', {'type':'file'})
			, upPath = $(obj).attr('path')
			// , hupload = this.createIframe(obj, upPath)
			, file = $(obj).attr('file')
			;
			if(onUploaded){ onUploaded = new Function(onUploaded); }
			if(!onUploaded && obj.onUploaded){ onUploaded = obj.onUploaded; }
			if(!onUploaded){
				Object.defineProperty(obj, 'onUploaded', {
					get:function(){ return onUploaded;}
					,set:function(v){ onUploaded = typeof(v)=='function'?v:new Function(v);; }
				});
				onUploaded = function(){};
			}

			if($(obj).attr('name')){
				ipt = ET.ce('input', {'for': obj.id, 'name' : $(obj).attr('name'), 'readonly' : true})
			}
			obj.watchAtrr('file', function(e){
				if(!e.value)return ;
				var file = e.value;
				if(file){
					if(_me.isImage(file)){
						img.setThum(file);
					}else{
						$(img).attr('src', ET.imgPath+'/file.png');
					}
					$(img).attr({'alt':file, 'title':file});
					$(ipt).val(file);
					onUploaded.call(_me, file);
				}
			});
			if(!upPath)upPath='';
			$(hiptFile).change(function(){
				if(this.value=='')return ;
				ET.upload.input(this, function(e){
					if(e.errcode==0 && e.data.length>0){
						var f = e.data[0];
						$(obj).attr('file', f.uri);
					}
				}, upPath);
			});
			$(img).click(function(){ $(hiptFile).click(); });
			$(spn).append(ET([img]));
			if(ipt) {
				$(ipt).hide();
				$(spn).append(ET(ipt));
			}
			return spn;
		}
		, isImage : function(e){
			return /.*(bmp|jpg|jpeg|png|tif|gif|pcx|tga|exif|fpx|svg|psd|cdr|pcd|dxf|ufo|eps|ai|raw|WMF|webp)$/gi.test(e);
		}
	}
	
}