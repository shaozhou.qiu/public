/*
*
*	div 扩展 1.0.0.1
*	Release date:
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		ET.loadLib('delayedLoading');
		this.cid();
		this.readystate=0;
		if( $(this).attr('data-lazyload')!==undefined || $(this).attr('delayedLoading')!==undefined ) this.data_lazyload();
	}
	,data_lazyload:function(){
		var _me=this;
		this.lazyload = function(ob){
			var _me = this;
			if(_me.readystate>0)return ;
			var url = _me.getUrl();
			if(!url) {
				_me.readystate =2;
				if(typeof(_me.callback)=='function'){_me.callback(null);}
				_me.readystate =4;
				return 1;
			}
			_me.readystate =1;
			if( $(_me).is(':visible') || ob) _me.load(url, function(e){
				_me.readystate =3;
				if(e.errcode==0){
					_me.data=e; $(_me).html('');
					var ret = _me.call('callback', e);
					ret && $(_me).html(ret);
				}else{
					var div=ET.ce('div',{'class':'tx'})
					, err = " <span class=info><a href='" + url + "'>" + e.data + "</a></span> ";
					$(div).html(err);
					$(_me).append(div);
				}
				_me.readystate =4;
			} );
			return 0;
		};
		ET.loadLib('delayedLoading', function(e){
			this.createinstance(_me, _me.lazyload);
		});
	}
	,getUrl:function(){
		if($(this).attr('data-lazyload'))return $(this).attr('data-lazyload');
		if($(this).attr('delayedLoading'))return $(this).attr('delayedLoading');
		if($(this).attr('href'))return $(this).attr('href');
		if($(this).attr('src'))return $(this).attr('src');
		if($(this).attr('action'))return $(this).attr('action');
		if($(this).attr('url'))return $(this).attr('url');
	}
	,load:function(url, fun){
		var _me=this;
		ET.getFile(url, function(e){
			var ret = {'errcode':this.status, 'data':this.statusText};
			if(this.status<400){
				ret.errcode = 0;
				ret.data = e;
			}
			fun && fun.call(this, ret);
		}, {'dataType':'json'});
	}
}