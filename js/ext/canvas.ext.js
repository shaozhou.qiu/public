/*
*
*	Canvas 扩展 1.0.0.1 
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		ET.loadLib('color');
		ET.loadLib('object');
		ET.loadLib('ruler');
		if( !$(this).html() ){ $(this).html( 'Does not support canvas'); }
		var _me = this;
		ET.loadLib('chart', function(){
			var chart = this;
			for(var k in this){
				if(k=='initialize' ||k=='fn')continue;
				_me['chart'+ET.string.ucfirst(k)]=function(){
					var data = [this];
					for(var i=0, l=arguments.length; i<l; i++){
						data.push(arguments[i]);
					}
					chart[k].apply(chart, data);
				}
			}
		});
		$.extend(this, this.fn);
	}
	,fn:{
		/**
		 * 绘制2d路径
		 * @param  {[type]} fun [description]
		 * @param  {[type]} conf 相关配置，如 lineWidth 线条宽 ， strokeStyle 线条色
		 * @param  {[type]} requestAnimationFrame 是否启用，默认为flase
		 * @return {[type]}     [description]
		 */
		draw2d:function(fun, conf, requestAnimationFrame){
			var ctx = this.getContext('2d');
			ctx.save(); //保存状态
			
			ctx.beginPath();//起始一条路径，或重置当前路径
			fun=fun?fun:function(){};
			if( ET.object.getType(fun)!= "function" ){fun=function(){}; }
			if(conf){
				for(var k in conf){
					ctx[k] = conf[k];
				}
			}
			fun.call(ctx);
			if(conf.fillStyle){
				ctx.fill();
			}
			ctx.stroke(); //绘制已定义的路径 
			ctx.restore(); //恢复画布状态
			if(requestAnimationFrame){
				var _me=this;
				ET.window.requestAnimationFrame(function(){
					_me.draw2d(fun, conf, requestAnimationFrame);
				});
			}
			return this;
		}		
		
		/**
		 * draw line
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} x2    end x
		 * @param  {[type]} y2    end y
		 * @param  {[type]} color color
		 * @return {[type]}       [description]
		 */
		,drawLine:function(x1, y1, x2, y2, color, stWidth){
			var conf={};
			if(color){ conf.strokeStyle = ET.color.toString(color); }
			if(stWidth){ 
				conf.lineWidth = stWidth; 
				if(stWidth==1){
					if(x1==x2 && /^\d+$/gi.test(x1+'')){
						x1=x2=x1+0.5;
					}
					if(y1==y2 && /^\d+$/gi.test(y1+'')){
						y1=y2=y1+0.5;
					}
				}
			}
			x1=x1?x1:0; y1=y1?y1:0;
			x2 = x2?x2:x1+100; y2 = y2?y2:y1+100;
			return this.draw2d(function(){
				this.moveTo(x1, y1); this.lineTo(x2, y2);
			}, conf);
		}
		/**
		 * draw Rect
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} x2    end x
		 * @param  {[type]} y2    end y
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawRect:function(x1, y1, x2, y2, rx, ry, bgColor, stColor, stWidth){
			var conf={};
			stColor = stColor?stColor:bgColor;
			if(bgColor){ conf.fillStyle = ET.color.toString(bgColor); }
			if(stColor){ conf.strokeStyle = ET.color.toString(stColor); }
			if(stWidth){ conf.lineWidth = stWidth; }

			return this.draw2d(function(){
				x1 = x1?x1:0; y1 = y1?y1:0;
				x2 = x2?x2:x1+100; y2 = y2?y2:y1+100;
				rx = rx?rx:0; ry = ry?ry:rx;
				width = x2 - x1; height = y2 - y1;
				if(width<0){width *= -1; x1 -= width; }
				if(height<0){height *= -1; y1 -= height; }
				this.rect(x1, y1, width, height);
			}, conf);
			
		}
		/**
		 * draw Circle
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} r    radius
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawCircle:function(x, y, r, bgColor, stColor, stWidth){
			var conf={};
			stColor = stColor?stColor:bgColor;
			if(bgColor){ conf.fillStyle = ET.color.toString(bgColor); }
			if(stColor){ conf.strokeStyle = ET.color.toString(stColor); }
			if(stWidth){ conf.lineWidth = stWidth; }

			return this.draw2d(function(){
				this.arc(x, y, r, 0, 2*Math.PI);
			}, conf);
		}
		/**
		 * draw Ellipse
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} cx   The radius of the ellipse along the x axis
		 * @param  {[type]} cy   The radius of the ellipse along the y axis
		 * @param  {[type]} rotation Ellipse rotation angle in radians
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawEllipse:function(cx, cy, rx, ry, rotation, bgColor, stColor, stWidth){
			var conf={};
			stColor = stColor?stColor:bgColor;
			if(bgColor){ conf.fillStyle = ET.color.toString(bgColor); }
			if(stColor){ conf.strokeStyle = ET.color.toString(stColor); }
			if(stWidth){ conf.lineWidth = stWidth; }

			return this.draw2d(function(){
				this.ellipse(cx, cy, rx, ry, 0, rotation?rotation:0, Math.PI*2);
			}, conf);
		}
		/**
		 * draw Polygon
		 * @param  {[type]} points   [{x:0, y:0},{x:10, y:20}] or [[0,0],[10,20]]
		 * @param  {[type]} cx   The radius of the ellipse along the x axis
		 * @param  {[type]} cy   The radius of the ellipse along the y axis
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawPolygon:function(points, bgColor, stColor, stWidth){
			var conf={};
			stColor = stColor?stColor:bgColor;
			if(bgColor){ conf.fillStyle = ET.color.toString(bgColor); }
			if(stColor){ conf.strokeStyle = ET.color.toString(stColor); }
			if(stWidth){ conf.lineWidth = stWidth; }

			return this.draw2d(function(){
				for(var i=0; i<points.length; i++){
					var p = points[i];
					if(i==0){
						this.moveTo(p.x, p.y);
						continue;
					}
					this.lineTo(p.x, p.y);
				}
				this.closePath();
			}, conf);
		}
		/**
		 * draw Polyline
		 * @param  {[type]} points   [{x:0, y:0},{x:10, y:20}] or [[0,0],[10,20]]
		 * @param  {[type]} cx   The radius of the ellipse along the x axis
		 * @param  {[type]} cy   The radius of the ellipse along the y axis
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawPolyline:function(points, bgColor, stColor, stWidth){
			var conf={};
			stColor = stColor?stColor:bgColor;
			if(bgColor){ conf.fillStyle = ET.color.toString(bgColor); }
			if(stColor){ conf.strokeStyle = ET.color.toString(stColor); }
			if(stWidth){ conf.lineWidth = stWidth; }

			return this.draw2d(function(){
				for(var i=0; i<points.length; i++){
					var p = points[i];
					if(i==0){
						this.moveTo(p.x, p.y);
						continue;
					}
					this.lineTo(p.x, p.y);
				}
			}, conf);
		}
		/**
		 * draw text
		 * @param  {[type]} x The X-axis position of the list. The position of the nth character in the text is on the nth x-axis. If there are additional characters following, exhaust them from where they were placed after the last character. 0 is the default
		 * @param  {[type]} y The Y-axis position of the list. (Reference x) 0 is the default "
dx = "Move the absolute position of the last drawn glyph in the length list of characters. (Refer to x)
		 * @param  {[type]} dx Moves the absolute position of the glyph relative to the last drawn glyph in the length list of characters. (Reference x)
		 * @param  {[type]} dy Moves the absolute position of the glyph relative to the last drawn glyph in the length list of characters. (Reference x)
		 * @param  {[type]} rotate A rotated list. The nth rotation is the nth character. Extra characters do not give the final rotation value
		 * @param  {[type]} text
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawText:function(x, y, text, dx, dy, rotate, bgColor, stColor, stWidth){
			var conf={};
			stColor = stColor?stColor:bgColor;
			if(bgColor){ conf.fillStyle = ET.color.toString(bgColor); }
			if(stColor){ conf.strokeStyle = ET.color.toString(stColor); }
			if(stWidth){ conf.lineWidth = stWidth; }

			return this.draw2d(function(){
				this.fillText(text, x, y);
			}, conf);
		}
		/**
		 * 绘制图像、画布或视频
		 * @param  {[type]}	img	规定要使用的图像、画布或视频。
		 * @param  {[type]}	sx	可选。开始剪切的 x 坐标位置。
		 * @param  {[type]}	sy	可选。开始剪切的 y 坐标位置。
		 * @param  {[type]}	swidth	可选。被剪切图像的宽度。
		 * @param  {[type]}	sheight	可选。被剪切图像的高度。
		 * @param  {[type]}	x	在画布上放置图像的 x 坐标位置。
		 * @param  {[type]}	y	在画布上放置图像的 y 坐标位置。
		 * @param  {[type]}	width	可选。要使用的图像的宽度。（伸展或缩小图像）
		 * @param  {[type]}	height	可选。要使用的图像的高度。（伸展或缩小图像）
		 * @param  {[type]} conf    
		 * @return {[type]}         [description]
		 */
		,drawImage:function(img, sx, sy, swidth, sheight, x, y, width, height, conf){
			if(ET.object.getType(img)=="string"){
				var ext = img.replace(/.*\.(\w+)$/gi, '$1')
				if(ext){
					ext=ext.toLowerCase();
					if(ext=='jpg'){ext='jpeg';}
					var o=null;
					if(ET.mediatypes.getIANAMIME(ET.mediatypes.IANAMIME_IMAGE, ext)){o=new Image(); o.src=img; }
					else if(ET.mediatypes.getIANAMIME(ET.mediatypes.IANAMIME_VIDEO, ext)){ o=ET.ce('video', {'src':img}); }
					else if(ET.mediatypes.getIANAMIME(ET.mediatypes.IANAMIME_AUDIO, ext)){ o=ET.ce('audio', {'src':img}); }
					else{o=new Image(); o.src=img; }
					if(o){
						var _me=this, div=ET.ce('div');
						$(div).css({
							'visibility': 'visible','position': 'fixed'
							,'top': ET.unit.pxorem('-999rem')
							,'left': ET.unit.pxorem('-999rem')
						});
						div.onappend(function(){
							_me.drawImage(o, sx, sy, swidth, sheight, x, y, width, height, conf);
							$(this).remove();
						});
						$(div).append(o);
						$('body').append(div);
						return this;
					}
				}
			}
			sx=sx?sx:0; sy=sy?sy:0;
			x=x?x:0; y=y?y:0;
			return this.draw2d(function(){
				this.drawImage(img, sx, sy, swidth, sheight, x, y, width, height);
			}, conf);
		}		
		/**
		 * draw text
		 * @param  {[type]} x
		 * @param  {[type]} y
		 * @param  {[type]} w
		 * @param  {[type]} h
		 * @param  {[type]} min
		 * @param  {[type]} max
		 * @param  {[type]} branch 分段数量
		 * @param  {[type]} fo 刻度及字段方向 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawRuler:function(x1, y1, x2, y2, min, max, branch, fo, bgColor, stColor, stWidth, callback){
			min = min?min:0;
			max = max?max:100;
			var rdata = ET.ruler.direction(x1, y1, x2, y2);
			var ruler = ET.ruler.createInstance(rdata.min, rdata.max, min, max, branch);
			var _me = this;
			var op = null;
			var direction = rdata.direction;
			stColor=stColor?stColor:0x888888;

			ruler.getScale(function(e){
				var x = x1, y = y1
				, val = e.data.toFixed(2) , txt = ''
				, l=5
				, p = ET.ruler.getPosByDirection({x:x, y:y}, direction, function(v, d){ 
					return (e.scale-v)*((d==0 || d==3)?-1:1); 
				});
				// 刻度座标
				var cp = ET.ruler.getPosByDirection(p, (direction == 0 || direction == 2)?(fo?1:3):(fo?0:2), l*3);

				// 大刻度
				_me.drawLine(p.x, p.y, cp.x, cp.y, stColor, 1);

				if(op){
					// 刻度水平线
					_me.drawLine(op.x, op.y, p.x, p.y, stColor, 1);

					// 小刻度
					var scd = ET.ruler.direction(op.x, op.y, p.x, p.y);
					var sdl = (scd.max - scd.min)/9;
					var scdd = scd.direction;
					if(scdd==2){ scdd = 0; }
					else if(scdd==0){ scdd = 2; }
					var tp = op;
					for(var i=0; i<9; i++){
						var scp = ET.ruler.getPosByDirection(tp, scdd, sdl);
						var ndd = (scdd==0||scdd==2)?(fo?1:3):(fo?0:2);
						var nscp = ET.ruler.getPosByDirection(scp, ndd, i==4?l*2:l);
						_me.drawLine(scp.x, scp.y, nscp.x, nscp.y, stColor, 1);
						tp = {x:scp.x, y:scp.y };
					}
				}
				op = {x:p.x, y:p.y};

				// 刻度值
				txt = val;
				if(direction==2 || direction==3){
					txt = ruler.getOppositeData(val);
				}
				var fs = ET.string.size(txt);
				if(direction ==2){ cp.x -= fs.width; }
				if(direction ==3){ cp.y -= (fs.height - fs.height/(txt+'\n').match(/\n/gi).length) ; }
				if(direction ==1){ cp.y += fs.height; }
				_me.drawText(cp.x, cp.y, txt, 0, 0, 0, stColor);
				
			});
			return ruler;
		}
	}
	
}