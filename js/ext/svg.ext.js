/*
*
*	Canvas 扩展 1.0.0.1 
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		ET.loadLib('object');
		if( !$(this).html() ){ $(this).html( 'Does not support svg'); }
		var _me = this;
		ET.loadLib('chart', function(){
			var chart = this;
			for(var k in this){
				if(k=='initialize' ||k=='fn')continue;
				_me['chart'+ET.string.ucfirst(k)]=function(){
					var data = [this];
					for(var i=0, l=arguments.length; i<l; i++){
						data.push(arguments[i]);
					}
					chart[k].apply(chart, data);
				}
			}
		});
		ET.extend(this, this.fn);
	}
	, obj2Style:function(obj){
		var style = ET.object.toString(obj);
		if(!style || style=='')style='{}';
		style = style.substring(1, style.length-1);
		style = style.replace(/"/gi, '');
		style = style.replace(/,[\n\r]/gi, ';');
		return style;
	}
	, array2point:function(data){
		var npos = {
			'x':data['x']?data['x']:data[0]?data[0]:0
			,'y':data['y']?data['y']:data[1]?data[1]:0
		};
		for(var k in data){
			if(k==0 || k==1 || k=='x' || k=='y')continue;
			npos[k] = data[k];
		}
		return npos;
	}
	, array2points:function(data){
		var points = [];
		for(var i=0, l=data.length; i<l; i++){
			points.push(this.array2point(data[i]));
		}
		return points;
	}
	, createCss:function(bgColor, stColor, stWidth){
		if(typeof(bgColor)== "number") bgColor = '#'+bgColor.toString(16);
		if(typeof(stColor)== "number") stColor = '#'+stColor.toString(16);
		stWidth = stWidth?stWidth:0;
		bgColor = bgColor?bgColor:stWidth?'transparent':'#000';
		stColor = stColor?stColor:'#000';		
		var style ={
			'fill' : bgColor,
			'stroke' : stColor,
			'stroke-width' : stWidth
		};
		style = this.obj2Style(style);
		return style;
	}
	,fn:{
		/**
		 * draw 
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} x2    end x
		 * @param  {[type]} y2    end y
		 * @param  {[type]} color color
		 * @return {[type]}       [description]
		 */
		draw:function(tagName, bgColor, stColor, stWidth, callback){
			var attr = {
				'style':this.createCss(bgColor, stColor, stWidth)
			};
			var e = this.ceNS(tagName, attr);
			if(callback){
				var val = callback;
				if(typeof(callback)=="function"){
					val = callback.call(e);
				}
				$(e).attr(val?val:{});
				// $.extend(attr, val?val:{});
			}
			$(this).append(e);
			return e;
		}
		/**
		 * draw line
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} x2    end x
		 * @param  {[type]} y2    end y
		 * @param  {[type]} color color
		 * @return {[type]}       [description]
		 */
		,drawLine:function(x1, y1, x2, y2, color, stWidth){
			return this.draw('line', null, color, stWidth?stWidth:1, function(){
				x1 = x1?x1:0; y1 = y1?y1:0;
				x2 = x2?x2:x1+100; y2 = y2?y2:y1+100;
				return {'x1':x1, 'y1':y1, 'x2':x2, 'y2':y2 };
			});
		}
		/**
		 * draw Rect
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} x2    end x
		 * @param  {[type]} y2    end y
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawRect:function(x1, y1, x2, y2, rx, ry, bgColor, stColor, stWidth){
			return this.draw('rect', bgColor, stColor, stWidth, function(){
				x1 = x1?x1:0; y1 = y1?y1:0;
				x2 = x2?x2:x1+100; y2 = y2?y2:y1+100;
				rx = rx?rx:0; ry = ry?ry:rx;
				width = x2 - x1; height = y2 - y1;
				if(width<0){width *= -1; x1 -= width; }
				if(height<0){height *= -1; y1 -= height; }
				return {'x':x1, 'y':y1,
					'rx':rx, 'ry':ry,
					'width':width, 'height':height
				};
			});
		}
		/**
		 * draw Circle
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} r    radius
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawCircle:function(x, y, r, bgColor, stColor, stWidth){
			return this.draw('circle', bgColor, stColor, stWidth, function(){
				x = x?x:1; y = y?y:1;
				r = r?r:1;
				return {'cx':x, 'cy':y, 'r':r};
			});
		}
		/**
		 * draw Ellipse
		 * @param  {[type]} x    postion x
		 * @param  {[type]} y    postion y
		 * @param  {[type]} cx   The radius of the ellipse along the x axis
		 * @param  {[type]} cy   The radius of the ellipse along the y axis
		 * @param  {[type]} rotation Ellipse rotation angle in radians
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawEllipse:function(cx, cy, rx, ry, rotation, bgColor, stColor, stWidth){
			return this.draw('ellipse', bgColor, stColor, stWidth, function(){
				cx = cx?cx:1; cy = cy?cy:1;
				rx = rx?rx:5; ry = ry?ry:10;
				return {'cx':cx, 'cy':cy, 'rx':rx , 'ry':ry};
			});
		}
		/**
		 * draw Polygon
		 * @param  {[type]} points   [{x:0, y:0},{x:10, y:20}] or [[0,0],[10,20]]
		 * @param  {[type]} cx   The radius of the ellipse along the x axis
		 * @param  {[type]} cy   The radius of the ellipse along the y axis
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawPolygon:function(points, bgColor, stColor, stWidth){
			var _me = this;
			return this.draw('polygon', bgColor, stColor, stWidth, function(){
				var ptxt = '';
				points = _me.array2points(points);
				for(var i=0, l=points.length; i<l; i++){
					var p = points[i];
					ptxt+=' '+p['x']+','+p['y']
				}
				ptxt = ptxt.substring(1);
				return {'points': ptxt};
			});
		}
		/**
		 * draw Polyline
		 * @param  {[type]} points   [{x:0, y:0},{x:10, y:20}] or [[0,0],[10,20]]
		 * @param  {[type]} cx   The radius of the ellipse along the x axis
		 * @param  {[type]} cy   The radius of the ellipse along the y axis
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawPolyline:function(points, bgColor, stColor, stWidth){
			var _me = this;
			return this.draw('polyline', bgColor, stColor, stWidth?stWidth:1, function(){
				var ptxt = '';
				points = _me.array2points(points);
				for(var i=0, l=points.length; i<l; i++){
					var p = points[i];
					ptxt+=' '+p['x']+','+p['y']
				}
				ptxt = ptxt.substring(1);
				return {'points': ptxt};
			});
		}
		/**
		 * draw path
		 * @param  {[type]} points   [{c:'m', x:0, y:0}]
			M = moveto
			L = lineto
			H = horizontal lineto
			V = vertical lineto
			C = curveto
			S = smooth curveto
			Q = quadratic Belzier curve
			T = smooth quadratic Belzier curveto
			A = elliptical Arc
			Z = closepath
		 * @param  {[type]} closepath default true
		 * @param  {[type]} bgColor fill color with background 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawPath:function(points, closepath, bgColor, stColor, stWidth){
			var _me = this;
			return this.draw('path', bgColor, stColor, stWidth?stWidth:1, function(){
				var ptxt = '';
				points = _me.array2points(points);
				for(var i=0, l=points.length; i<l; i++){
					var p = points[i]
					, c = p['c']?p['c'].toUpperCase():'M';
					ptxt+=' '+c+p['x']+' '+p['y']
				}
				ptxt = ptxt.substring(1);

				if(closepath==true || closepath==undefined){
					ptxt += ' Z';
				}
				return {'d': ptxt};
			});
		}
		/**
		 * draw text
		 * @param  {[type]} x The X-axis position of the list. The position of the nth character in the text is on the nth x-axis. If there are additional characters following, exhaust them from where they were placed after the last character. 0 is the default
		 * @param  {[type]} y The Y-axis position of the list. (Reference x) 0 is the default "
dx = "Move the absolute position of the last drawn glyph in the length list of characters. (Refer to x)
		 * @param  {[type]} dx Moves the absolute position of the glyph relative to the last drawn glyph in the length list of characters. (Reference x)
		 * @param  {[type]} dy Moves the absolute position of the glyph relative to the last drawn glyph in the length list of characters. (Reference x)
		 * @param  {[type]} rotate A rotated list. The nth rotation is the nth character. Extra characters do not give the final rotation value
		 * @param  {[type]} text
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawText:function(x, y, text, dx, dy, rotate, bgColor, stColor, stWidth){
			var e = this.draw('text', bgColor?bgColor:'black', stColor?stColor:'none', stWidth?stWidth:1, function(){
				var attr = {
					'x' : x?x:0
					,'y' : y?y:12
				};
				if(dx !=null && dx !==undefined){ $.extend(attr, {'dx':dx}); }
				if(dy !=null && dy !==undefined){ $.extend(attr, {'dy':dy}); }
				if(rotate !=null && rotate !==undefined){ $.extend(attr, {'rotate':rotate}); }
				return attr;
			});
			$(e).html(text);
			return e;
		}
		/**
		 * draw text
		 * @param  {[type]} x
		 * @param  {[type]} y
		 * @param  {[type]} w
		 * @param  {[type]} h
		 * @param  {[type]} min
		 * @param  {[type]} max
		 * @param  {[type]} branch 分段数量
		 * @param  {[type]} fo 刻度及字段方向 
		 * @param  {[type]} stColor stroke color
		 * @return {[type]}       [description]
		 */
		,drawRuler:function(x1, y1, x2, y2, min, max, branch, fo, bgColor, stColor, stWidth, callback){
			var _me = this
			,ruler = {
				'x1' : x1, 'y1' : y1
				,'x2' : x2, 'y2' : y2
				,'width' : (x2-x1)*((x2-x1)>-1?1:-1)
				,'height' : (y2-y1)*((y2-y1)>-1?1:-1)
				,'branch' : branch?branch:10
				,'data' : {min:min, max:max}
			};
			/**
			 * 
			 * @param  {[type]}   x        [description]
			 * @param  {[type]}   y        [description]
			 * @param  {[type]}   step     每个刻度间距
			 * @param  {[type]}   number   刻度数量
			 * @param  {[type]}   so       尺子方向
			 * @param  {[type]}   to       刻度方向
			 * @param  {Function} callback [description]
			 * @return {[type]}            [description]
			 */
			function drawScale(x, y, step, number, so, to, callback){
				var points=[]
				, h = 5
				, m = {x:x, y:y}
				, g = _me.draw("g", bgColor?bgColor:'black', stColor?stColor:'none', stWidth?stWidth:1, function(){
					return {'id': 'hsvg_ruler_'+ET.random.Hex(6)};
				})
				, txts = []
				;
				if(!step) step= 10;
				if(!number) number= 10;
				if(!so) so = 1;
				if(to == undefined) to = 1;
				for (var i = 0; i < number+1; i++) {
					var c={x:0, y:0}, t={x:0, y:0}, lh=h, stp = points.length?step:0;
					if(i%5 == 0){ lh += h; }
					if(i%10 == 0){ lh += h; }
					if( so == 1 ){ 
						m.y -= stp;
						c.x = m.x + (to?lh:lh*-1); 
						c.y = m.y;
					}else if( so == 2 ){ 
						m.x += stp;
						c.x = m.x;
						c.y = m.y + (to?lh:lh*-1); 
					}else if( so == 3 ){ 
						m.y += stp;
						c.x = m.x + (to?lh:lh*-1); 
						c.y = m.y;
					}else if( so == 4 ){ 
						m.x -= stp;
						c.x = m.x;
						c.y = m.y + (to?lh:lh*-1); 
					}
					points.push({'c':i?'l':'m', 'x':m.x, 'y':m.y});
					points.push({'c':'l', 'x':c.x, 'y':c.y});
					points.push({'c':'l', 'x':m.x, 'y':m.y});
					if(lh/h == 3){
						t.x = c.x; t.y = c.y;
						var txt = i;
						if(callback){
							txt = callback.call(g, m, c, lh/h, i);
						}
						if( so==2 || so==4 ){
							t.y += to?ET.string.width('0'):ET.string.width(txt+'')-1;
						}else{
							t.x += to?ET.string.width('0'):ET.string.width(txt+'')*-1;
						}
						txts.push(_me.drawText(t.x, t.y, txt));
					}					
				}
				var p = _me.drawPath(points, false, null, 0x8c8c8c);
				$(g).append(p);
				$(g).append(txts);

			}
			$.extend(ruler, {
				'step' : 5
				, isHorizontal : function(){ return this.width>this.height; }
				, to : function(){
					var val = 2
					, x = this.x2-this.x1 
					, y = this.y2-this.y1
					, vh=this.isHorizontal();
					if(vh && x<0){
						val = 4;
					}else if(!vh && y<0){
						val = 1;
					}else if(!vh && y>0){
						val = 3;
					}
					return val;
				}
				, ratio : function(){
					var dataRange = this.data.max-this.data.min;
					return dataRange/(this.isHorizontal()?this.width:this.height);
				}
				, getPos : function(n){
					var v = n-this.data.min
					, l = this.isHorizontal()?this.width:this.height
					, pixl = l/(this.data.max-this.data.min)
					, to = this.to()
					;
					v = v * pixl;
					if(to==1){
						v = y1 - v;		
					}else if(to==2){
						v = x1 + v;
					}else if(to==3){
						v = y1 + v;
					}else if(to==4){
						v = x1 - v;
					}
					return v;

				}
				,draw : function(){
					var _me=this, vh=this.isHorizontal();;
					var dataRange = this.data.max-this.data.min;
					var ratio = this.ratio();
					var scb = this.branch*10;
					var step = (vh?this.width:this.height)/scb;
					drawScale(this.x1, this.y1, step, scb, this.to(), fo, function(m, c, h, i){
						var l = _me.data.max-_me.data.min;
						var v = l/scb * i;
						if(l>100){
							v = v.toFixed(0);
						}else if(l<100 && l>10 ){
							v = v.toFixed(1);
						}else{
							v = v.toFixed(2);
						}
						if(callback){
							v = callback(v*1);
						}
						return v;
					});
				}
			});
			ruler.draw();
			return ruler;
		}
	}
	
}