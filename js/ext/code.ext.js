/*
*
*	code 扩展 1.0.0.1 
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
    initialize:function(){
        ET.loadLib('string');
        ET.loadLib('stx');
        ET.loadLib('code');
        this._getType();
        if(this.type && ET.stx){
            var _me=this, m = /(\w+)\/(\w+)/gi.exec(this.type);
            if(m){ 
                ET.stx.load(m[2], function(){
                    $(_me).addClass('ET_CODE');
                    _me.stx = this;
                    _me.rendering();
                    $(_me).on('DOMSubtreeModified', function(){
                        if(this._render ){ return }
                        _me.rendering();
                    });
                }); 
            }
        }else{
            return {errcode:1001, data:'The stx component not loaded', lib:['stx']};
        }        
    }
    , rendering:function(){
        this._render = true;
        code = this.stx.rendering(this.getCode()) ;
        code = ET.string.exclude(code, /\<[^\>]*\>/gi, function(c){
            c = c.replace(/ /gi, "&nbsp;");
            c = c.replace(/\t/gi, "&nbsp;&nbsp;&nbsp;&nbsp;");
            return c.replace(/[\n\r]/gi, "<br />");
        });
        // code = code.replace(/\n/gi, '<br />');
        $(this).html( code );
        this._render = false;        
    }
        /**
     * 获取代码
     * @return {[type]} [description]
     */
    ,getCode:function(){
        var text = $(this).text();
        return text;

    }
    ,_getType:function(){
        this.type = $(this).attr('type');
        if(this.type){ 
            if( !/(\w+)\/(\w+)/gi.test(this.type) ){ this.type = "text\/"+this.type;}
            this.type = this.type.replace(/\s+/gi,''); 
        }
        if(this.type){ return ;}
        var code = this.getCode();
        if( /^[\s\r\n]*<?(php)?\s/gi.test(code) ){
            this.type = 'text/php';
        }
    }

}