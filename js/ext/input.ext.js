/*
*
*	input 扩展 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		// if(!ET.responsive) ET.loadLib('responsive');
		if(!ET.data) ET.loadLib('data');
		if(!ET.unit) ET.loadLib('unit');
		var _me = this;
		this.cid();
		var type = $(this).attr("type")?$(this).attr("type").toLowerCase():'text'
		,tClass="ET_STYLE_INPUT_"+type.toUpperCase()
		,isSupportType = (this.type.toLowerCase() == type)
		,ext = type;
		!$(this).hasClass(tClass) && $(this).addClass(tClass);
		if(type!='hidden') this.iniPlaceholder();
		if(type =='file') this.iniUpload();
		if(type =='password') this.iniPassword();
		if("date;week;month;time;datetime;datetime-local".indexOf(type)>-1 ){ ext='calendar'; }
		if(!isSupportType){this.extend(ext); }
	}
	/**
	 * 传输加密
	 * @return {[type]} [description]
	 */
	,iniPassword:function(){
		var _me=this, ipt = $(this).clone();
		$(ipt).attr('name','');
		$(this).attr('id','');
		$(this).after(ipt).hide();
		$(ipt).change(function(event) {
			$(_me).val(this.value);
		});
	}
	/**
	 * 初始化上传
	 * @return {[type]} [description]
	 */
	,iniUpload:function(){
		this.extend('input_file');
	}
	
	//验证数据是否,符合rules属性指定的规则。供form提交时验证
	,verification:function(){
		var _me=this,ret={errcode:0, data:'ok'}
		,rules = $(this).attr('rules')
		,required = $(this).attr('required')
		,msg=$(this).attr('placeholder')?$(this).attr('placeholder'):''
		,intMax=Math.pow(2,31)-1
		,min=(!this.minLength || this.minLength<0)?0:this.minLength
		,max=(!this.maxLength || this.maxLength<0)?intMax:this.maxLength ;
		min=(min>intMax)?intMax:min; min=(min<0)?0:min;
		max=(max>intMax)?intMax:max; max=(max<1)?1:max;

		required=!required?false:true;
		if(msg==''){msg = this.id;}
		msg = '['+msg+']';
		if(required && this.value==''){return tx({errcode:10000, data:msg+'Can not be empty!'}); }
		if(rules){
			if(! ET.data.verification(this.value, rules) ){
				return tx({errcode:10001, data:msg+'The data is illegal!'});
			}
		}		
		if(!ET.data.lengthrangelimit(this.value, min, max)){
			return tx({errcode:10002, data:msg+'data length is not within the scope of!'});
		}
		function tx(data){$('[for='+_me.id+']').addClass('tx'); return data; }
		$('[for='+this.id+']').removeClass('tx');
		return ret;
	}
	,iniPlaceholder:function(){
		var placeholder = $(this).attr('placeholder');				
		if(!('placeholder' in document.createElement('input')) && placeholder ){ this.CVPlaceholder(placeholder); }
		// this.CVPlaceholder(placeholder);
		return this;
	}
	/**
	 * 创建虚拟 placeholder
	 * @return {[type]} [description]
	 */
	,CVPlaceholder:function(txt){
		if(this.vplaceholder!==undefined){ return this; }
		var _me = this
		, div = ET.ce('div', {'for':this.id+'_placeholder', 'class':'ET_PLACEHOLDER', 'innerHTML':txt});
		$(this).keyup(function(){
			if(this.value.length>0){$(div).hide(); }else{$(div).show(); }
		});

		var of=$(this).offset(), w = $(this).width() + $(this).css('border-left-width').replace(/[^\d]+/,'')*1
			+ $(this).css('border-right-width').replace(/[^\d]+/,'')*1
			,h = $(this).height() + $(this).css('border-top-width').replace(/[^\d]+/,'')*1
			+ $(this).css('border-bottom-width').replace(/[^\d]+/,'')*1;
		$(div).css({
			'width':ET.unit.pxorem(w+'px')
			,'height':ET.unit.pxorem(h+'px')
			,'overflow':'hidden'
			,'font-size': ET.unit.pxorem($(this).css('font-size'))
			// ,'top':of.top+'px'
			// ,'left':of.left+'px'
		}).click(function(){$(_me).focus();	});
		$(this).before(div);
		return this;
	}


}