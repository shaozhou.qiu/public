/*
*
*	img 扩展 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*	
* 	thum : 缩略图显示
*/
{
	initialize:function(){
		if(!ET.delayedLoading) ET.loadLib('delayedLoading');
		if(!ET.image) ET.loadLib('image');
		this.cid();
		this.readystate =0;
		if( $(this).attr('data-lazyload') || $(this).attr('delayedLoading') ) this.data_lazyload();
		var m = /^(\s*thum:\/\/)(.*)/gi.exec(this.src+'');
		if(m){ $(this).attr('thum', m[2]); }
		var thum = $(this).attr('thum');
		if(thum)this.setThum(thum);

		// 监控节点属性变化
		var obj =ET.element.new(this), _me = this;
		obj.mutationObserver(
			function(e){
				for(var i=0; i<e.length; i++){
					if(e[i].attributeName == "data-lazyload"
						|| e[i].attributeName == "delayedLoading"
						){
							_me.src = $(_me).attr(e[i].attributeName);
					}
				}
			}
		);
	}
	,setThum:function(url){
		var _me = this, el = $(this);
		while( el.height() == 0 && el.width() == 0 ){
			el = el.parent();
		}
		this.originUrl = url;

		var div = ET.ce('div');
		$(div).css({"display": "inline-block"});
		$(this).before(div);
		$(div).append(this);
		if(!div.thumevent){
			$(div).mouseover(function(){_me.showOrigin();}).mouseleave(function(){_me.destroyOrigin();});
			div.thumevent = true;
		}
		this.src = ET.imgPath+'/data-lazyload.gif';
		ET.image.preload(ET.image.thumbnail(url, el.width(), el.height()), function(e){ _me.src = e==200?this.src:''; });

	}
	, showOrigin : function(){
		if(!this.originUrl)return;
		if(this.originImg) return;
		var _me=this, img = ET.ce('img', {src:ET.imgPath+'/data-lazyload.gif', 'class':'ET_STYLE_IMG_ZONE', 'style':'z-index:'+document.all.length});
		$(_me).after(img);
		$(img).click(function(){
			window.open(this.src);
		});
		function repos(){
			var rect = ET.element.getRect(_me);
			var forrect = ET.element.getRect(img);
			var bdy = ET.element.getRect(document.body);
			var t=0, l=0;
			if(rect.left<bdy.width*0.5 && rect.top<bdy.height*0.5 ){
				t = rect.top + rect.height; l = rect.left + rect.width;
			}else if(rect.left>bdy.width*0.5 && rect.top<bdy.height*0.5 ){
				t = rect.top + rect.height; l = rect.left - forrect.width;
			}else if(rect.left>bdy.width*0.5 && rect.top>bdy.height*0.5 ){
				t = rect.top-forrect.height; l = rect.left - forrect.width;
			}else if(rect.left<bdy.width*0.5 && rect.top>bdy.height*0.5 ){
				t = rect.top-forrect.height; l = rect.left - rect.width;
			}
			$(img).css({'top' : (t-10)+'px','left' : (l-10)+'px'});
		}
		repos();
		ET.image.preload(this.originUrl, function(e){ img.src = e==200?this.src:''; repos();});
		this.originImg = img;
		return img;
	}
	, destroyOrigin : function(){
		if(!this.originImg)return;
		$(this.originImg).remove();
		this.originImg = null;
	}

	,data_lazyload:function(){
		ET.delayedLoading.createinstance(this, function(){
			if(this.readystate>0)return ;
			var url = this.getUrl();			
			this.src = url;
			this.readystate =4;
		});
	}
	,getUrl:function(){
		if($(this).attr('data-lazyload'))return $(this).attr('data-lazyload');
		if($(this).attr('delayedLoading'))return $(this).attr('delayedLoading');
		if($(this).attr('href'))return $(this).attr('href');
		if($(this).attr('src'))return $(this).attr('src');
		if($(this).attr('action'))return $(this).attr('action');
		if($(this).attr('url'))return $(this).attr('url');
	}
}