/*
*
*	ip Beta 1.0.0.1  IP地址处理库
*	Release date: 2019-6-28
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		ET.loadLib('country');
		this.idx = 0;
		this.api = [];
		for(var k in this.infoFN){
			this.api.push(k);
		}
	},
	getInfo:function(ipv4, fun){ return this.infomation(ipv4, fun); },
	infomation:function(ipv4, fun){
		return ET.ip._get(ET.appPath+'/EtSoftWare.php?m=Ip&ipv4='+ipv4, fun, function(e){
			
			var data = e;
			if(typeof(data) != 'object' || data == null){ return null; }

			var country = ET.country.getInfoByCn(data.country);
			return {
				errcode:0,
				region:data.area,
				city:data.area,
				country:country?country:{"cn":data.country},
				ip:ipv4,
				isp:data.area,
			};
		});
	},
	_get:function(apiUrl, callback, pretreatment){
		pretreatment = pretreatment?pretreatment:function(e){return e;};
		var funAnalysis = function(e){
			if(!e) return ;
			return pretreatment(e);
		}, fun = function(e, conf){
			if(!e)return ;
			if((this.statusText && this.statusText.toLowerCase() == 'load')
				|| (conf && conf.dataType == 'jsonp')
				){
				var re = ET.execJS(e);
				e = re.data;
			}			
			return callback(funAnalysis(ET.execJS(e))); 
		}
		if(!callback){ return ;}
		var ret = ET.getFile(apiUrl, fun);
		return funAnalysis( ret );		
	}
}