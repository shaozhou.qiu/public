/*
*
*	responsive Beta 1.0.0.1 响应式插件
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		var _me = this;
		$(window).resize(function(){_me.resize();});
		$.extend(this.cls.prototype, this.clsprototype);
		this._funs={};
		for(var k in this.respSize){
			this[k] = new this.cls(k, this);
			this._funs[k]={fin:[], fout:[]};
		}
	}
	,respSize:{
		'xs':{min:0,max:767}//手机
		,'sm':{min:768,max:0}//平板
		,'md':{min:992,max:0}//桌面
		,'lg':{min:1200,max:0}//桌面
	}
	,resize:function(){
		var w = $(window).width();
		for(var s in this.respSize){
			var rs = this.respSize[s], event = 'fout';
			if( (rs.min <= w || rs.min==0) &&
			 (w <= rs.max || rs.max==0)
			 ){
			 	event = 'fin';
			}
			var fs = this._funs[s][event];
			for(var i=0; i<fs.length; i++) fs[i].call(window);
		}
		return obj;
	}
	,cls:function(v, context){this.sn=v;this.context=context;}
	,clsprototype:{
		in:function(fun){this.context._funs[this.sn].fin.push(fun); this.context.resize();}
		,out:function(fun){this.context._funs[this.sn].fout.push(fun); this.context.resize();}
	}
}