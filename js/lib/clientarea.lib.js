/*
*
*	clientarea Beta 3.0.0.1 可视窗口
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/

{
	initialize:function(){
		ET.loadLib('element');
		$.extend(this.newCI.prototype, this.proto);
	}
	// 在当前显示控件内/显示时调用
	,inside:function(el, fun){return this.listen(el, function(ioo){ ioo && fun && fun.call(el); }); }
	// 在当前显示控件外/隐藏时调用
	,outside:function(el, fun){return this.listen(el, function(ioo){ !ioo && fun && fun.call(el); }); }
	,listen : function(el, fun){
		if(!(el instanceof HTMLElement))return ;
 		if(el.style.display == 'none'){ return ;};
		if(!fun)return ;
		el = $(el)[0];
		var _me=this, o = el.clientareaInstance;
		o = o?o:new this.newCI(el, fun);
		o.initialize();
		o.callback(window);
	}
	,newCI : function(el, fun){el.cicb=fun;this.el=ET.element.new(el);}
	,proto : {
 		 initialize : function(){
			if(!this.el)return null;
			this.scroll($(this.el).parent()[0]);
			var _me = this;
 			$(window).on('resize', function(){ _me.callback(window);});
		}
 		,callback : function(e){
 			var r = this.el.getRect();
 			var dr = ET.element.new(document).getRect();
 			if(r.top > 0 && r.top < dr.height && r.left > 0 && r.left < dr.width){
 				this.el.cicb(true);
 			}else{
 				this.el.cicb(false);
 			}
 		}
 		,scroll : function(el){
 			if(!(el instanceof HTMLElement)){ return; }
 			var _me = this;
 			$(el).on('scroll', function(){ _me.callback(el);});
 			this.scroll($(el).parent()[0]);
 		}
	}


}