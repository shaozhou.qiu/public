/*
*
*	dump Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		ET.loadLib('object');
		ET.loadLib('array');
		if(!window.dump){
			var _me = this;
			window.dump=function(){
				_me.fn.dump.apply(_me.fn, arguments);
			}
		}
	}
	,fn:{
		reanderHtml : function(obj, tab){
		    tab = tab?tab:'';
		    var type = 'string', prefix='', reval='<pre class="ET_DUMP" style="z-index:'+(ET.css.getMaxZIndex()+1)+'">';
		    if(obj instanceof Array){
		        reval+= '<span class="prefix" >Array:'+obj.length+' </span>[';
		        for(var k in obj){
		            var val = obj[k], prefix='';
		            if( typeof(val) == 'object'){
		                val = this.reanderHtml(val, tab+'    ');
		            }else{
		                prefix=typeof(val);
		            }
		            reval+= '\n'+ tab+'    <span class="keyword">' + k + '</span> => <span class="'+prefix+'" title="'+prefix+'">'+ val +'</span>,';
		        }
		        reval = reval.substr(0,reval.length-1);
		        reval+= '\n'+tab+']';
		    }else if(obj instanceof Object){
		        reval+= '<span class="prefix">Object# </span>{';
		        for(var k in obj){
		            var val = obj[k], prefix='';
		            if( typeof(val) == 'object'){
		                val = this.reanderHtml(val, tab+'    ');
		            }else{
		                prefix=typeof(val);
		            }
		            reval+= '\n'+ tab+'    <span class="keyword">' + k + '</span> => <span class="'+prefix+'" title="'+prefix+'" >'+ val +'</span>,';
		        }
		        reval = reval.substr(0,reval.length-1);
		        reval+= '\n'+tab+'}';
		    }else{
		        type = typeof(obj);
		        reval+= tab+'<span class="'+type+'" title="'+type+'" >'+type+':'+ obj +'</span>';
		    }
		    reval+='</pre>'
		    return reval;
		}
		,dump:function(obj){
			$('body').append(this.reanderHtml(obj))
		}
		,var_dump:function(){}
	}

}