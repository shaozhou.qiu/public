/*
*
*	json 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
	}
	/**
	 * 编码
	 * ssr转 base64
	 * @param  {[type]} data [description]
	 * @return {[type]}     [description]
	 */
	,encode:function(server, port, method, password, protocol, garble, remarks, group){
        port=port?port:9050;
        protocol = encodeURIComponent(protocol?protocol:'origin');
        garble = encodeURIComponent(garble?garble:'plain');
        method = encodeURIComponent(method?method:'aes-256-cfb');
        remarks = remarks?remarks:'';
        group=group?group:'EtSoftware';
        var ssr = encodeURIComponent(server) + ':' + port + ':'+protocol+':' + method + ':'+ garble +':' +  this.base64_encode(password) +
            '/?obfsparam=&remarks=' + this.base64_encode(remarks)+
            '&group=' + this.base64_encode(group);
        return 'ssr://' + this.base64_encode(ssr) ;
	}
	/**
	 * 解码
	 * 将base64 转为 ssr
	 * @param  {[type]} str 如ssr://xxxxxx
	 * @return {[type]}     [description]
	 */
	,decode:function(str){
        if(!str){return null;}
        if( false==/^[\s]*ssr:\/\/(\w+?)[\t\s=]*$/gi.test(str) ){return null; }
        var m = /^[\s]*ssr:\/\/(\w+?)[\t\s=]*$/gi.exec(str);
        if(!m){return null;}
        var url=this.base64_decode(m[1]);
        m = /^([^:]+):([^:]+):([^:]+):([^:]+):([^:]+):([^:]+)(\/\?.*)$/gi.exec(url);
        if(!m){ return null;}
        var op = {remarks:'', group:''};
        var mr = /[\&\?]?remarks=([^&]*).*/gi.exec(m[7]);
        if(mr){ op.remarks = this.base64_decode(mr[1]); }
        console.log(mr, op);
        var mr = /[\&\?]?group=([^&]*).*/gi.exec(m[7]);
        if(mr){ op.group = this.base64_decode(mr[1]); }
        if(!op){ op = [null, null, null];}
        var data = {
            server      : m[1]
            ,port       : m[2]
            ,method     : m[4]
            ,password   : this.base64_decode(m[6])
            ,protocol   : m[3]
            ,garble     : m[5]
            ,remarks    : op.remarks
            ,group      : op.group
        };		
        return data;
	}
    ,base64_decode:function(str){
        if(!str) return str;
        str = str.replace(/[\t\s]*(.*?)[\t\s=]*/gi, '$1');
        str = str.replace(/_/gi,'\/');
        str = str.replace(/[\-\–]/gi,'\+');
        str = decodeURIComponent(escape(atob( str )));
        // str = atob(str);
        return str;
    }    
	,base64_encode:function(str){
        if(!str) return str;
        // str = btoa(str);
        str = btoa(unescape(encodeURIComponent( str )));
        str = str.replace(/[\=\t\s]+$/gi,'');
        str = str.replace(/\//gi,'_');
        str = str.replace(/\+/gi,'–');
        return str;		
	}



}