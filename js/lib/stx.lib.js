/*
*
*	stx Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		ET.loadLib('code');
		this.lib={};
	}
	,load : function(type, callback){
		if(!type){return ;}
		type = type.toLowerCase();
		var _me = this;
		if(type == 'javascript'){ type ='js';}
		this.fopen(type, function(e, conf){
			if(this.statusText && this.statusText.toLowerCase() == 'load'
				|| (conf && conf.dataType == 'jsonp')
				){
				var re = ET.execJS(e);
				e = re.data;
			}
			var handle = {
				errcode : 0
				, data : e?e:''
				, index : 0
				, end : e.length
			};
			stx = _me.getConfig(handle);
			if(stx.number_pattern){ _me.getNumberPattern(stx); }
			$.extend(stx, _me.fn);
			_me.lib[type] = stx;
			callback && callback.call(stx);
		});
	}

	, fn :{
		/**
		 * 渲染
		 * @return {[type]} [description]
		 */
		rendering:function(code){			
			var html = {style:[]};
			code = code.replace(/</gi, '__HTMLLT__');
			code = code.replace(/>/gi, '__HTMLGT__');
			code = this.renderQuotation(code);
			code = this.renderComment(code);
			code = this.renderVariable(code);
			code = this.renderWords(code);
			code = this.renderColor(code, html);
			code = code.replace(/__HTMLLT__/gi, '&lt;');
			code = code.replace(/__HTMLGT__/gi, '&gt;');
			for(var k in html){
				code+="<"+k+">"
				html[k].forEach(function(v,k){ code+= v+"\n";});
				code+="</"+k+">"
			}
			// console.log(code);
			// console.log(this);
			return code;
		}
		/**
		 * 删除注释
		 * @param  {[type]} code [description]
		 * @return {[type]}      [description]
		 */
		, removeComments:function(code){
			var comm = null, quot = null;
			if(this.linecomment ){
				comm = comm?comm:[];
				comm.push(ET.stx.fillterRegExp(this.linecomment)+".*");
			}
			if(this.commenton &&  this.commentoff ){
				comm = comm?comm:[];
				var on = ET.stx.fillterRegExp(this.commenton);
				var off = ET.stx.fillterRegExp(this.commentoff);
				comm.push(on+"[\\W\\w]*?"+off);
			}
			[this.quotation1, this.quotation2].forEach(function(v, i){
				if(!v){return ;}
				quot = quot?quot:[];
				quot.push(v);
			});
			return ET.code._removeComments(code, comm, quot);
		}
		, renderVariable:function(code, html){
			$res = [];
			if(this.prefix3){ $res.push(ET.stx.fillterRegExp(this.prefix3)+'\\w+'); }
			return this._render(code, $res, "variable");
		}
		, renderColor:function(code, html){
			return this._render(code, ["\\#[\\da-zA-z]{2,8}", "rgb\\s*\\(\\s*\\d+\\s*,\\s*\\d+\\s*,\\s*\\d+\\s*\\)"], function(m){
				if(!m){return 'color ';}
				var clsName = "color-"+m[0].replace(/\W/gi,'');
				html.style.push( "."+clsName+":before{color:"+m[0]+"}" );
				return "color " + clsName;
			});
		}
		, renderQuotation:function(code){
			var restrs = [];
			[this.quotation1, this.quotation2].forEach(function(v, i){
				if(!v){return ;}
				var q = v.replace(/([\\\/\.\-\*])/gi, '\\$1');
				var restr = q+"[^"+q+"]*"+q;
				restrs.push(restr);

			});
			code = this._render(code, restrs, 'string');
			return code;
		}
		, renderComment:function(code){
			var restr = [];
			if(this.linecomment ){
				restr.push(this.linecomment.replace(/([\\\/\.\-\*])/gi, '\\$1')+".*");
			}
			if(this.commenton &&  this.commentoff ){
				var on = this.commenton.replace(/([\\\/\.\-\*])/gi, '\\$1');
				var off = this.commentoff.replace(/([\\\/\.\-\*])/gi, '\\$1');
				restr.push(on+"[\\W\\w]*?"+off);
			}
			code = this._render(code, restr, 'comment');
			return code;
		}
		, renderWords:function(code){
			
			for(var k in this.keyword){
				var res = this.keyword[k];
				var m = k.match(/[A-Z]/g);
				var cls = k.substr(0,3).toLowerCase();
				cls = k.toLowerCase();
				if(m){ m.forEach(function(v, i){ cls += v.toLowerCase();}); }
				if(res && res.length>0){
					code = this._render(code, res, cls);
				}
			}
			return code;
		}
		, _render:function(code, words, cls){
			var _me= this;
			if(ET.object.getType(words) != "array"){
				return this._render(code, [words], cls);
			}
			if(words.length == 0){return code;}
			var htmlTagRe = "\\<[^\\>]+\\>[\\W\\w]*?\\<[^\\>]+\\>";
			return ET.string.preclude(code, new RegExp(htmlTagRe, 'gi'), function(code){
				var restr = '(^|\\W|\\s)(';
				words.forEach(function(v,i){
					restr += '('+v+')|'; 
				});
				restr = restr.substr(0, restr.length-1);
				restr += ')(\\W|\\s|$)';
				var re = new RegExp(restr, 'gi');
				var mc = code.match(re);
				if(!mc){ return code;}
				for(var i=0, l=mc.length; i<l;  i++){
					code = ET.string.preclude(code, new RegExp(htmlTagRe, 'gi'), function(code){
						re = new RegExp(restr, 'gi');
						var m = re.exec(mc[i]), clsName=cls;
						if(!m){return code;}
						if(typeof cls == 'function'){ 
							clsName = cls(m); 
						}
						return code.replace(m[0], m[1]+'<span class="'+clsName+'">__stx_re_mc_'+i+'__</span>'+m[m.length-1]);
					});
				}
				for(var i=0, l=mc.length; i<l;  i++){
					code = ET.string.preclude(code, new RegExp("\\<[^\\>]*\\>", 'gi'), function(code){
						re = new RegExp(restr, 'gi');
						var m = re.exec(mc[i]);
						if(!m) return code;
						return code.replace('__stx_re_mc_'+i+'__', m[2]);
					});
				}
				return code;
			}, []);
		}
	}
	,fillterRegExp : function(str){
		return str.replace(/([\[\]\$\^\*\.\-\{\}\\\|])/gi, "\\$1");
	}
	,getConfig : function(hdl){
		var i=0, conf={keyword:{}}, lp=null;
		while(true){
			var msg = this.fgets(hdl);
			if(msg === null){break;}
			if(/(^\s*;)|(^\s*$)/gi.test(msg)){continue;}
			if(/^\s*#/gi.test(msg)){
				var m = /\s*(\w+)\s*=\s*(.*)\s*/gi.exec(msg);
				if(!m){continue;}
				var k = m[1];
				if(k.toLowerCase() == 'keyword'){
					lp = this.psr4Key(m[2]);
					conf.keyword[ lp ] = [];
				}else{
					conf[this.psr4Key(k)] = m[2];
				}
			}else if(lp){
				msg = this.fillterRegExp(msg); 
				conf.keyword[lp].push(msg.replace(/[\r\n]+/gi, ''));
			}
		}
		return conf;
	}
	,getNumberPattern:function(stx){
		var _me = this;
		this.fopen(stx.number_pattern, function(e){
			var handle = {
				errcode : 0
				, data : e?e:''
				, index : 0
				, end : e.length
			};
			var npConf = _me.getConfig(handle);
			for(var k in npConf.keyword){
				if(!stx.keyword[k]){stx.keyword[k] = npConf.keyword[k]; continue;}
				var ary = stx.keyword[k].concat(npConf.keyword[k]);
				stx.keyword[k] = [];
				ary.forEach(function(v,i){ (stx.keyword[k].indexOf(v)==-1) && stx.keyword[k].push(v); });
			}
		});
	}
	,psr4Key:function(val){
		var mc = val.match(/\w+/gi), key='';
		for(var i=0, l=mc.length; i<l; i++){
			var m = mc[i].toLowerCase();
			if(i == 0){key += m; continue;}
			key += m.replace(m[0], m[0].toUpperCase());
		}
		return key;
	}
	,fopen : function(ext, callback){
		var url = ET.jsPath+'/../stx/'+ext+'.stx';
		var ret = ET.getFile(url, callback, {'dataType':'text'});
		// if(ret.errcode != 0) { return ret;}
		// return {
		// 	errcode : 0
		// 	, data : ret.data
		// 	, index : 0
		// 	, end : ret.data.length
		// }
	}
	,fgets : function(hdl){
		if(!hdl)return null;
		if(hdl.index >= hdl.end){return null;}
		var tmp = hdl.data.substr(hdl.index);
		var index =  tmp.indexOf('\n')+1;
		hdl.index +=  index;
		if(index == 0){return null;}
		var val = tmp.substr(0, index);
		tmp = null;
		return val;

	}
	,fclose : function(hdl){
		if(!hdl)return null;
		hdl = null;
		return ;
	}

}