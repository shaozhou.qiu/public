/*
*
*	messagebox Beta 1.0.0.1 消息提示框
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.clientarea) ET.loadLib('clientarea');
		if(!ET.drag) ET.loadLib('drag');
		if(!ET.unit) ET.loadLib('unit');
		if(!ET.object) ET.loadLib('object');
		if(!ET.hotkey) ET.loadLib('hotkey');
		if(!ET.css) ET.loadLib('css');
	}
	,BUTTON_OK:1
	,BUTTON_NO:2
	,BUTTON_CANCEL:4
	,BUTTON_YES:8
	,BUTTON_CUSTOMIZE:16
	,throw:function(e){console.error(e); }
	/**
	 * 显示对话框
	 * @param  {[type]}   obj      显示文本或对象
	 * @param  {Function} callback 回调函数，默认为空可以是function(){} 或 {'onok':function(){} ...}
	 * @param  {[type]}   target   作用域对象
	 * @return {[type]}            返回为一个对象
	 */
	,show:function(obj, callback, target){
		var _me=this, msk = this.mask(target)
		,dialog = this.dialog(obj, '', {
			'onDestroy':function(){
				msk.destroy && msk.destroy(); 
				try{
					if(typeof(callback)=='function'){
						if(callback){ callback.call(this, null); }
					}else if(callback){
						if((this.buttons & _me.BUTTON_YES) && callback.onyes){ callback.onyes.call(this); }
						if((this.buttons & _me.BUTTON_OK) && callback.onok){ callback.onok.call(this); }
						if((this.buttons & _me.BUTTON_NO) && callback.onno){ callback.onno.call(this); }
						if((this.buttons & _me.BUTTON_CANCEL) && callback.oncancel){ callback.oncancel.call(this); }
						callback.onDestroy && callback.onDestroy.call(this);
					}
				}catch(e){_me.throw(e); }
			}
		}, target);
		msk.onresize=function(){
			var zIndex = parseInt($(msk).css('z-index')) + 1;
			$(dialog).css({'z-index': zIndex});
		}
		$(msk).after(dialog);
		// 热键
		ET.hotkey.addKey(ET.hotkey.Escape, null, function(){dialog.destroy(); });
		ET.hotkey.addKey(ET.hotkey.Tab, null, function(){
			if($(dialog).find(document.activeElement).length==0){
				$(dialog).find('a:first').focus();
			}
		});
		var o = {'destroy':function(){ dialog.destroy(); } };
		Object.defineProperty(o, 'body', {'set':function(v){ dialog.setContent(v); } });
		return o;
	}
	/**
	 * 对话框
	 * @param  {[type]} txt    显示文本或对象
	 * @param  {[type]} tit    显示标题
	 * @param  {[type]} events 响应事件
	 * @param  {[type]} target 目标
	 * @param  {[type]} buttons 热键组合 默认为BUTTON_OK
	 * @return {[type]}        [description]
	 */
	,dialog:function(txt, tit, events, target, buttons){
		target=ET.element.new(target?target:document.body);
		if( !ET.object.isElement(target) ){ target=document.body; }
		buttons=buttons?buttons:this.BUTTON_OK;
		var _me=this, div=ET.element.new(ET.ce('div', {'class':'ET_STYLE_MSGBOX', 'for':target.id?target.id:''}))
		,thead=ET.ce('div', {'class':'thead'})
		,tbody=ET.ce('div', {'class':'tbody'})
		,hspnClose=ET.ce('a', {'href':'javascript:void(0)','innerHTML':'×', 'style':'border:1px transparent;float: right; padding: '+ ET.unit.pxorem('0.01rem')+' '+ ET.unit.pxorem('0.03rem')})
		,hContent=ET.ce('div', {'class':'content'})
		,hTools=ET.ce('div', {'class':'tools'})
		,rect=target.getRect()
		,hbtn=[]
		,css={
			'min-width': ET.unit.pxorem( '100px')
			,'top': 0 ,'left': 0
			, 'visibility': 'hidden'
			,'min-height': ET.unit.pxorem( '10px')
			,'max-width': ET.unit.pxorem( (rect.clientWidth*0.8)+'px')
			,'max-height': ET.unit.pxorem( (rect.clientHeight/8*7)+'px')
		};
		if(target==document.body){
			ET.extend(css, {'position':'fixed'});
			// ET.extend(css, {'top':'50%','left': '50%'});
		}else{
			ET.extend(css, {'position':'absolute'});			
		}
		//生成热键
		if( buttons & this.BUTTON_YES){
			var btn = ET.ce('a', {'href':'javascript:void(0)','innerHTML':'Yes'});
			$(btn).click(function(e){ div.yes(); });
			hbtn.push(btn);
		}
		if( buttons & this.BUTTON_OK){
			var btn = ET.ce('a', {'href':'javascript:void(0)','innerHTML':'Ok'});
			$(btn).click(function(e){ div.ok(); });
			hbtn.push(btn);
		}
		if( buttons & this.BUTTON_NO){
			var btn = ET.ce('a', {'href':'javascript:void(0)','innerHTML':'No'});
			$(btn).click(function(e){ div.no(); });
			hbtn.push(btn);
		}
		if( buttons & this.BUTTON_CANCEL){
			var btn = ET.ce('a', {'href':'javascript:void(0)','innerHTML':'Cancel'});
			$(btn).click(function(e){ div.cancel(); });
			hbtn.push(btn);
		}
		$(thead).append([tit?tit:'@Etsoftware', '&nbsp;', hspnClose]);
		$(hTools).append( hbtn );
		$(tbody).append([hContent, hTools]);
		$(div).append([thead, tbody]);
		$(div).css(css).hide();
		['Yes', 'Ok', 'Load', 'No', 'Cancel', 'Destroy'].map(function(v,i){
			var e={}; e['on'+v]=function(){};
			ET.extend(div, e);
		});
		ET.extend(div, events);
		var evb = {'Yes':this.BUTTON_YES, 'Ok':this.BUTTON_OK, 'No':this.BUTTON_NO, 'Cancel':this.BUTTON_CANCEL};
		for(let k in evb){
			var e={}; e[k.toLowerCase()]=function(){
				this.buttons=evb[k];
				try{
					this['on'+k] && this['on'+k]();
				}catch(e){
					_me.throw(e); 
				} 
				this.destroy();
			};
			ET.extend(div, e);
		}
		div.destroy=function(){try{this.onDestroy && this.onDestroy(); }catch(e){_me.throw(e); } $(div).remove(); }
		div.loaded=function(){ 
			$(hContent).find('[et]').each(function(){ET(this)});
			$(hTools).find('>:first').focus(); 
			try{this.onLoad && this.onLoad();}catch(e){_me.throw(e); }
		}
		div.completed=function(e){}
		div.setContent=function(e){
			var _me = this; txt=e;
			var ifm = ET.ce('iframe', {
				style:'visibility: hidden; width: 0px; height: 0px;'
			});
			ifm.onload=function(){ _me.completed.call(_me); }
			$(hContent).html(txt); 
			$(hContent).append(ifm);
		}

		div.resize=function(){
			function getHeight(ele){
				var h = $(ele).height();
				h += $(ele).css('margin-top').replace(/[^\.\d]+/gi, '')*1;
				h += $(ele).css('margin-bottom').replace(/[^\.\d]+/gi, '')*1;
				return h;
			}
			var cHeight = $(hContent).height()
			, height = $(this).height()
			, thHeight = getHeight(thead)
			, tbdyHeight = height - thHeight
			, toolHeight = getHeight(hTools)
			;
			if(  cHeight < tbdyHeight ){ return ; }
			$(hContent).css({'height': (tbdyHeight-toolHeight)+'px', 'overflow': 'overlay'});
		}
		div.move=function(x, y){
			var r = this.getRect();
			var w = $(window).width()>rect.clientWidth?rect.clientWidth:$(window).width();
			var h = $(window).height()>rect.clientHeight?rect.clientHeight:$(window).height();
			x=x?x:w/2 - r.clientWidth/2;
			y=y?y:h/2 - r.clientHeight/2;
			$(this).css({'top':y, 'left':x});
		}
		$(hContent).bind('DOMNodeInserted', function(e) {
			clearTimeout(this.tid);
			this.tid = setTimeout(function(){ div.resize(); }, 500);
        });
		div.onappend(function(){setTimeout(function(){
			div.move(); 
			div.resize();
			$(div).css('visibility', 'visible');
		}, 1);$(div).show(); div.loaded(); });
		$(hspnClose).click(function(event) {div.cancel(); });
		ET.drag.element(thead, div);
		$(target).after(div);
		div.setContent(txt);
		return div;
	}
	/**
	 * 屏蔽层
	 * @param  {[type]} target  被屏蔽对象
	 * @param  {[type]} info    显示信息
	 * @param  {[type]} mskevent响应事件
	 * @return {[type]}         [description]
	 */
	,mask:function(target, info, mskevent){
		target=ET.element.new(target?target:document.documentElement);
		if( !ET.object.isElement(target) ){ target=document.documentElement; }
		info=info?info:mskevent?'Loading...':'';
		if(!target || !ET.object.isElement(target) ){return null;}
		var zIndex = ET.css.getMaxZIndex();
		var div=ET.ce('div', {'class':'ET_MSG_MASK', 'for':target.id?target.id:''});
		var hdivInfo=ET.ce('div', {'class':'ET_MSG_MASK_INFO', 'innerHTML':info});		
		$(div).css({'z-index': zIndex}).hide();
		$(hdivInfo).css({'z-index': zIndex+1});
		if(!info){ $(hdivInfo).hide();}
		// 触发事件
		var events={
			// 改变大小时触发
			onresize:function(){}
			// 加载完成后触发
			,onload:function(){}
			// 销毁后触发
			,ondestroy:function(){}
			// 显示时触发
			,onshow:function(){}
		};
		ET.extend(div, events, mskevent);
		div.onappend(function(){
				var divcss={};
				if(target==document.documentElement){
					divcss={'position': 'fixed'};
				}else{
					divcss={'position': 'absolute'
						,'margin-top': '-'+ET.unit.pxorem($(target).css('padding-top'))
						,'margin-left': '-'+ET.unit.pxorem($(target).css('padding-left'))
					};
				}
				$(div).css(divcss).show();
				if(div.onload){setTimeout(function(){ div.onload(); },1);}
				try{div.onresize && div.onresize(); }catch(e){ }
			}
		);
		//销毁 
		div.destroy=function(){ try{this.ondestroy && this.ondestroy();}catch(e){_me.throw(e); } $(this).remove();$(hdivInfo).remove(); };
		div.show=function(){ 
			try{this.onshow && this.onshow();}catch(e){_me.throw(e); } 
			$(target).prepend([div,hdivInfo]); 
			var zIndex = ET.css.getMaxZIndex();
				$(div).css({'z-index': zIndex});
				$(hdivInfo).css({'z-index': zIndex+1});
			this.resize(); 
		};
		div.resize=function(){ 
			var rect = target.getRect();
			$(this).css({
				'width': ET.unit.pxorem(rect.clientWidth+'px')
				,'height': ET.unit.pxorem(rect.clientHeight+'px')
			});
			try{this.onresize && this.onresize(); }catch(e){_me.throw(e); }
		};
		div.show();
		// 监听事件
		$(window).resize(function(event) { div.resize();});
		
		return div;
	}
}