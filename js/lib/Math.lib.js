/*
*
*	Math Beta 1.0.0.0 数学函数
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		// if(!ET.object) ET.loadLib('object');
	}
	/**
	 * 解析公式得出结果
	 * @param  {[type]} str [description]
	 * @return {[type]}     [description]
	 */
	,formulas:function(str){
		var re = '\\([^\\(\\)]*\\)';
		var mc = str.match( re );
		if(mc){
			for(var i = 0, l = mc.length; i<l; i++){
				var s = mc[i] , v = s
				, m = /^\((.*)\)$/gi.exec( s );
				if(m){
					v = this.formulas( m[1] );
					str = str.replace(s, v);
				}
			}
			return this.formulas( str );
		}
		if(str==''){ return 0; }
		return this.formula( str );

	}
	/**
	 * 解析公式得出结果， 如果传入4+6 返回结果为10
	 * @return {[type]} [description]
	 */
	,formula:function(str){
		str = str.replace(/×/gi, '*');
		str = str.replace(/÷/gi, '/');
		var re = /^[\d\.]+((\s*[\+\-\*\/\%]\s*[\d\.]+\s*)){1,}$/gi
		, m = re.exec(str)
		;
		if(!m){return null;}
		return ET.execJS('return '+str);
	}
	/**
	 * 反math.pow函数
	 * @param  {[type]} 参数
	 * @return {[type]}     [description]
	 */
	,unpow:function(){
		var arg=arguments, e=2, n=0;
		if(arg.length==0){ return NaN;}
		if(arg.length>1){ e=arg[0]; n=arg[1]; }else{ n=arg[0];}
		return Math.log(n)/Math.log(e);
	}
	// 异或 运算 x^x
	,xor:function(){
		var arg=arguments;
		if(arg.length<2 ){return;}
		var ret = arg[0];
		for(var i=1;i<arg.length; i++){
			ret ^= arg[i];
		}
		return ret;
	}
	/**
	 * 求和
	 * @return {[type]} [description]
	 */
	,sum:function(){
		var args = arguments, ret=0;
		if(args.length == 1 && ET.object.getType(args[0]) == "array"){
			this.sum.apply(this, args);
		}
		for(var i=0, l=args.length; i<l; i++){
			ret += args[i];
		}
		return ret;
	}
	/**
	 * 求差
	 * @return {[type]} [description]
	 */
	,diff:function(){
		var args = arguments, ret=args[0];
		if(args.length == 1 && ET.object.getType(args[0]) == "array"){
			this.diff.apply(this, args);
		}
		for(var i=1, l=args.length; i<l; i++){
			ret -= args[i];
		}
		return ret;
	}
	
}