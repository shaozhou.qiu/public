/*
*
*	contextmenu Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.event) ET.loadLib('event');
		if(!ET.element) ET.loadLib('element');
		if(!ET.scroll) ET.loadLib('scroll');
	}
	,create:function(e, el){
		e = e?e:null;
		el = el?el:document;
		el = (el==document || el==window)?document.body:el;
		var _me=this, div = ET.ce('div', {'class': 'ET_STYLE_CONTEXTMENU'})
		, ul = ET.ce('ul', {'style':'margin:0;'})
		// , mp = e?ET.event.getPos(e):{x:0, y:0};
		, mp = e?ET.event.getMousePos(e):{x:0, y:0}
		, scroll = ET.scroll.postion() ;
		mp.y -= scroll.top;
		mp.x -= scroll.left;
		$(div).append( ul ).css({
			'left' : mp.x
			,'top' : mp.y
			,'padding' : 0
			,'margin' : 0
			,'width' : 'auto'
			,'z-index' : document.all.length+999
		});
		if( typeof(el)== "function" ){
			el = el.call(div, div);
		}
		div.context = el;
		ET.extend(div, this.fn);
		$(div).blur(function(){
			this.destroy();
		}).css('z-index', this.getMaxzIndex()+1).focus();
		
		$(el).after(div);
		$(document).click(function(){
			div.destroy();
		});
		return div;
	}
	/**
	 * 获取页面中最大的zindex
	 * 
	 * @return {[type]} [description]
	 */
	,getMaxzIndex : function(){
		// 通过数组 Symbol.iterator 进行处理
		var ales = document.all, data=[0];
		for(var i=0, l = ales.length; i<l; i++){ 
			var e = window.getComputedStyle(ales[i]);
			data.push(e.zIndex || 0);
		}
		return Math.max.apply(data);
	}
	,fn:{
		show:function(){ $(this).show(); return this;}
		,addLine:function(){ return this.add('<hr>'); }
		,addSubMenu:function(title, hotkey){
			this.add(title+'&nbsp;&nbsp;>', hotkey, function(){

			});
			var li = $(this).find('>ul>li:last');
			var menu = ET.contextmenu.create(null, function(div){
				$(li).append(div);
			});
			var t = $(li).offset().top
			,l = $(li).offset().left + $(this).width()
			;
			var h = 0, pel = $(li).prev();
			while(pel.length){
				h += pel[0].clientHeight;
				h += pel.css('margin-top').replace(/[^\d]+/gi, '')*1;
				h += pel.css('margin-bottom').replace(/[^\d]+/gi, '')*1;
				pel = $(pel).prev();
			}
			t = this.style.top.replace(/[^\d]+/gi, '')*1 + h;
			$(menu).css({
				'top' : t+'px'
				,'left' : l+'px'
			});
			$(menu).hide();
			$(li).mousemove(function(event) {
				$(menu).show();
			}).mouseout(function(event) {
				$(menu).hide();
			});
			return menu;
		}

		/**
		 * 添加菜单
		 * @param {[type]} title     菜单名
		 * @param {[type]} hotkey 热键
		 */
		,add:function(title, hotkey, fun){ 
			var _me = this, li = ET.ce('li');
			if(hotkey){
				$(li).append('<span>(<u>'+hotkey+'</u>)</span>');
			}
			$(li).click(function(){ 
				_me.destroy(); 
				if(fun){
					var context = _me.context?_me.context:this;
					fun.call(context, this, $(this).index());
				}
			});
			$(li).append(title);
			$(this).find('>ul').append( li );
			return this;
		}
		,destroy:function(){ $(this).remove(); return this;}
	}
}