/*
*
*	Rsa Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		var url = ET.jsPath+'/jsencrypt.min.js';
		$.ajax({url:url, success:function(e){
			
		}});
	}
	,setPrivkey : function($data){}
	,setPubkey : function($data){}
	,decryptByPrivate : function($data, $key=null, $encoding='base64'){
		if(typeof(JSEncrypt)!= "function")return ;
		var rsa = new JSEncrypt();
		rsa.rsa.setPrivateKey($key);
		return rsa.decrypt($data);
	}
	,encryptByPrivate : function($data, $key=null, $encoding='base64'){
		if(typeof(JSEncrypt)!= "function")return ;
		var rsa = new JSEncrypt();
		rsa.rsa.setPrivateKey($key);
		return rsa.encrypt($data);
	}
	,encryptByPublic : function($data, $key=null, $encoding='base64'){
		if(typeof(JSEncrypt)!= "function")return ;
		var rsa = new JSEncrypt();
		rsa.setPublicKey($key);
		return rsa.encrypt($data);
	}
	,decryptByPublic : function($data, $key=null, $encoding='base64'){
		if(typeof(JSEncrypt)!= "function")return ;
		var rsa = new JSEncrypt();
		rsa.rsa.setPublicKey($key);
		return rsa.decrypt($data);
	}
	
};