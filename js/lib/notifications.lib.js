/*
*	桌面通知
*	notifications Beta 1.0.0.1 
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.string) ET.loadLib('string');
		if(window.Notification){
			// $.extend(this, this.wNotification);
		}else{

		}
	}
	/**
	 * 创建桌面提示
	 * @param  {[type]} title   显示标题
	 * @param  {[type]} options 选项
	 *                          dir：文字方向，可能的值为auto、ltr（从左到右）和rtl（从右到左），一般是继承浏览器的设置。
	 *                          lang：使用的语种，比如en-US、zh-CN。
	 *                          body：通知内容，格式为字符串，用来进一步说明通知的目的。。
	 *                          tag：通知的ID，格式为字符串。一组相同tag的通知，不会同时显示，只会在用户关闭前一个通知后，在原位置显示。
	 *                          icon：图表的URL，用来显示在通知上。
	 * @return {[type]}         [description]
	 */
	,create : function(title, options){
		var div = ET.ce('div', {class:'ET_NOTIFICATIONS'});
		$('body').append(div);
	}

	,wNotification:{
		initialize: function(){
			this.requestPermission();
		}
		,create : function(title, options){
			var notification = new Notification(title, options);
			//通知显示给用户时触发
			options.onshow && (notification.onshow=options.onshow);
			//用户点击通知时触发
			options.onclick && (notification.onclick = options.onclick);
			//用户关闭通知时触发
			options.onclose && (notification.onclose = options.onclose);
			//通知出错时触发
			options.onerror && (notification.onerror = options.onerror);
		}
		,requestPermission : function(){
			if(window.Notification && Notification.permission !== "denied") {
			    Notification.requestPermission(function(status) {
					// default：用户还没有做出任何许可，因此不会弹出通知。
					// granted：用户明确同意接收通知。
					// denied：用户明确拒绝接收通知。
			        if(status == 'default'){

			        }
			    });
			}			
		}
	}
}