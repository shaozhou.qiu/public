{
	initialize:function(){
	}
	,loadData:function(data){
		if(!data)return this;
		this.data = data;
	}
	,load:function(areaCode, async){
		var _me = this;
		if(!areaCode) areaCode = "cn";
		var url = ET.jsPath +"/dat/CarLicenseNumber."+areaCode+".dat.js" ;
		$.ajax({
			url:url
			,async:async?true:false
			,dataType:"json"
			,success:function(e){_me.loadData(e); }

		});
	}
	,getData : function(id, pid){
		if(!this.data)return null;
		for(var i=0; i<this.data.length; i++){
			if(this.data[i].id==id){
				if(pid && this.data[i].pid!=pid )continue;
				return this.data[i];
			}
		}
		return null;
	}
	,getInfo : function(number){
		if(!number) return null;
		if(!this.data) this.load(null, false);
		var m = /\s*(.)\s*([a-zA-Z])\s*([a-zA-Z\d]+)/gi.exec(number);
		if(!m)return null;
		var province = this.getData(m[1]);
		if(!province)return null;
		var city = this.getData(m[2], province.id);
		return city?{
			province : province.title
			, city : city.title
			, number : number
		}:null;
	}
}