/*
*
*	object Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.array) ET.loadLib('array');
		if(!ET.json) ET.loadLib('json');
		if(!ET.string) ET.loadLib('string');
	}
    /**
     * 监听对象中的值是否被操作，如获取，修改
     * @param  {[type]} obj  {key:'', ....}
     * @param  {[type]} desc {get:function(){}, set:function(){}}
     * @return {[type]}      [description]
     */
    ,listen: function(obj, desc){
        desc = desc?desc:{};
        for(var k in obj){
            var tkey = '__'+k;
            Object.defineProperty(obj, tkey, {
                configurable:false, value: obj[k],
                writable:true, enumerable:false
            });
            new function(){
                var _tkey = tkey;
                var _key = k;
                Object.defineProperty(obj, _key, {
                    get : function(){
                        desc.get && (desc.get(_key, this[_tkey]));
                        return this[_tkey];
                    },
                    set : function(val){
                        this[_tkey] = val;
                        desc.set && (desc.set(_key, this[_tkey]));
                    },
                });
            }();
            if( ET.object.getType(obj[k]) == 'object' ){
                this.listen(obj[k], desc);
            }else if( ET.object.getType(obj[k]) == "array" ){
                var aryDesc = {};
                ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse'].forEach(function(itm){
                    aryDesc[itm]= function(k, v){
                    	if(desc[k]){ desc[k](k, v); } 
                    	desc.set && (desc.set(k, v)); 
                    }
                });
                ET.array.listen(obj[k], aryDesc);
            }
        }
    }
    /**
     * 在一个对象上定义一个新属性，或者修改一个已经存在的属性
     * @param  {[type]} obj  需要定义属性的当前对象
     * @param  {[type]} prop 当前需要定义的属性名
     * @param  {[type]} desc 属性描述符
     * {
     *     configurable:false, // 是否可以改变描述符, false时不允许删除， true时可以被删除
     *     enumerable:false, // 是支持枚举
     *     value:undefined, // 任何有效的Javascript值(数值，对象，函数)
     *     writable:true, // 是否允许修改
     *     get:function(){}, // 一个给属性提供getter的方法
     *     set:function(value){}, // 一个给属性提供setter的方法
     * }
     * @return {[type]}      [description]
     */
    ,defineProperty : function(obj, prop, desc){
        return Object.defineProperty(obj, prop, desc);
    }
    /**
     * 在一个对象上定义新的属性或修改现有属性，并返回该对象
     * @param  {[type]} obj   [description]
     * @param  {[type]} props [description]
     * @return {[type]}       [description]
     */
    ,defineProperties : function(obj, props){
        return Object.defineProperties(obj, props);
    }
	/**
	 * 比较json对象是否相等
	 * @return {[type]} [description]
	 */
	, compare:function(){
        var ret = 0, args = arguments;
        if(args.length<2){ return -1; } //传入数量太少
        var a = args[0], al = 0;
        if(args.length>2){ //传入数量太多
            for(var i = 1, l = args.length; i<l; i++){
                ret = compare(a, args[i]);
                if(ret != 0){ break ; }
            }
            return ret;
        }
        var b = args[1], bl = 0;
        for(var k in a){al++;}
        for(var k in b){bl++;}
        if(al!=bl){ return -2; } // 数量不一至

        for(var k in a){
            if( !b.hasOwnProperty(k) ){ return -3; } // 键不存在
            var akt = ET.object.getType(a[k]);
            if( akt != ET.object.getType(b[k]) ){ return -4;  } // 数据类型不一至
            if( akt == 'object' ){
                ret = this.compare(a[k], b[k]);
            }else if( akt == 'array' ){
                ret = ET.array.compare(a[k], b[k]);
            }else{
            	if( a[k] != b[k] ){ return -5;  } // 数据不一至 
            }
            if(ret != 0){ return ret; }
        }
        return ret;
    }	
	,toString:function(obj){
		var t = this.getType(obj);
		if( t=='object' ){
			return ET.json.serialize(obj);
		}else if( t=='array' ){
			return ET.array.serialize(obj);
		}else{
			return obj;
		}
	}
	,getType:function(obj){
		var ret=Object.prototype.toString.call(obj)
		,m=/\[([^\s\t]+)[\s\t]*([^\s\t]+)\]/gi.exec(ret)
		,type = m?m[2].toLowerCase():''
		,tn = (obj && obj.tagName)?obj.tagName.toLowerCase():null;
		if(type=='object'){
			if(tn){
				type='html'+tn+'element';
			}else if(obj===null){
				type='null';
			}
		}		
		return type;
	}
    , clone : function(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        // Handle Date
        if (obj instanceof Date) {
            var copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }
        // Handle Array or Object
        if (obj instanceof Array | obj instanceof Object) {
            var copy = (obj instanceof Array)?[]:{};
            for (var attr in obj) {
            if (obj.hasOwnProperty(attr))
            copy[attr] = this.clone(obj[attr]);
            }
            return copy;
        }
        throw new Error("Unable to clone obj! Its type isn't supported.");
    }    
	/**
	 * 判断对象是否为 HTMLELEMENT 元素
	 * @param  {[type]}  obj 传入要认证的对象
	 * @return {Boolean}        如果是Element对象返回为true, 不是返回为false
	 */
	,isElement:function(obj){ return (ET.object.getType(obj).indexOf('element')>-1)?true:false; }
	,isInputElement:function(obj){ return ET.object.getType(obj).indexOf('htmlinputelement')>-1?true:false; }
	/**
	 * 判断是否为日期对象
	 * @param  {[type]}  obj 传入要认证的对象
	 * @return {Boolean}        如果是日期对象返回为true, 不是返回为false
	 */
	,isDate:function(obj){ return ET.object.getType(obj)==='date' ; }
	/**
	 * 判断是否为 数字类型
	 * @param  {[type]}  obj 传入要认证的对象
	 * @return {Boolean}        如果是返回为true, 不是返回为false
	 */
	,isNumber:function(obj){ return ET.object.getType(obj)==='number' ; }
	/**
	 * 判断是否为 布尔类型
	 * @param  {[type]}  obj 传入要认证的对象
	 * @return {Boolean}        如果是返回为true, 不是返回为false
	 */
	,isBoolean:function(obj){ return ET.object.getType(obj)==='boolean' ; }
	/**
	 * 将object序列化
	 * @param  {[type]} Obj    序列化对象
	 * @param  {[type]} pre    前标签 如数组为'[' json为'{'， 默认系统自动识别
	 * @param  {[type]} sub    后标签 如数组为']' json为'}'， 默认系统自动识别
	 * @param  {[type]} itmtpl 模板， 默认系统自动识别
	 * @param  {[type]} tab    自动缩进， 默认系统自动缩进
	 * @return {[type]}        返回为个已经序列后的字符串
	 */
	,serialize:function(Obj, pre, sub, itmtpl, tab){
		var reVal = "", key='{{key}}', value='{{value}}';
		if(!itmtpl){ itmtpl='"'+key+'":'+value+''; }
		tab = tab?tab:'\t';
		if(typeof(Obj)!='object')return Obj;
		if(pre===undefined && sub===undefined){
			if( this.getType(Obj) =="array" ){ pre='['; sub=']'; }else{pre='{'; sub='}'; }
		}
		for(var k in Obj){
			var val = Obj[k];	
			if( val instanceof Array ){val = ET.array.serialize(val, tab+'\t'); }
			else if( typeof(val)=='object' && val === null ){ val= 'null'; }
			else if( typeof(val)=='object' ){ val = ET.json.serialize(val, tab+'\t'); }
			else if( typeof(val)=='string' ){ 
				val=val.replace(/([\"\'\\])/gi, "\\$1").replace(/\n/gi,"\\n").replace(/\r/gi,"\\r");
				val = "\""+val+"\"";
			}
			else if( typeof(val)=='number'){val*=1;}
			else if( typeof(val)=='boolean'){ val =val?'true':'false';}
			else if( typeof(val)=='function' ){val = val.toString(); }
			else{ val=val?val:'""';}
			try{				
				var data={};
				data[key]=k;  data[value]=val;
				reVal += ",\n"+ tab + ET.string.format(itmtpl, data);
			}catch(e){throw e; }
		}
		reVal += "\n"+ tab.substring(1) + sub;
		reVal = pre + ( (reVal.length>1)?reVal.substring(1):reVal );
		var re = new RegExp("\\"+pre+"[^\\"+pre+"\\"+sub+"\\,]+\\"+sub, "gi")
		,mc = reVal.match(re);
		if(mc){
			for(var i=0; i<mc.length;i++){
				var nstr = mc[i].replace(/[\r\n\t]+/gi,'');
				reVal = reVal.replace(mc[i], nstr);
			}
		}
		return reVal;
	}
	/**
	 * 反序列化为oject
	 * 从已存储的表示中创建 
	 * @param  {[type]} str [description]
	 * @return {[type]}     [description]
	 */
	,unserialize:function(str){
        str = str.replace(/^[\r\n\s\t]*(.*?)[\r\n\s\t]*$/gi, '$1');
        if(! /^\{[\w\W]*\}$/gi.test(str)){
            return null;
        }
        return ET.execJS(str);
	}
    /**
     * 数据映射
     * @param  {[json]} data   源数据
     * @param  {[type]} ruler 映射规则如 {'序号':'id'}
     * @return {[type]} [description]
     */ 
    ,map : function(data, ruler){
        if(!ruler) return null;
        var reVal = {};
        for(var k in ruler){
            var v = ruler[k];
            reVal[k] = data[v]!=undefined?data[v]:'';
        }
        return reVal;
    }
    /**
     * 数据过虑
     * @param  {[json]} data   源数据
     * @param  {[type]} ruler 映射规则如 ['id']
     * @return {[type]} [description]
     */ 
    ,filter : function(data, ruler){
        if(!ruler) return null;
        var reVal = {};
        for(var i=0, l=ruler.length; i<l; i++){
            var key = ruler[i];
            reVal[key] = (data[key]!=undefined)?data[key]:'';
        }
        return reVal;
    }

}