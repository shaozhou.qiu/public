/*
*
*	image Beta 1.0.0.1  IP地址处理库
*	Release date: 2019-6-28
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		
	}
	,getSize : function(url){
		var data=null,url = ET.appPath+'/EtSoftWare.php?m=Images&act=getsize&url='+
				encodeURIComponent(url);
		$.ajax({
			url:url , async:false
			, dataType:'json'
			, success : function(e){data = e;}
		});
		return data;

	}
	,jigsaw : function(url, r, c, w, h){
		if(!w && !h){
			var ret = this.getSize(url);
			if(!ret){return ;}
			w = ret.width;
			h = ret.height;
		}
		var rct = 100;
		r=r?r:parseInt(h/rct); c=c?c:parseInt(w/rct);
		var _me=this, el = ET.ce('canvas', {'width':w, 'height':h})
		, render=function(x,y){
			var ctx=el.getContext("2d");
			ctx.drawImage(this, x, y);
		};
		el.onappend(function(){
			var ws = parseInt(w/c);
			var hs = parseInt(h/r);
			var x = 0, y=0;
			for(var i=0; i<=c; i++){
				x = ws*i;
				for(var j=0; j<=r; j++){
					y = hs*j;
					(function(lib, url, ws, hs, x, y, w,h, cb){
						lib.thumbnail(url, ws, hs, x, y, w,h, function(e){
							lib.preload(e, function(e){ if(e==200)cb.call(this, x, y); });
						})
					})(_me, url, ws, hs, x, y, ws,hs, render);
				}
			}
		});
		return el;
	}
	,thumbnail : function(url, w, h, sx, sy, sw, sh, callback){
		var ret = ET.appPath+'/EtSoftWare.php?m=Images&act=thumbnail&url='+
				encodeURIComponent(url) + 
				'&w='+w+'&h='+h+
				'&sx='+(sx?sx:0)+'&sy='+(sy?sy:0)+
				'&sw='+(sw?sw:0)+'&sh='+(sh?sh:0);
		if(callback)ret = callback(ret);
		return ret;
	}
	, preload : function(url, callback){
		var img = new Image();
		img.addEventListener("load",function(){callback.call(this, 200, 'ok'); $(this).remove(); });
		img.onerror = function(){
			callback.call(this, 404, 'error');
			$(this).remove();
		};
		img.src = url;
		$(img).css({"opacity": 0, "width": "0", "height": 0});
		return img;
	}
	
}