/*
*
*	json 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.object) ET.loadLib('object');
	}
	/**
	 * 将object转换成字符串
	 * @param  {[type]} obj [description]
	 * @return {[type]}     [description]
	 */
	,serialize:function(obj, tab){
		if(typeof(obj)!='object')return obj;
		if( obj instanceof Object ){
			return ET.object.serialize(obj, '{', '}', '"{{key}}":{{value}}', tab);
		}
		return null;
	}
	/**
	 * 合并json
	 * @return {[type]} [description]
	 */
	,merger:function(){
		var args = arguments;
		if(args.length==1)return args[0];
		if(args.length==2){
			var d1 = args[0], d2 = args[1];
			if(typeof(d1)!='object' || typeof(d2)!='object')return null;
			var d=ET.object.clone(d1);
			for(var k in d2){
				if(d[k]==undefined){
					d[k] = ET.object.clone(d2[k]);
					continue;
				}
				if(typeof(d[k])=='object'){
					if(typeof(d2[k])=='object'){
						d[k] = this.merger(d[k], d2[k]);
						continue;
					}else{
						d[k] = d2[k];
						continue;
					}
				}else{
					d[k] = d2[k];
				}
			}
			return d; 
		}
		if(args.length>2){
			var d = args[0];
			for(var i=1; i<args.length; i++){
				d = this.merger(d, args[i]);
			}
			return d;
		}
		return null;
	}
}