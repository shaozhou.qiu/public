/*
*
*	array Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.object) ET.loadLib('object');
		if (!Array.prototype.indexOf){
			Array.prototype.indexOf=this.fn.indexOf;
		}
	}
    ,listen : function(obj, desc){
    	if( ET.object.getType(obj) == 'object' ){
    		return ET.object.listen(obj, {
    			set:function(){
    				['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse'].forEach(function(itm){
    					desc[itm] && (desc[itm]());
    				});
    			}
    		});
    	}else if( ET.object.getType(obj) == 'array' ){
    		this.defineProperty(obj, desc);		
    	}
    }
    ,defineProperty : function(obj, desc){
		var _me=this, arrayProto = Array.prototype
		, arrayMethods = Object.create(arrayProto)
		, aryFun = ['push', 'pop', 'shift', 'unshift', 'splice', 'sort', 'reverse']
		;
		aryFun.forEach(function(itm){
			Object.defineProperty(arrayMethods, itm, {
				value : function(){
					//缓存原生方法，之后调用
					// console.log('array被访问'+itm);
					var original = arrayProto[itm]  
					, args = Array.from(arguments);
					original.apply(this, args);
					desc[itm] && (desc[itm].apply(desc, args));
				},
			})
		});
		obj.__proto__ = arrayMethods;
		obj.forEach(function(v, i, arry){
            if(ET.object.getType(v)=='object'){
            	var odesc={set:function(e){
            		aryFun.forEach(function(k){ desc[k] && desc[k](e); });
            		desc.set && desc.set (e);
            	}};
            	ET.object.listen(arry[i], odesc);
            } else if(ET.object.getType(v)=='array'){
            	_me.listen(arry[i], desc);
            }
        });
		return obj;
    }	
	/**
	 * 比较Array对象是否相等
	 * @return {[type]} [description]
	 */
	, compare:function(){
        var ret = 0, args = arguments;
        if(args.length<2){ return -1; } //传入数量太少
        var a = args[0];
        if(args.length>2){ //传入数量太多
            for(var i = 1, l = args.length; i<l; i++){
                ret = compare(a, args[i]);
                if(ret != 0){ break ; }
            }
            return ret;
        }
        var b = args[1];
        if(a.length != b.length){ return -2; } // 数量不一至
        for(var i=0, l=a.length; i<l; i++){
        	var ait = ET.object.getType(a[i]);
        	if( ait != ET.object.getType(b[i]) ){ return -4;  } // 数据类型不一至
        	if( ait == 'object' ){ 
        		ret = ET.object.compare(a[i], b[i]);
        	}else{
        		if( a[i] != b[i] ){ return -5;  } // 数据不一至 
        	} 
        	if(ret != 0){ return ret; }
        }
        return ret;
    }		
	,fn:{
		/**
		 * 方法可返回某个指定的字符串值在字符串中首次出现的位置。
		 * @param  {[type]} searchvalue 必需。规定需检索的字符串值。
		 * @param  {[type]} fromindex   可选的整数参数。规定在字符串中开始检索的位置。它的合法取值是 0 到 stringObject.length - 1。如省略该参数，则将从字符串的首字符开始检索
		 * @return {[type]}             [description]
		 */
		indexOf:function(searchvalue, fromindex){
			fromindex=fromindex?fromindex:0;
			var type = typeof(searchvalue);
			for (var i = 0; i <this.length; i++) {
				if( searchvalue==this[i]&& type== typeof(this[i]) ){return i; }
			}
			return -1;
		}
		
	}
	,sort:{
		/**
		 * 冒泡排序（每一趟找出最大的）
		 * 性能一般
		 * @param  {[type]} data 输入一个需要被排序的数组
		 * @param  {[type]} orderby  asc升序，desc 降序
		 * @param  {[type]} key  指定数组中的key
		 * @return {[type]}      [description]
		 */
		bubble:function(data, orderby, key){
			orderby = orderby?(orderby+'').toLowerCase():'asc';
			var temp, d1, d2;
			//冒泡排序,每一趟找出最大的,总共比较次数为arr.length-1次,每次的比较次数为arr.length-1次，依次递减
			for(var i = 0; i < data.length-1; i++){
			    for(var j = 0; j < data.length-1; j++){
		    		d1 = data[j];
		    		d2 = data[j+1];
			    	if( key ){ d1 = d1[key]; d2 = d2[key]; }
			    	var ret = (orderby=='asc')?d1 > d2:d1 < d2;
			        if(ret){
			            temp=data[j];
			            data[j]=data[j+1];
			            data[j+1]=temp;
			        }
			    }
			}
			return data;
		}
		/**
		 * 选择排序 性能一般
		 * @param  {[type]} data 输入一个需要被排序的数组
		 * @param  {[type]} orderby  asc升序，desc 降序
		 * @param  {[type]} key  指定数组中的key
		 * @return {[type]}      [description]
		 */
		,choose:function(data, orderby, key){
			orderby = orderby?(orderby+'').toLowerCase():'asc';
			var temp, d1, d2;
			for(var i=0;i<data.length-1;i++){
			    for(var j=i+1;j<data.length;j++){
			    	d1 = data[i]; d2 = data[j];
			    	if( key ){ d1 = d1[key]; d2 = d2[key]; }
			    	var ret = (orderby=='asc')?d1 > d2:d1 < d2;
			        if(ret){
			            temp=data[i];
			            data[i]=data[j];
			            data[j]=temp;
			        }
			    }
			}
			return data;
		}
		/**
		 * 快速排序
		 * @param  {[type]} data 输入一个需要被排序的数组
		 * @param  {[type]} orderby  asc升序，desc 降序
		 * @param  {[type]} key  指定数组中的key
		 * @return {[type]}      [description]
		 */
		,fast:function(data, orderby, key){
			orderby = orderby?(orderby+'').toLowerCase():'asc';
			if(data.length <= 1){return data; }
			var left = [], right = []
			, midIndex = parseInt(data.length / 2)
			, mid = data[midIndex]
			, d1 = key?mid[key]:mid
			, d2 = null;
			for(var i = 0 ; i < data.length ; i++){
				if(i == midIndex) continue;
				d2 = key?data[i][key]:data[i];
		    	var ret = (orderby=='asc')?d2 < d1:d2 > d1;
				if(ret){
					left.push(data[i]);
				}else{
					right.push(data[i]);
				}
			}
			return this.fast(left, orderby, key).concat([mid], this.fast(right, orderby, key)); 
		}
		/**
		 * 插入排序
		 * @param  {[type]} data 输入一个需要被排序的数组
		 * @param  {[type]} orderby  asc升序，desc 降序
		 * @param  {[type]} key  指定数组中的key
		 * @return {[type]}      [description]
		 */
		,insert:function(data, orderby, key){
			orderby = orderby?(orderby+'').toLowerCase():'asc';
			var n, n2, temp, d1, d2;
			for(var i=0;i<data.length;i++){
				n = i; n2 = n + 1;
				d1 = key?data[n][key]:data[n];
				d2 = key?data[n2][key]:data[n2];
		    	var ret = (orderby=='asc')?d1 > d2:d1 < d2;
				while(ret && n >= 0){
					temp=data[n];
					data[n]=data[n2];
					data[n2]=temp;
					n--;
					n2 = n + 1;
					d1 = key?data[n][key]:data[n];
					d2 = key?data[n2][key]:data[n2];
		    		ret = (orderby=='asc')?d1 > d2:d1 < d2;
				}
			}
			return data;
		}
		/**
		 * 希尔排序 性能最优
		 * @param  {[type]} data 输入一个需要被排序的数组
		 * @param  {[type]} orderby  asc升序，desc 降序
		 * @param  {[type]} key  指定数组中的key
		 * @return {[type]}      [description]
		 */
		,hill:function(data, orderby, key){
			orderby = orderby?(orderby+'').toLowerCase():'asc';
		    var interval = parseInt(data.length / 2)  //分组间隔设置
		    , n, n2, d1, d2;
		    while(interval > 0){
		        for(var i = 0 ; i < data.length ; i ++){
		            n = i; n2 = n - interval; 
		            d1 = key?data[n][key]:data[n]; 
		            d2 = key?(data[n2]?data[n2][key]:data[n2]):data[n2];
		    		var ret = (orderby=='asc')?d1 < d2:d1 > d2;
		            while(ret && n > 0){
		                var temp = data[n];
		                data[n] = data[n2];
		                data[n2] = temp;
		                n = n2;
		                n2 = n - interval;
			            d1 = key?data[n][key]:data[n]; 
			            d2 = key?(data[n2]?data[n2][key]:data[n2]):data[n2];
		    			ret = (orderby=='asc')?d1 < d2:d1 > d2;
		            }
		        }
		        interval = parseInt(interval / 2);
		    }
			return data;
		}
		/**
		 * 对象数组排序
		 * @param  {[type]} data 输入一个需要被排序的数组
		 * @param  {[type]} orderby  asc升序，desc 降序
		 * @param  {[type]} key  指定数组中的key
		 * @return {[type]}      [description]
		 */
		,compare:function(data, orderby, key){
			orderby = orderby?(orderby+'').toLowerCase():'asc';
			return data.sort(function(d1, d2){
				return (orderby=='asc')?d1[key] - d2[key]:d2[key] - d1[key];
			});
		}
	}
	,indexOf:function(data, search){
		for(var k in data){
			if( typeof(search) == 'function'){
				if( search(data[k]) ){ return k; }
				continue;
			}
			if( data[k] == search){
				return k;
			}
		}
		return -1;
	}
	/**
	 * 将object转换成字符串
	 * @param  {[type]} obj [description]
	 * @return {[type]}     [description]
	 */
	,serialize:function(obj, tab){
		if(typeof(obj)!='object')return obj;
		if( obj instanceof Array ){
			return ET.object.serialize(obj, '[', ']', '{{value}}', tab);
		}
		return null;
	}
	/**
	 * 去重复
	 * 将数组中重复项去除掉，返回为没有重复的新数组
	 * @return {[type]} 成功返回为数组
	 */
	,removeRepeat:function(data){
		if(!data || data==null || data==undefined){return data;}
		var n = {}, r = [], len = data.length, val, type;
		for (var i = 0, l=data.length; i < l; i++) {
			val = data[i];
			type = typeof val;
			if (!n[val]) {
				n[val] = [type];
				r.push(val);
			} else if (n[val].indexOf(type) < 0) {
				n[val].push(type);
				r.push(val);
			}
		}
		return r;
	}

}