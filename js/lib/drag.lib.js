/*
*
*	drag Beta 1.0.0.1
*	Release date: 拖动控件
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.object) ET.loadLib('object');
		if(!ET.event) ET.loadLib('event');
		if(!ET.unit) ET.loadLib('unit');
		if( ET.object.isElement(this) ){
			this.element(this);
		}
	}

	, auto : function(el, parentEl, callback){
		if(!el) return;
		el = $(el)[0];
		if(el.dragAuto)return;
		el.dragAuto = true;
		if(!parentEl) parentEl = $(el).parent()[0];		
		$(el).bind('mousedown', initEvent);
		$(el).bind('touchstart', initEvent);
		var elt = "0"+$(el).css('top').replace(/[^\d\.]+/gi, '')
		   ,ell = "0"+$(el).css('left').replace(/[^\d\.]+/gi, '')
		   ,ol = ($(parentEl).width()+$(el).width())
		   ,ot = ($(parentEl).height()+$(el).height());
		var opos={};
		var lpos={};
		function initkeyframes(){
			if($('#hstyDrag').length >0)return;
			var css = '@keyframes ml{ to { opacity:0; left:'+ ol*-1 +'px; } }\n'
					 + '@keyframes mr{ to { opacity:0; left:'+ ol +'px; } }\n'
					 + '@keyframes mt{ to { opacity:0; top:'+ ot*-1 +'px; } }\n'
					 + '@keyframes mb{ to { opacity:0; top:'+ ot +'px; } }\n'
					 + '@keyframes mo{ \n'
					 + '    to { opacity:1; top:'+ elt +'px;left:'+ ell +'px; } \n'
					 + '} \n'
					 + '@keyframes shw{ to { opacity:1; }\n'
					 ;
			var style = document.createElement('style');
			style.id = 'hstyDrag';
			style.type = 'text/css';
			style.innerHTML = css
			document.getElementsByTagName('head')[0].appendChild(style);
		}
		function initEvent(){
			if(event.touches && event.touches.length>1){
				cancelEvent();
				return true;
			}
			ET.event.preventDefault(event);
			opos = getCursorPos();
			var doc = window.document;
			$(doc).bind('mousemove', setElPos);
			$(doc).bind('touchmove', setElPos);
			$(doc).bind('mouseup', uninitEvent);
			$(doc).bind('touchend', uninitEvent);
		}
		function setElPos(){
			var cpos = getCursorPos();
			var x =(opos.x - cpos.x)*-1, y =(opos.y - cpos.y)*-1;
			if(x==0 && y==0)return;
			lpos=cpos;
			$(el).css({'position':'absolute', 'left':x + 'px', 'top': y + 'px'});

		}
		function uninitEvent(){			
			cancelEvent();
			var cpos = getCursorPos();
			if(!cpos.x && !cpos.y) cpos= lpos;
			var x =(opos.x - cpos.x)*-1, y =(opos.y - cpos.y)*-1;
			var animationName = "ml";
			if(x<0)x*=-1; if(y<0)y*=-1;
			var ll = 25;
			if( (x > ll*-1 && x < ll) && (y > ll*-1 && y < ll)
				)
				return;
			if(x>y){ // 左右
				animationName=((opos.x - cpos.x)>0)?"ml":"mr";
			}else{ // 上下
				animationName=((opos.y - cpos.y)>0)?"mt":"mb";
			}
			initkeyframes();
			var t = 300;
			$(el).css({
				"animation-name": animationName
				, "animation-duration": t/1000+"s"
				, "animation-iteration-count": "1"
				, "animation-fill-mode": "forwards"
			});
			if(callback)
				setTimeout(function(){
					callback(animationName); 
					if(animationName == 'mt'){
						$(el).css("top", ot + "px");
					}else if(animationName == 'mb'){
						$(el).css("top", ot*-1 + "px");
					}else if(animationName == 'ml'){
						$(el).css("left", ol + "px");
					}else if(animationName == 'mr'){
						$(el).css("left", ol*-1 + "px");
					}
					$(el).css({
						"animation-name": "mo"
						, "animation-duration": t/1000+"s"
						, "animation-iteration-count": "1"
						, "animation-fill-mode": "forwards"
					});	
				}, t);
		}
		function cancelEvent(){			
			var doc = window.document;
			$(doc).unbind('mousemove', setElPos);
			$(doc).unbind('touchmove', setElPos);
			$(doc).unbind('mouseup', uninitEvent);
			$(doc).unbind('touchend', uninitEvent);
		}
		function getCursorPos(){
			var e = event;
			if(e.touches && e.touches.length>0)
				e = e.touches[0];
			else if(e.changedTouches && e.changedTouches.length>0)
				e = e.changedTouches[0];
			return getMousePos(e);
			
		}
		function getMousePos(e){			
			e = e || window.event; 
			var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft; 
			var scrollY = document.documentElement.scrollTop || document.body.scrollTop; 
			var x = e.pageX || e.clientX + scrollX; 
			var y = e.pageY || e.clientY + scrollY; 
			return { 'x': x?x:0, 'y': y?y:0 };
		}


	}
	/**
	 * 拖动目元素
	 * @param  {[type]} ele    响应元素，即用于控制动作的目标元素
	 * @param  {[type]} target 被控制元素，可以ele同一个
	 * @param  {[type]} direction 方向限制，默认为0不限制， 
	 * 							1只限向上拖动，2只限向右拖动，
	 * 							4只限向下拖动，8只限向下拖动
	 * @param  {[type]} descriptor 附加参数与函数
	 * @return {[type]}        [description]
	 */
	,element:function(ele, target, direction, descriptor){
		if(!ele)return null;
		var doc = window.document;
		ele.target=target?target:ele;
		ele.direction=direction?direction:0;
		if(!descriptor)descriptor={};
		descriptor.wd_mousedown=function(){
			this.ontouchstart && this.ontouchstart.apply(this, arguments);
			this.mousedown && this.mousedown.apply(this, arguments);
		};
		descriptor.wd_mousemove=function(){
			this.touchmove && this.touchmove.apply(this, arguments);
			this.mousemove && this.mousemove.apply(this, arguments);
		};
		descriptor.wd_mouseup=function(){
			this.touchend && this.touchend.apply(this, arguments);
			this.mouseup && this.mouseup.apply(this, arguments);
		};
		function mousedown(e){
			ET.event.preventDefault(e);
			if(e.button>1)return ;
			this.position = $(this).css('position').toLowerCase();
			var ele=this, touch=false, target=$(ele.target), mxy=getMousePos(e.touches?e.touches[0]:e);	
			if(e.touches){e=e.touches[0];touch=true;}			
			ele.by={x:mxy.x-(target.offset().left),y:mxy.y-(target.offset().top)};			
			$(ele).css('cursor','move');			
			var tpos = {
				top:(target.css('top').replace('px',''))*1+(target.css('margin-top').replace('px',''))*1,
				left:(target.css('left').replace('px',''))*1+(target.css('margin-left').replace('px',''))*1
					};		
			target.css({
				'top':ET.unit.pxorem(tpos.top+'px')
				,'left':ET.unit.pxorem(tpos.left+'px')
				,'margin':ET.unit.pxorem('0px')
			});
			this.bindfun={
				"touchmove": function(e){wd_mousemove(e, ele);},
				"touchend": function(e){wd_mouseup(e, ele);},
				"mousemove": function(e){wd_mousemove(e, ele);},
				"mouseup": function(e){wd_mouseup(e, ele);}
			}
			if(touch){				
				doc.addEventListener('touchmove',this.bindfun.touchmove, false);
				doc.addEventListener('touchend',this.bindfun.touchend, false);
				// $(doc).bind('touchmove',function(e){wd_mousemove(e, ele);});
				// $(doc).bind('touchend',function(e){wd_mouseup(e, ele);});
			}else{
				$(doc).bind('mousemove', this.bindfun.mousemove);
				$(doc).bind('mouseup', this.bindfun.mouseup);
			}
			doc.onselectstart=new Function("event.returnValue=false"); 
			descriptor.wd_mousedown && descriptor.wd_mousedown(ele.by);
		}
		function wd_mousemove(e, ele){
			if(ele.by==undefined) return ;
			ET.event.preventDefault(e);
			var mxy=getMousePos( e.touches?e.touches[0]:e )
			, left = mxy.x-ele.by.x
			, top = mxy.y-ele.by.y
			, tgt = ele.target
			, position = $(ele.target).css('position');			
			if( position=='fixed' ){
				left-= $(document).scrollLeft();
				top-= $(document).scrollTop();
			}else if( position=='absolute' ){
			}else{
				if(!ele.vTarget){
					ele.vTarget = ET.ce('div', {'style':'position:absolute;border: '+ET.unit.pxorem('1px')+' dashed yellow;'});
					$(ele.vTarget).css({
						'width': ET.unit.pxorem($(ele.target).width()+'px')
						,'height': ET.unit.pxorem($(ele.target).height()+'px')
					});
					$(ele.target).after(ele.vTarget);
				}
				tgt=ele.vTarget;
			}
			if(ele.direction == 0){
				$(tgt).css('top', ET.unit.pxorem(top+"px"))
				.css('left', ET.unit.pxorem(left+"px"));
			}else{
				if(ele.direction & 1){
					if(ele.otop && top < ele.otop){
						$(tgt).css('top', ET.unit.pxorem(top+"px"));
					}
				}
				if(ele.direction & 2){
					if(ele.oleft && left > ele.oleft){
						$(tgt).css('left', ET.unit.pxorem(left+"px"));
					}
				}
				if(ele.direction & 4){
					if(ele.otop && top > ele.otop){
						$(tgt).css('top', ET.unit.pxorem(top+"px"));
					}
				}
				if(ele.direction & 8){
					if(ele.oleft && left < ele.oleft){
						$(tgt).css('left', ET.unit.pxorem(left+"px"));
					}
				}
				if(!ele.otop)ele.otop = top;
				if(!ele.oleft)ele.oleft = left;
			}
			descriptor.wd_mousemove && descriptor.wd_mousemove(ele.by, mxy);
		}
		function wd_mouseup(e, ele){
			e=e?e:event;
			if(e.touches){
				if(doc.removeEventListener )
					doc.removeEventListener('touchmove',wd_mousemove,false);
				else					
					doc.detachEvent("touchmove", wd_mouseup);
			}else{
				if(ele.bindfun)for(var k in ele.bindfun)
					$(doc).unbind(k, ele.bindfun[k]);
			}
			$(ele).css('cursor','');	
			if(ele.vTarget){$(ele.vTarget).remove(); ele.vTarget=undefined; }
			doc.onselectstart='';
			descriptor.wd_mouseup && descriptor.wd_mouseup(ele.by, getMousePos(e));
			ele.by=undefined;
		}
		function getMousePos(e){			
			e = e || window.event; 
			var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft; 
			var scrollY = document.documentElement.scrollTop || document.body.scrollTop; 
			var x = e.pageX || e.clientX + scrollX; 
			var y = e.pageY || e.clientY + scrollY; 
			return { 'x': x, 'y': y };
		}
		ele.onmousedown=ele.ontouchstart=mousedown;		
	}

}