/*
*
*	progress bar Beta 2.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*	
* 	eg:
* 	ET.loadLib('progressbar');
*	var b1= ET.progressbar.create();
*	$('section').append(b1);
*	setInterval(function(){b1.setPos(); }, 50);
*/
{
	initialize:function(){
		ET.loadLib('event'); 
	}
	/**
	 * 创建进度条
	 * @param  {[type]} min 范围，最小值,默认为0
	 * @param  {[type]} max 范围，最大值,默认为100
	 * @param  {[type]} width 控件宽,默认为100%
	 * @param  {[type]} height 控件高,默认为100%
	 * @param  {[type]} style 样式
	 * @return {[type]}   [description]
	 */
	,create:function(min, max, width, height, style){
		var _me = this;
		min=min?parseInt(min):0;
		max=max?parseInt(max):100;
		if(min>max){ [min, max] = [max, min]; }

		width = width?parseInt(width):'100%';
		height = height?parseInt(height):'auto';
		var div = ET.ce('div', {"class":"ET_PROGRESS_BAR", "style":
			"width:" + ET.unit.pxorem(width+'px') +
			";height:" + ET.unit.pxorem(height+'px')
		})
		,btn0 = ET.ce('div', {"class":"btn"})
		,hblock = ET.ce('div', {"class":"block"})
		,btn1 = ET.ce('div', {"class":"btn"})
		;
		div.data = {min:min, max:max, idx:min};
		$(div).append([btn0, hblock, btn1]);
		$.extend(div, this.fn, {block:hblock, btn:[btn0, btn1]});
		Object.defineProperty(div, 'value', {
			get:function(){ return this.data.idx; }
			,set:function(v){ this.data.idx = v; }
		});
		ET.object.listen(div.data, {set:function(){ div.render(); }});
		return div;
	}
	,fn:{
		render:function(){
			var pdata = this.data, w =$(this).width()
			, h =$(this).height();
			var r = (pdata.idx - pdata.min)/(pdata.max - pdata.min)
			,txt =''
			,rng =(r*100).toFixed(2)+'%'
			;
			if(r>1){this.error(); return ;}
			if(r==1){this.complete();}
			if(r<0){this.error(); return ;}
			txt ='Range : ' + pdata.min + ' ~ ' + pdata.max;
			txt +='\nValue : ' + pdata.idx;
			txt +='\nRatio : ' + rng;

			var bw = w*r;
			$(this.block).css('width', ET.unit.pxorem(bw+'px') );
			$(this).attr('title', txt);
			if(rng.length*9 > bw){
				$(this).attr('value', rng);
				$(this.block).attr('value', '>');
			}else{
				$(this.block).attr('value', rng);
				$(this).attr('value', '');
			}

		}
		,incrementProgressBy:function(val){this.data.idx+=val?parseInt(val):1;}
		,complete:function(){console.log('complete');}
		,error:function(){console.log('err');}
	}
	
}