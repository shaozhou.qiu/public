/*
*
*	 SOCKS5  Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		
	}
		/**
	 * 编码
	 * MTProto 转 base64
	 * @param  {[type]} data [description]
	 * @return {[type]}     [description]
	 */
	,encode:function(server, port, secret, remarks){
		var msg = '';
        var data = 'MTProto://'+ this.base64_encode(secret +'@'+ server +':'+port) ;
        if(remarks){
        	data +='#' + encodeURIComponent(remarks);
        }
        return data ;
	}
	/**
	 * 解码
	 * 将base64 转为 MTProto
	 * @param  {[type]} str 如ss://xxxxxx
	 * @return {[type]}     [description]
	 */
	,decode:function(str){
		if(!str){return null;}
		var re = "^[\\s]*MTProto:\\/\\/(\\w+?)[\\t\\s=]*(#.*)?$";
		if( false==(new RegExp(re, 'gi')).test(str) ){return null; }
		var m = (new RegExp(re, 'gi')).exec(str);
		if(!m){return null;}
		var remarks = m[2]?m[2].substr(1):'';
		remarks = decodeURIComponent( remarks.replace(/\+/gi, ' '));
		var url=this.base64_decode(m[1]);
		m = /^(([^:]+):)?([^\@]+)\@([^:]+):(.+)$/gi.exec(url);
		if(!m){ return null;}
		return {
			// username: m[2],
			secret: m[3]
			,server: m[4]
			,port: m[5]
			,remarks: remarks
		};
	}
	,base64_decode:function(str){
		if(!str) return str;
		str = str.replace(/[\t\s]*(.*?)[\t\s=]*/gi, '$1');
		str = str.replace(/_/gi,'\/');
        str = str.replace(/[\-\–]/gi,'\+');
        str = decodeURIComponent(escape(atob( str )));
        // str = atob(str);
        return str;
	}
	,base64_encode:function(str){
		if(!str) return str;
        // str = btoa(str);
        str = btoa(unescape(encodeURIComponent( str )));
        str = str.replace(/[\=\t\s]+$/gi,'');
        str = str.replace(/\//gi,'_');
        str = str.replace(/\+/gi,'–');
        return str;		
	}
}