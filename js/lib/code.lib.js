{
	initialize:function(){
		ET.loadLib('string');
		ET.loadLib('stx');
	}
	,removeComments:function(code, type){
		type=type?type.toLowerCase():'js';
		if(ET.stx){ 
			if(ET.stx.lib[type]){
				code = ET.stx.lib[type].removeComments(code);
			}else{
				ET.stx.load(type);
				// code = null;
			}
		}else{
			code = this._removeComments(code);
		}
		return code;
	}
	,compress:function(code){
		code = this.removeComments(code);
		code = ET.string.preclude(code, /('[^']')|("[^"]")/gi, function(code){
			code = code.replace(/}(\s*[^\s\,\)\}])/gi, '};$1');
			code = code.replace(/\s+/gi, ' ');
			code = ET.string.preclude(code, /\w+\s\w+/gi, function(code){
				return code.replace(/\s+/gi, '');
			});
			
			return code;
		});
		return code;
	}
	,_removeComments:function(code, commenton, quotation){
		quotation = quotation?quotation:["\"", "'"];
		commenton = commenton?commenton:["\\/\\/.*", "\\/\\*[\\W\\w]*?\\*/"];
		var _me = this, rc = function(code){
			var  res=[];
			quotation.forEach(function(v, i, arr){
				var restr = '';
				v = v.replace(/([\r\n\$\"\'])/gi, '\\$1');
				restr += "("+v+"[^"+v+"\r\n\$]*"+v+")"; // 防止跨行字符串
				res.push( new RegExp(restr, 'gi') );
			});
			code = preclude(code, res, function(code){
				var restr = '(^|[^\\w\\\\]|\\s)(';
				commenton.forEach(function(v,i){
					restr += '('+v+')|'; 
				});
				restr = restr.substr(0, restr.length-1);
				restr += ')(\\W|\\s|$)';
				var re = new RegExp(restr, 'gi');
				return code.replace(re, '');
			});
			return code;
		}
		,preclude=function(code, res, fun){
			res = typeof(res)=="string"?[res]:res;
			var re = res[0];
			if(!ET.string){ return code; }
			return ET.string.preclude(code, re, function(code){
				if(res.length>1){
					code = preclude(code, res.filter(function(v,i){ return i>0;}), fun);
				}else{
					code = fun?fun(code):code;
				}
				return code;
			});
		};
		return rc(code);

	}
}