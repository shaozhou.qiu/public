/*
*
*	tablelib 1.0.0.1  数据验证
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
	},
	fn:{
		get:function(){
			var data = this.getData(), reval=[];
			for(var i=0, l=data.length; i<l; i++){
				var d = data[i], ismacth=false;
				for(var j=0, c=this._where.length; j<c; j++){
					var w = this._where[j], re='';
					if( ! d[w.key] ){ ismacth = false; break;}
					if( w.oprater == '=' ){ re = w.value;}
					if( w.oprater == 'like' ){ re = (w.value+'').replace(/(\\|\*|\.|\-)/gi, '\\$1').replace('%', '.*') ;}

					re = new RegExp('^\\s*' + re + '\\s*$', 'ig');
					if( !re.exec(d[w.key]) ){ ismacth = false; break;}
					ismacth = true;
				}
				if(ismacth){
					if( this._field && this._field.length>0 ){
						var nd={};
						for (var k = 0, kl =this._field.length; k < kl; k++) {
							var key = this._field[k];
							nd[key] = d[key];
						}
						reval.push(nd);
					}else{
						reval.push(d);
					}
				}
			}
			this._where = [];
			this._field = [];
			return reval;
		},
		select:function(){
			var args = arguments;
			if(args.length<1 ){ return this; }
			if(args.length>1 ){
				var data =[];
				for(var i=0, l=args.length; i<l; i++){
					data.push(args[i]);
				}
				return this.select(data);
			}
			if(args.length==1 && ET.object.getType(args[0])=='array' ){
				for(var i=0, l=args[0].length; i<l; i++){
					this.select(args[0][i]);
				}
				return this;
			}
			if(args.length==1 && ET.object.getType(args[0])=='string' ){
				if(!this._field){this._field=[];}
				this._field.push(args[0]);
			}
			return this;
		},
		// this->where([{'ss':11}, {'ss':22} ]);
		where:function(){
			var args = arguments;
			if(args.length==1 && ET.object.getType(args[0])=='array' ){
				var _me=this, d=args[0];
				for(var k in d){
					_me = this.where(k, '=', d[k]);
				}
				return _me;
			}
			if(args.length==2){
				return this.where(args[0], '=', args[1]);
			}
			if(args.length==3){
				if(!this._where){this._where=[];}
				this._where.push({
					key : args[0]
					,oprater : args[1]
					,value : args[2]
				});
				return this;
			}
			return this;
		},
		getData:function(filter){ return ET.tablelib.getData(this, filter); },
		getRowDataByKey:function(tr, filter){ return ET.tablelib.getRowDataByKey(tr, this, filter); },
		getRowValue:function(tr, filter){ return ET.tablelib.getRowValue(tr, filter); },
		getKey:function(th){ return ET.tablelib.getKey(this, th); },
		getKeyMap:function(){ return ET.tablelib.getKeyMap(this); },
		getKeyTemplate:function(){ return ET.tablelib.getKeyTemplate(this); },
	},
	/**
	 * 初始化表格并返回
	 * @param  {[type]} tbl [description]
	 * @return {[type]}     [description]
	 */
	getTable:function(tbl){
		var tbls = this.initializeTable(tbl);
		var data=[];
		tbls.each(function(){ data.push(this); });
		return data;
	},
	/**
	 * 初始化表格
	 * @param  {[type]} tbl [description]
	 * @return {[type]}     [description]
	 */
	initializeTable:function(tbl){
		tbl = $(tbl);
		tbl.each(function(){ !this.initlib && ET.extend(this, _me.fn); this.initlib = true;});
		return tbl;
	},
	/**
	 * 获取所有数据
	 * @param  {[type]} tbl     [description]
	 * @param  {[type]} filter [description]
	 * @return {[type]}         [description]
	 */
	getData:function(tbl, filter){
		tbl = $(tbl?tbl:this);
		var  trs = tbl.find('>tbody>tr')
		,data = [];
		for(var i=0, l=trs.length; i<l; i++){
			data.push( this.getRowDataByKey(trs[i], tbl, filter) );
		}
		return data;
	},
	/**
	 * 获取行数据带标题
	 * @param  {[type]} tr      [description]
	 * @param  {[type]} tbl     [description]
	 * @param  {[type]} filter [description]
	 * @return {[type]}         [description]
	 */
	getRowDataByKey:function(tr, tbl, filter){
		var keys = this.getKey(tbl), idx=0
		, data = this.getRowValue(tr, filter)
		, ret = {};
		for(var i=0, l=data.length; i<l; i++){
			var k = keys[i];
			k = k?k:'column_'+i;
			ret[k] = data[i];
		}
		return ret;
	},
	/**
	 * 获取列数据
	 * @param  {[type]} td      [description]
	 * @return {[type]}         [description]
	 */
	getColData:function(td){
		if(!td) return null;
		var html = $(td).html()
		, txt = $(td).text()
		, val = html
		, data = {};
		txt = txt.replace(/^[\s\r\n]*(.*?)[\s\r\n]*$/gi, '$1');
		var m = /^\s*(\d+)\.?(\d+)?\s*$/gi.exec(txt);
		if(m){
			var z = m[1], y = m[2], max = 2, n=0;
			while(n++<54)max*=2;
			if(parseInt(z)>max){
				val = txt;
			}else if(y==undefined){
				val = parseInt(z);
			}else{
				val = (parseInt(y)>max)?txt:parseFloat(z+'.'+y);
			}
		}
		var attrs = td.attributes;
		for(var i=0, l=attrs.length; i<l; i++){
			var attr = attrs[i].name;
			data[attr] = $(td).attr(attr); 
		}
		data['value'] = val;
		['rowspan', 'colspan'].map(function(e){
			var val = $(td).attr(e);
			data[e] = parseInt(val?val:0);
		});
		return data;
	},
	/**
	 * 获取行数据 
	 * @param  {[type]} tr      [description]
	 * @param  {[type]} filter [description]
	 * @return {[type]}         [description]
	 */
	getRowData : function(tr, filter){
		filter = filter?filter:function(e){return e;}
		var _me = this, tds =$(tr).find('td,th')
		,data =[];
		tds.each(function(){
			var dat = _me.getColData(this);
			data.push( filter.call(this, dat) );
		});
		return data;
	},
	/**
	 * 获取行数据 value
	 * @param  {[type]} tr      [description]
	 * @param  {[type]} filter [description]
	 * @return {[type]}         [description]
	 */
	getRowValue:function(tr, filter){
		filter = filter?filter:function(e){return e;}
		var _me = this;
		return	this.getRowData(tr, function(e){
			return filter.call(this, e.value);
		});
	},	
	mergeCol:function(tds, thd){
		if(!tds || tds.length == 1) return tds;
		thd = thd?thd:'td';
		var data =  null, val = '';
		for (var i = 0, l = tds.length; i < l; i++) {
			var d = this.getColData(tds[i]);
			if(!data){ data = d; val = d.value; continue;}
			for(var k in d){
				if(k == 'value'){
					val += '/' + d[k];
					continue;
				}
				data[k] = data[k]?data[k]+d[k]:d[k];
			}
		}
		delete(data.colspan);
		delete(data.rowspan);
		delete(data.value);
		var td = ET.ce(thd, data);
		$(td).html(val);
		return td;
	},
	mergeRow:function(trs, thd){
		if(!trs || trs.length <2){return trs;}
		var tr=null, ntr = null;
		if(trs.length>2){
			for (var i = 0, l = trs.length; i < l; i++) {
				tr = trs[i];
				if(!ntr){ntr = tr; continue; }
				ntr =  this.mergeRow([ntr, tr]);
			}
		}else{
			tr = ET.ce('tr');
			var tds0 = $(trs[0]).find('>td,>th')
			, tds1 = $(trs[1]).find('>td,>th')
			, stp1 = 0;
			;
			for(var i = 0, l = tds0.length; i < l; i++){
				var d = this.getColData(tds0[i]), tds=[];
				if(d.rowspan < 2 ){
					if(d.colspan < 2){
						tds = this.mergeCol([tds0[i], tds1[stp1]], thd);
						stp1 ++;
					}else{
						for(var j = 0; j < d.colspan; j++){
							var td1 = tds1[stp1];
							var td = this.mergeCol([tds0[i], td1], thd);
							if(td1.colspan>1){
								$(td).attr('colspan', td1.colspan);
							}
							if(td1.rowspan>1){
								$(td).attr('rowspan', td1.colspan);
							}
							tds.push(td);
							stp1++;
						}
					}
				}else{
					d.rowspan--;
					if(d.rowspan<2) delete(d.rowspan);
					if(d.colspan<2) delete(d.colspan);
					var html = d.value;
					delete(d.value);
					tds = ET.ce(thd, d);
					$(tds).html(html);
				}
				$(tr).append(tds);
			}
		}
		return tr;
	},
	/**
	 * 获取th标签排序
	 * @param  {[type]} tbl [description]
	 * @param  {[type]} col  [description]
	 * @return {[type]}     [description]
	 */
	getColIndex:function(col, tbl){
		if(!col){return -1;}
		var ret = $(col).index(), tr = $(col).parent();
		while(tr.length){
			tr.find('>th,>td').each(function(){
				var rs = $(this).attr('rowspan')
				,cs = $(this).attr('colspan');
				rs = rs?rs*1:0;
				cs = cs?cs*1:0;
				if(rs>1){ ret++; }
			});
			tr = $(tr).prev();
		}
		return ret;
	},
	getColAttr:function(th, fun){
		if(!th)return null;
		var data={}, colData = this.getColData(th);
		if(!colData)return null;
		var key = colData.value.replace(/\s*(.*?)\s*/gi, '$1')
		, val = null;
		if(fun){val = fun.call(th, colData);}
		data[key] = val;
		return data;
	},
	getKeyData:function(tbl, fun){
		var node = $(tbl).find('>thead')
		, trs = node.find('>tr')
		, tr = this.mergeRow(trs, 'td')
		, tds = $(tr).find('>td, >th')
		, data = {};
		for(var i=0, l=tds.length; i<l; i++){
			var d=tds[i];
			if(fun){d = fun.call(this, d);}
			$.extend(data, d);
		}
		return data;
	},
	getColTemplate:function(th){
		return this.getColAttr(th, function(e){
			var d = e.template?e.template:e.tpl?e.tpl:'';
			return d?d:'';
		});
	},
	getKeyTemplate:function(tbl){
		return this.getKeyData(tbl, function(e){
			return this.getColTemplate(e);
		});
	},
	getColMap:function(th){
		return this.getColAttr(th, function(e){
			return (e.feild?e.feild:e.value).replace(/\s*(.*?)\s*/gi,'$1');
		});
	},
	getKeyMap:function(tbl){
		return this.getKeyData(tbl, function(e){
			return this.getColMap(e);
		});
	},
	getKey:function(tbl, th){
		tbl = $(tbl?tbl:this);
		var _me = this, node = tbl.find('>thead')
		, trs = null;
		if(node.length == 0){
			var trFirst = tbl.find('>tbody>tr:first')
			trs = trFirst.clone();
			trs.find('>th, >td').each(function(){
				$(this).html('column_' + $(this).index());
			});
		}else{
			trs = node.find('>tr');
		}
		var tr = this.mergeRow(trs, 'td');
		var data = this.getRowValue(tr, function(e){
			e = (e+'').replace(/(\<[^\>]*\>)|(\&[\w\d]+;)/gi, '');
			e = e.replace(/\s+/gi, ' ');
			e = e.replace(/^\s+(.*?)\s*$/gi, '$1');
			if(e == ''){ e = 'column_' + $(this).index(); }
			return e;
		});
		if(th){
			var idx = this.getColIndex(th, tbl);
			if(idx==-1 || !(idx <data.length)){return null;}
			return data[idx];
		}
		return data;
	}
}

