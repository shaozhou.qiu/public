/*
*
*	json 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
        ET.loadLib('object');
	}
	/**
	 * 编码
	 * vmess 转 base64
	 * @param  {[type]} data [description]
	 * @return {[type]}     [description]
	 */
    ,encode:function(add, port, type, id, aid, host, net, path, ps, tls, v ){
        var data = {
            'add': add
            , 'aid': aid?aid:233
            , 'host': host?host:add
            , 'id': id
            , 'net': net?net:"ws"
            , 'path': path?path:"/"
            , 'port': port
            , 'ps': ps?ps:""
            , 'tls': tls?tls:"tls"
            , 'type': type?type:"none"
            , 'v': v?v:2
        };
        return 'vmess://'+this.base64_encode(ET.object.toString(data));
	}
	/**
	 * 解码
	 * 将base64 转为 vmess
	 * @param  {[type]} str 如 vmess://bm9uZTo3Mjg0MjZiMC0xMDM0LTQ0NTAtYTgzNS1iNDk0MzFkYjYwNjZAMTk0LjE1Ni4xMjEuMTI0OjM4MDI4
	 * @return {[type]}     [description]
	 */
	,decode:function(str){
        var m = /vmess:\/\/(.*?)(\?.*)?$/gi.exec(str);
        if(!m)return null;
        var m1 = this.base64_decode(m[1]);
        var ps = (m[2]?m[2]:'').match(/([^=&?]+)=([^&]*)/gi);
        var m = /([^:]*):([^@]*)@([^:]*):(\d+)/gi.exec(m1)
        , data = {
            'method' : '', 'id' : ''
            , 'add' : '127.0.0.1' , 'port' : 443
            , 'ps' : 'default', 'aid' : ''
            , 'net': "ws", 'type': "none"
            , 'host': 'localhost'
            , 'path': "/", 'tls': "tls", 'v' : '2'
        };
        if(m){
            $.extend(data, {
        		'method' : m[1]
        		, 'id' : m[2]
        		, 'add' : m[3]
        		, 'port' : m[4]
                , 'host': m[3]
        	});
        }else if(m = /^\s*\{[\w\W]*\}\s*$/gi.test(m1)){
            data = ET.execJS(m1);
        }else{
            var m = /^(.*?)\s*=\s*vmess\s*,\s*([(\w+\.)]+)\s*,\s*(\d+)\s*,\s*(\w+)\s*,\s*['"]?([\w\-]+)/gi.exec(m1);
            if(m){
                $.extend(data, {
                    'method' : m[4]
                    , 'id' : m[5]
                    , 'add' : m[2]
                    , 'port' : m[3]
                    , 'host': m[2]
                    , 'ps': m[1]
                });
                var rem = m1.match(/".*?"/gi), nstr=m1;
                if(rem){
                    for(var i=0, l=rem.length; i<l; i++){
                        nstr = nstr.replace(rem[i], '{rem_'+i+'}');
                    }
                    var p = nstr.match(/[\w+\-]+\s*=\s*[^,]+/gi);
                    if(p){
                        for(var i=0, l=p.length; i<l; i++){
                            for(var j=0, jl=rem.length; j<jl; j++){
                                p[i] = (p[i]+'').replace('{rem_'+j+'}', rem[j]);
                            }

                            var m=/([^=]+)\s*=\s*"?([^"]+)"?/gi.exec(p[i]);
                            if(m){
                                var d={};
                                d[m[1]] = m[2];
                                $.extend(data, d);
                            }
                        }
                    }
                }
            }
        }
        if(ps){
            for(var i=0; i<ps.length; i++){
                var p = /([^=&?]+)=([^&]*)/gi.exec(ps[i]);
                var p2 = decodeURIComponent(escape(p[2]));
                if(p[1] == 'remarks'){
                    data.ps = p2
                }else{
                    data[p[1]] =  p2;
                }
            }
        }
        return data;

	}
    ,base64_decode:function(str){
    	if(!str) return str;
        str = str.replace(/[\t\s]*(.*?)[\t\s=]*/gi, '$1');
        str = str.replace(/_/gi,'\/');
        str = str.replace(/[\-\–]/gi,'\+');
        str = decodeURIComponent(escape(atob( str )));
        // str = atob(str);
        return str;
    }    
	,base64_encode:function(str){
		if(!str) return str;
        // str = btoa(str);
        str = btoa(unescape(encodeURIComponent( str )));
        str = str.replace(/[\=\t\s]+$/gi,'');
        str = str.replace(/\//gi,'_');
        str = str.replace(/\+/gi,'–');
        return str;		
	}



}