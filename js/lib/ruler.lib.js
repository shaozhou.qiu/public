/*
*
*	ruler Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
	}
	, createInstance:function(min, max, dataMin, dataMax, scale){
		scale = scale?scale:5;
		var ruler = {
			'min' : min
			,'max' : max
			,'data' : {min:dataMin, max:dataMax}
			,'scale' : scale
		};
		$.extend(ruler, this.fn);
		return ruler;
	}
	/**
	 * 获取两个坐标之间的距离和方向
	 * @param  {[type]} x1 [description]
	 * @param  {[type]} y1 [description]
	 * @param  {[type]} x2 [description]
	 * @param  {[type]} y2 [description]
	 * @return {[type]}    [description]
	 * ^
	 * |2
	 * |1
	 * |___________>
	 * x:0, y:0
	 */
	,direction(x1, y1, x2, y2){
		var x = x2-x1;
		var y = y2-y1;
		x = x<0?x*-1:x;
		y = y<0?y*-1:y;
		var data = {min:0 , max:100 ,direction:1 }
		if(x>y){
			data.direction = x1>x2?3:1;
			data.min = x1<x2?x1:x2;
			data.max = x1>x2?x1:x2;
		}else{
			data.direction = y1>y2?2:0;
			data.min = y1<y2?y1:y2;
			data.max = y1>y2?y1:y2;
		}
		return data;
	}
	,getPosByDirection(pos, direction, callback){
		var x = pos.x, y = pos.y;
		if(callback == undefined)return pos;
		if(typeof(callback)=='function'){
			fun = callback;
		}else{
			fun = function(){return callback;}
		}
		switch(direction){
			case 0:
				y -= fun.call(pos, y, direction);
				break;
			case 1:
				x += fun.call(pos, x, direction);
				break;
			case 2:
				y += fun.call(pos, y, direction);
				break;
			case 3:
				x -= fun.call(pos, x, direction);
				break;
		}
		return {x:x, y:y};
	}
	,fn:{
		// 获取比率
		getRate : function(){
			var slen = this.max - this.min;
			var dlen = this.data.max - this.data.min;
			return dlen/slen;
		}
		// 数据取反
		, getOppositeData : function(v){
			var dlen = this.data.max - this.data.min;
			return dlen-parseFloat(v);
		}
		, getOppositeScale : function(v){
			var dlen = this.max - this.min;
			return dlen-parseFloat(v);
		}
		// 根据刻度数获取数据值
		, getDataByScale : function(v){
			var rate = this.getRate();
			return v*rate;
		}
		// 根据刻度数获取数据值
		, getScaleByData : function(d){
			var rate = this.getRate();
			return d/rate;
		}
		// 根据数据获取原始刻度数
		, getOriginalScaleByData : function(d){
			d = d - this.data.min;
			return this.min + this.getScaleByData(d);
		}
		// 根据数据获取刻度值
		, getOriginalDataByScale : function(v){
			var vd = this.getDataByScale(v);
			return this.data.min + vd;
		}
		// 获取刻度集合
		, getScale : function(callback){
			var data = [];
			var slen = this.max - this.min;
			var v = slen/this.scale;
			for(var i=0; i<this.scale+1; i++){
				var scl = v*i;
				var d = {
					scale : this.min+scl
					,data : this.getOriginalDataByScale(scl)
				};
				callback && callback.call(this, d);
				data.push(d);
			}
			return data;
		}

	}
}