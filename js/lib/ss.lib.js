/*
*
*	json 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
	}
	/**
	 * 编码
	 * ss转 base64
	 * @param  {[type]} data [description]
	 * @return {[type]}     [description]
	 */
	,encode:function(server, port, method, password, remarks){
        var data = 'ss://'+ this.base64_encode(encodeURIComponent(method)+':'+ encodeURIComponent(password) +'@'+ encodeURIComponent(server) +':'+port) ;
        if(remarks){
        	data +='#' + encodeURIComponent(remarks);
        }
        return data ;
	}
	/**
	 * 解码
	 * 将base64 转为 ss
	 * @param  {[type]} str 如ss://xxxxxx
	 * @return {[type]}     [description]
	 */
	,decode:function(str){
		if(!str){return null;}
		var m = /^\s*ss:\/\/(.*?)@(.+?)(:(\d+))?(#.*)?$/gi.exec(str);
		if(m){
			var head = m[1];
			if(!/.+\:.+/gi.test(head)){ head = atob(head);}
			head = /(.+)\:(.+)/gi.exec(head);
			return {
				method: head[1]
				,password: head[2]
				,server: m[2]
				,port: m[3].substr(1)*1
				,remarks: m[5].substr(1)
			};
		}
		var re = "^[\\s]*ss:\\/\\/(\\w+?)[\\t\\s=]*(#.*)?$";
		if( false==(new RegExp(re, 'gi')).test(str) ){return null; }
		var m = (new RegExp(re, 'gi')).exec(str);
		if(!m){return null;}
		var remarks = m[2]?m[2].substr(1):'';
		remarks = decodeURIComponent( remarks.replace(/\+/gi, ' '));
		var url=this.base64_decode(m[1]);
		m = /^([^:]+):([^\@]+)\@([^:]+):(.+)$/gi.exec(url);
		if(!m){ return null;}
		return {
			method: m[1]
			,password: m[2]
			,server: m[3]
			,port: m[4]
			,remarks: remarks
		};
	}
	,base64_decode:function(str){
		if(!str) return str;
		str = str.replace(/[\t\s]*(.*?)[\t\s=]*/gi, '$1');
		str = str.replace(/_/gi,'\/');
        str = str.replace(/[\-\–]/gi,'\+');
        str = decodeURIComponent(escape(atob( str )));
        // str = atob(str);
        return str;
	}
	,base64_encode:function(str){
		if(!str) return str;
        // str = btoa(str);
        str = btoa(unescape(encodeURIComponent( str )));
        str = str.replace(/[\=\t\s]+$/gi,'');
        str = str.replace(/\//gi,'_');
        str = str.replace(/\+/gi,'–');
        return str;		
	}
}