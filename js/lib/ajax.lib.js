/*
*
*	ajax Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		var _me = this, xhr = window.XMLHttpRequest;
		if ( typeof window.CustomEvent != "function" ){
			this.customEvent.prototype = window.Event.prototype;
  			window.CustomEvent = this.customEvent;
		}
		window.XMLHttpRequest = function(){
			var nXHR = new xhr();
			['abort', 'error', 'load', 'loadstart', 'progress', 'timeout', 'loadend', 'readystatechange'].forEach(function(v, i)
			{
				nXHR.addEventListener(v, function () { _me.dispatchEvent.call(this, v); }, false);
			});
			return nXHR;
		}
	}
	, addEventListener :function(evt, fun){
		if(!evt || !fun){return ;}
		evt = 'onAjax'+evt.replace(evt[0], evt[0].toUpperCase());
		window.addEventListener(evt, fun);
	}
	,customEvent:function(event, params){
		params = params || { bubbles: false, cancelable: false, detail: undefined };
		var evt = document.createEvent( 'CustomEvent' );
		evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
		return evt;		
	}
	/**
	 * 事件调度
	 * @return {[type]} [description]
	 */
	,dispatchEvent:function(event){
		event = 'onAjax'+event.replace(event[0], event[0].toUpperCase());
		var ajaxEvent = new CustomEvent(event, { detail: this });
		window.dispatchEvent(ajaxEvent);
	}
};