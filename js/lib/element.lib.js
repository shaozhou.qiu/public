/*
*
*	element Beta 1.0.0.0
*	Release date: 2020-4-29
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		
	}
	,new : function(el){
		if(!el )return el;
		$.extend(el, this.fn, {libParent:this});
		return el;
	}
	,getRect : function(el){
		var reVal = {};
		if(el == document)el = document.documentElement;
		$.extend(reVal
			,(el instanceof HTMLElement)?$(el).offset():{top: 0, left: 0}
			,{
				'width' : $(el).width()
				,'height' : $(el).height()
			}
			,{
				'clientTop' : el.clientTop?el.clientTop:0
				,'clientLeft' : el.clientLeft?el.clientLeft:0
				,'clientWidth' : el.clientWidth?el.clientWidth:0
				,'clientHeight' : el.clientHeight?el.clientHeight:0
			}
			,{
				'offsetTop' : el.offsetTop?el.offsetTop:0
				,'offsetLeft' : el.offsetLeft?el.offsetLeft:0
				,'offsetWidth' : el.offsetWidth?el.offsetWidth:0
				,'offsetHeight' : el.offsetHeight?el.offsetHeight:0
			}
			,{
				'scrollTop' : el.scrollTop?el.scrollTop:0
				,'scrollLeft' : el.scrollLeft?el.scrollLeft:0
				,'scrollWidth' : el.scrollWidth?el.scrollWidth:0
				,'scrollHeight' : el.scrollHeight?el.scrollHeight:0
			}
		);
		return reVal;

	}
	,fn : {
		getRect : function(){
			return this.libParent.getRect(this);
		}
		,addEvent : function(name, callback){
			if(arguments.length==1 && typeof(name) !='string'){
				for(var k in name){
					this.addEvent(k, name[k]);
				}
				return this;
			}
			if(!name || typeof(name)!='string')return ;
			name = name.toLowerCase();
			if(callback && typeof(callback)=='function'){ 
				this[name] = callback; 
			}else{
				if(!this['on'+name]){
					var code = $(this).attr('on'+name);
					this['on'+name] = new Function(code?code:'return 0;');
				}
				if(!this[name]){ 
					this[name] = this['on'+name]; 
				}
			}
			return this;
		}
		// 监听变化
		,mutationObserver : function(fun, conf){
			var mto = new MutationObserver(fun);
			mto.observe(this, $.extend({
				attributes: true
				, childList: true
				, subtree: true 
			}, conf));
		}
	}
	
}