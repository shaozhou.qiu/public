/*
*
*	json 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
        ET.loadLib('progressbar'); 
	}
    , input : function(el, cb){
        el = $(el);
        if(el.length<1){return; }
        el=el[0];
        this.file(el.files, cb);
    }
    , remoteFile : function(url, cb){
        var fun = "remoteFile_"+ET.random.Hex(5);
        if(!cb)cb=function(e){};
        $.ajax({
           url: ET.appPath + "/EtSoftWare.php?m=Upload"
           ,type:'GET' 
           ,data:{url: encodeURIComponent(url)}
           ,dataType:'json'
           ,success : cb
        });
    }
    , file : function(FileList, cb){
        var div = ET.ce('div');
        var proCount = ET.progressbar.create(0, 100, 200, 20);
        var proFile = ET.progressbar.create(0, 100, 200, 20);
        var total = ET.ce('div', {innerHTML:'total'});
        var curFile = ET.ce('div', {innerHTML:'...'});
        $(div).append([total, proCount, curFile, proFile]);
        ET.messagebox.show(div);
        if(FileList.length == 0){
            cb({errcode:1, data:'No such file found!'});
            return ;
        }
        proCount.data.max = FileList.length; 
        proCount.data.idx = 1; 
        // 0.1M
        var uploadfiles=[];
        this.fn.idx=0;
        this.fn.upload(FileList, function(e){
            switch(e.errcode){
                case 0: {
                    uploadfiles.push(e.data);
                    cb({errcode:0, data:uploadfiles});
                    break;
                }
                case 1: {
                    proCount.data.idx++;
                    uploadfiles.push(e.data);
                    break;
                }
                case 2: {
                    proFile.data.idx = e.idx;
                    proFile.data.max = e.num;
                    $(curFile).html(e.data);
                    break;
                }
                case 3: {
                    cb(e); break;
                }
            }

        });
    }
    , fn : {
        idx:0
        , blockSize : 1024*1024*0.1
        , upload : function(files, cb){
            var _me=this; ret = {idx:this.idx}
            if(!this.idx<files.length){
                cb(ret);
                return ;
            }
            this.uploadsingle(files[this.idx], function(e){
                switch(e.errcode){
                    case 0:{
                        _me.idx++;
                        if(!(_me.idx<files.length)){
                            cb(e); return;
                        }else{
                            cb({errcode:1, data:e.data});
                            _me.upload(files, cb);
                        }
                        break;
                    }
                    case 1:{
                        cb({errcode:2, idx:e.idx, num:e.num, data:e.data});
                        break;
                    }
                    case 3:{
                        cb(e); return;
                        break;
                    }
                }

            });
        }
        , uploadsingle : function(file, cb, idx, bs){
            var _me=this, fn = file.name, fs = file.size;
            var num = Math.ceil(fs/this.blockSize);
            idx = !idx?1:idx;
            if(idx>num){ cb({errcode:0, data : 'finish'});return; }
            bs=bs==undefined?0:bs;
            var be = this.blockSize*idx;
            if(be>fs)be=fs;
            var b = file.slice(bs, be);
            fd = new FormData();
            fd.append('block', b);
            fd.append('name', fn);
            fd.append('total', num);
            fd.append('index', idx);
            cb({errcode:1, idx:idx, num:num, data:'upload:' + fn + "<br>size:" + parseInt(fs) +"/" +parseInt(be)});
            $.ajax({
               url: ET.appPath + "/EtSoftWare.php?m=Upload"
               ,type:'POST' 
               ,data:fd
               ,dataType:'json'
               ,processData:false
               ,contentType:false
               ,success : function(e){
                    if(e.errcode == 0) {cb(e); return ;}
                    if(e.errcode == 3){
                        _me.tryNum = _me.tryNum?_me.tryNum+1:1;
                        if(_me.tryNum>5){
                            ET.messagebox.show(e.errmsg);
                            _me.tryNum = 0;
                            cb(e); return ;
                        }
                        _me.uploadsingle(file, cb, idx, be);
                        return ;
                    }
                    _me.uploadsingle(file, cb, idx+1, be);
               }
            });
        }
    }
}