/*
*
*	event 扩展 1.0.0.1 
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		if(!ET.scroll) ET.loadLib('scroll');
	}
	/**
	 * 停止传播事件
	 * @param  {[type]} e Event
	 * @return {[type]}   [description]
	 */
	,stopPropagation:function(e){
		try{
			e=(e&&e.stopPropagation)?e:window.event;
	        if(!e)return ;
	        e.stopPropagation?e.stopPropagation():e.cancelBubble = true;
	    }catch(e){
	    	console.log(e);
	    }
	}
	/**
	 * 防止默认
	 * @param  {[type]} e Event
	 * @return {[type]} [description]
	 */
	,preventDefault:function(e){
		try{
			e=(e&&e.preventDefault)?e:window.event;
	        if(!e)return ;
	        if(e.type == "touchmove"){ return ; }
	        e.preventDefault? e.preventDefault(): e.returnValue = false;
		}catch(e){
	    	console.log(e);
	    }
	}
	/**
	 * 获取鼠标位置
	 * @param  {[type]} e [description]
	 * @return {[type]}   [description]
	 */
	,getMousePos:function(e) {
		e = e?e:window.event;
		var scl = ET.scroll.postion()
		, x = e.pageX || e.clientX + scl.left
		, y = e.pageY || e.clientY + scl.top
		, points =  { 'x': x, 'y': y };
		return points;
	}
	/**
	 * 获取鼠标位置（准确）
	 * @param  {[type]} e [description]
	 * @return {[type]}   [description]
	 */
	,getPos:function(e) {
		e = e?e:window.event;
		var pos = this.getMousePos(e);
		el = $(e.toElement);
		while( el.length ){
			var t = el.css('top').replace(/[^\d]+/, '')*1
			, l = el.css('left').replace(/[^\d]+/, '')*1
			, p = el.css('position');
			if(p == 'absolute' || p == 'fixed'){
				pos.x -= l; pos.y -= t;
			}
			el = el.parent();
			if(el[0] == document){break;}
		}
		return pos;
	}
}