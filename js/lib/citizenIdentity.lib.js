{
	initialize:function(){
	}
	,loadData:function(data){
		if(!data)return this;
		this.data = data;
	}
	,load:function(areaCode, async){
		var _me = this;
		if(!areaCode) areaCode = "cn";
		var url = ET.jsPath +"/dat/CitizenIdentityCard."+areaCode+".dat.js" ;
		$.ajax({
			url:url
			,async:async?true:false
			,dataType:"json"
			,success:function(e){
				_me.loadData(e); 
			}

		});
	}
	,getData : function(id, pid){
		if(!this.data)return null;
		for(var i=0; i<this.data.length; i++){
			if(this.data[i].id==id){
				if(pid && this.data[i].pid!=pid )continue;
				return this.data[i];
			}
		}
		return null;
	}
	,create : function(){
		if(!this.data)this.load(null, false);
		var _me = this;
		function rnd(pid){
			var data =[];
			for(var i=0; i<_me.data.length; i++){
				var d = _me.data[i];
				if(pid != d.pid)continue;
				data.push(d);
			}
			return data[parseInt(Math.random()*data.length-1)];
		}
		var province = city =  area = null;
		while(!city || !area){
			province = rnd('86');
			if(!province)continue;
			city = rnd(province.id);
			if(!city)continue;
			area = rnd(city.id);
		}
		var year = 1900+parseInt(Math.random()*(new Date).getYear());
		var month = parseInt(Math.random()*12);
		var day = parseInt(Math.random()*30);
		if(!area)debugger;
		return this.createNumber(province.title, city.title, area?area.title:null, year, month, day, Math.random()>0.5);
	}
	,createNumber : function(province, city, area, year, month, day, sex){
		if(!this.data)this.load(null, false);
		var _me = this;
		function search(title, pid){
			for(var i=0; i<_me.data.length; i++){
				var d = _me.data[i];
				if(pid && pid != d.pid)continue;
				if(title == d.title){ return d;}
			}
			return null;
		}
		function getAC(tit, pid){
			if(!tit)return null;
			var pd = search(tit, pid);
			if(!pd){
				var t = tit.substr(0, tit.length-1);
				while(t.length>1 && !pd){pd = search(t, pid); t = t.substr(0, t.length-1);}
			}
			return pd;
		}
		var p = getAC(province);
		if(!p)p = getAC(province+'省')
		if(!p){ return null;}
		var c = getAC(city, p.id);
		if(!c)c = getAC(city+'市', p.id)
		if(!c){ return null;}
		var a = getAC(area, c.id);
		if(!a)a = getAC(area+'区', c.id)
		if(!a)a = getAC(area+'县', c.id)
		if(!a){ return null;}
		var data = a?a.id:c.id;
		data += ('19'+parseInt(year)).slice(-4);
		data += ('00'+parseInt(month)).slice(-2)+('00'+parseInt(day)).slice(-2);
		data += ('0'+ET.random.int(99)).slice(-2);
		data += (sex==true || sex=='男')?[1,3,5,7,9][ET.random.int(4)]:[0,2,4,6,8][ET.random.int(4)]
		data += this.createVC(data);
		return data;
	}
	,createVC : function(number){
		if(!number) return false;
		var xs = [7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2];
		var vc = [1,0,'X',9,8,7,6,5,4,3,2];
		var ret =0;
		for(var i=0; i<17; i++){
		    var n = number.substr(i, 1);
		    ret += n*xs[i];    
		}
		return vc[ret%11];
	}
	,checkVC : function(number){
		var vc = this.createVC(number);
		if(vc === false)return false;
		if((vc+'') == (number+'').substr(number.length-1,1))return true;
		return false;
	}
	,check : function(number){
		if(!number) return false;
		if(!/[\dX]{18}/gi.test(number))return false;
		return this.checkVC(number);
	}
	,getInfo : function(number){
		if(!this.data)this.load(null, false);
		if(!number) return null;
		if(!/[\dX]{18}/gi.test(number))return null;
		var m = /(\d{2})(\d{2})(\d{2})(\d{4})(\d{2})(\d{2})(\d{2})(\d)([\dxX])/gi.exec(number);
		if(!m) { return null;}
		var p = this.getData(m[1]+'0000');
		if(!p){ return null;}
		var c = this.getData(m[1]+m[2]+'00');
		if(!c){ return null;}
		var a = this.getData(m[1]+m[2]+m[3]);
		return {
			"province" : p.title
			,"city" : c.title
			,"area" : a?a.title:''
			,"birthday" : m[4]+'-'+m[5]+'-'+m[6]
			,"sex" : (parseInt(m[8])%2 == 0)?'女':'男'
		};
	}
}