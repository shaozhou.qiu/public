/*
*
*	Terminal Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
* eg:
*  <div et="map" longitude="0" latitude="0" Flag="true" maptype="baidu" onload="地图加载完成后执行"></div>
*/
{
	url:ET.appPath+"EtSoftWare.php?m=Terminal&cmd="
	,initialize:function(obj){
		// http://127.0.0.1:8080/public/app/EtSoftWare.php?m=Terminal&cmd=nmap&127.0.0.1&-p&80
	}
	,nmap:function(ip, port, fun){
		var url=this.url+'nmap';
		// for(var i =0, l = arguments.length; i<l; i++){
		// 	url += "&"+encodeURIComponent(arguments[i]);
		// }
		url += '&ip='+encodeURIComponent(ip);
		url += '&ports='+encodeURIComponent(port);
		var ret = ET.getFile(url, fun);
		if(ret.errcode==0){
			var re = new RegExp("[\\s]*("+port+")\\/(\\w+)[\\s]*(\\w+)[\\s]*(\\w+)", "gi");
			var m = re.exec(ret.data)
			if(m){
				return {
					'PORT' : m[1]+'/'+m[2]
					,'STATE' : m[3]
					,'available' : (m[3].toLowerCase()=='open')?true:false
					,'SERVICE' : m[4]
				};
			}
		}
		return null;
	}
}