/*
*
*	chart Beta 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
		
	}
	, fn:{
		/**
		 * [loadData description]
		 * @param  {[type]} data [[...],...] or [{...}, ...]
		 * @return {[type]}      [description]
		 */
		loadData:function(data){
			var reval={
				x:{min:0, max:data.length}
				,y:{min:null, max:null}
				,d:{}
			}, v = {min:100 , max:0 , d:{} };
			if(typeof(data[0])=='object'){
				v = this.loadObject(data);
			}else{
				v = this.loadArray(data);
			}
			$.extend(reval, {
				y:{min:v.min, max:v.max}
				,d:v.d
			});
			return reval;
		}
		,loadObject:function(data){
			var reval={
				min:100
				, max:0
				, d:{}
			};
			for(var i=0; i<data.length; i++){
				for(var k in data[i]){
					var v = data[i][k];
					if(v<reval.min) reval.min=v;
					if(v>reval.max) reval.max=v;
					if(!reval.d[k]){
						reval.d[k] =[];
					}
					reval.d[k].push(v);
				}
			}
			return reval;
		}
		,loadArray:function(data){
			var reval={
				min:100
				, max:0
				, d:{}
			};
			for(var i=0; i<data.length; i++){
				if(data[i]<reval.min) reval.min=data[i];
				if(data[i]>reval.max) reval.max=data[i];
			}
			reval.d = {'undefined':data};
			return reval;
		}
	}
	/**
	 * [line description]
	 * @param  {[type]} el   Element eg: svg
	 * @param  {[type]} data [1,.....] or [{a:1,b:2,...},....]
	 * @param  {[type]} xkeys 自定义坐标显示文本
	 * @param  {[type]} margin 偏移坐标
	 * @param  {[type]} callback 绘制坐标时回调函数
	 * @return {[type]}      [description]
	 */
	,line : function(el, data, xkeys, margin, callback){
		if(!el || !data)return ;
		if(!margin){
			margin = [30, 30,  30, 30];
		}
		var o = this.fn.loadData(data);
		var w = $(el).width()
		, h = $(el).height()
		, xRuler = null
		, yRuler = null
		, showDataPoint = $(el).attr('datapoint')
		;
		showDataPoint = parseInt(showDataPoint)?parseInt(showDataPoint):showDataPoint=='true'?2:0;
		if(xkeys){
			o.x.min = 0;
			o.x.max = xkeys.length;
		}
		margin[3] += (o.y.max+'').length*12;
		margin[2] += (o.x.max+'').split('\n').length*12;

		$.extend(margin, {"top" : margin[0], "right" : margin[1], "bottom" : margin[2], "left" : margin[3], });
		
		if(el.drawRuler){
			var xki = 0;
			var rx = margin.left, ry = h-margin.bottom;
			var rx2 = w-margin.right, ry2 = h-margin.bottom;
			xRuler = el.drawRuler(rx, ry, rx2, ry2, o.x.min, o.x.max, o.x.max, 1, null, null, 0, function(v){
				return xkeys?xkeys[xki++]:v;
			});
			
			ry2 = margin.top;
			yRuler = el.drawRuler(rx, ry, rx, ry2, o.y.min, o.y.max, 0, 0);
		}
		var tx = w-10-12*8, ty=margin.top;
		for(var k in o.d){
			var color = '#'+ET.random.Hex(6), points=[];
			for(var i=0; i<o.d[k].length; i++){
				var p = {c:'l', x:0, y:0};
				if(points.length==0){p.c='m';}

				p.x = xRuler.getPos(i);
				p.y = yRuler.getPos(o.d[k][i]);
				if(showDataPoint){
					el.drawCircle(p.x, p.y, showDataPoint);
				}
				if(callback){
					callback.call(this, p);
				}
				points.push(p);
			}
			if(el.drawPath){
				var l = el.drawPath(points, false, null,color);
				$(l).attr('title', k);
			}
			if(el.drawRect){
				ty+=12+5;
				el.drawRect(tx, ty, tx+10, ty+10, 0, 0, color);
				if(el.drawText){
					el.drawText(tx+15, ty+10, k);
				}
			}
		}
	}
	
}