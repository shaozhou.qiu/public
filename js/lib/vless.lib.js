/*
*
*	vless 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
        ET.loadLib('object');
	}
	/**
	 * 编码
	 * vmess 转 base64
	 * @param  {[type]} data [description]
	 * @return {[type]}     [description]
	 */
    ,encode:function(uid, add, port, encryption, security, type, headerType, flow, remarks, group){
        return "vless://"+uid+"@"+add+":"+port+"?"+
        "encryption="+(encryption?encryption:"none")+
        "&security="+(security?security:"xtls")+
        "&type="+(type?type:"tcp")+
        "&host="+add+
        "&headerType="+(headerType?headerType:"none")+
        "&flow="+(flow?flow:"xtls-rprx-direct")+
        "#=["+group+"]"+remarks;
	}
	/**
	 * 解码
	 * 将base64 转为 vmess
	 * @param  {[type]} str 如 vmess://bm9uZTo3Mjg0MjZiMC0xMDM0LTQ0NTAtYTgzNS1iNDk0MzFkYjYwNjZAMTk0LjE1Ni4xMjEuMTI0OjM4MDI4
	 * @return {[type]}     [description]
	 */
	,decode:function(str){
        if(!/^\s*vless:\/\/.*?@.*$/gi.test(str))return null;
        var m = /^\s*vless:\/\/(.*?)@(.*?):(\d+)\?(.*?)(#.*)?$/gi.exec(str);
        var data = {};
        data.addr = data.host = m[2];
        data.port = m[3]*1;
        data.uid = m[1];
        data.remarks = m[5].substr(1);
        var pams = m[4];
        if(pams){
            var mc = pams.match(/([^&]+?)=([^&]+)/gi);
            for (var i = 0; i < mc.length; i++) {
                var m = /([^&]+?)=([^&]+)/gi.exec(mc[i]);
                data[m[1]] = m[2];
            }
        }
        return data;
	}
}