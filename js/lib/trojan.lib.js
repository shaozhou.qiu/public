/*
*
*	Trojan 1.0.0.1
*	Release date: 
*	Author		: 半条虫(466814195)
*	Keywords	: Etsoftware 半条虫(466814195) rimke 39doo 39度
*	Description	: 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*	Blog		: http://rimke.blog.163.com/		
*	Website		: http://www.39doo.com/
*	Mail		: rimke@163.com
*	Copyright	: Power By Etsoftware
*
*/
{
	initialize:function(){
	}
	/**
	 * 编码
	 * trojan 转 base64
	 * @param  {[type]} data [description]
	 * @return {[type]}     [description]
	 */
	,encode:function(server, port,  password, remarks){
        var data = 'trojan://'+ encodeURIComponent(password) +'@'+ encodeURIComponent(server) +':'+port ;
        if(remarks){
        	data +='#' + encodeURIComponent(remarks);
        }
        return data ;
	}
	/**
	 * 解码
	 * 将base64 转为 trojan
	 * @param  {[type]} str 如 trojan://xxxxxx
	 * @return {[type]}     [description]
	 */
	,decode:function(str){
		if(!str){return null;}
		
		if( false==/^[\s]*trojan:\/\/([\w\@\:\-\@\.\%]+)(.*)$/gi.test(str) ){return null; }
		var m = /^[\s]*trojan:\/\/([\w\@\:\-\@\.\%]+)(.*)$/gi.exec(str);
		if(!m){return null;}
		var parms = m[2];
		var remarks = '';
		var url=m[1];
		m = /^([^\@]+)\@([^:]+):(.+)$/gi.exec(url);
		if(!m){ return null;}
		var obj = {
			password: m[1]
			,server: m[2]
			,port: m[3]
		};
		var mc = parms.match(/(\?|\&)(\w+)\s*=\s*([^&]+)/gi);
		if(mc){
			for(var i=0; i<mc.length; i++){
				var m = /(\?|\&)(\w+)\s*=\s*([^&]+)/gi.exec(mc[i]);
				if(m)obj[m[2]] = m[3];
			}

		}else{
			remarks = decodeURIComponent(parms.replace(/\+/gi, ' '));
		}
		obj.remarks = remarks;
		return obj;
	}
}
